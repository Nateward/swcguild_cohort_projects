/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.warmupdatamodeling;

import java.util.*;

/**
 *
 * @author apprentice
 */
public class Course {

    private String courseName;
    private String courseNumber;
    private Map<String, Student> studentMap;

    public Course(String courseName, String courseNumber) {

       this();
        this.courseName = courseName;
        this.courseNumber = courseNumber;
    }

    public Course() {
       studentMap = new HashMap<>();
        this.courseName = "";
        this.courseNumber = "";

    }

    public void setClassNumber(String classNumber) {
        this.courseNumber = classNumber;
    }

    public void addStudent(Student student) {
        studentMap.put(student.getEmail(), student);
    }

    public void printCourseReport() {
        System.out.println("Course name: " + courseName);
        System.out.println("Course number: " + courseNumber);
        
        Set<String> keys = studentMap.keySet();
        for (String key : keys) {
            String email = studentMap.get(key).getEmail();
            String name = studentMap.get(key).getStudentName();
            System.out.println("Student: " + name + " emai: " + email);
        }

//
//        for (int i = 0; i < 10; i++) {
//            System.out.println("Student: " + students[i].getStudentName());
//            System.out.println("Email: " + students[i].getEmail());
        }
    }

