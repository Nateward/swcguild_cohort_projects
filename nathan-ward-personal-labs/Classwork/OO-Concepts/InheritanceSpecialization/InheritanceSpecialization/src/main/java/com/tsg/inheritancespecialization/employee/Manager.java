/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.inheritancespecialization.employee;

import com.tsg.inheritancespecialization.employee.Employee;

/**
 *
 * @author apprentice
 */
public class Manager extends Employee {

    private String title;

    public Manager() {
        this("John Doe", "987-78-9789");
        title = "GM";
        name = "Bob Jones";
    }
    
    public Manager(String objectName){
//        this.(objectName.getName(), objectName.getSsn());
                
    }
    
    public Manager(String name, String ssn) {
        super(name, ssn);
        title = "PM";
    }

    public void hire() {
        System.out.println("Welcome aboard!");
    }

    public void fire() {
        System.out.println("Your services are no longer needed.");
    }

    public void givePerformanceReview() {
        System.out.println(name + " said: you need to do better");

    }

    @Override
    public void collectBonus() {
        System.out.println("Collect your 20% of anual pay");
    }

    @Override
    public void createObjectives() {
        System.out.println("Make everyone work harder!!!");
        super.createObjectives();
    }
}
