/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.gamebot.games;

import java.util.Random;
import java.util.Scanner;

/**
 * Idea for improvement: check for letters already guessed, either right or wrong
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class Hangman implements Game {

    private final String gameName = "Hangman";
    
    @Override
    public String getGameName(){
        return this.gameName;
    }

    @Override
    public void playGame() {
        Scanner sc = new Scanner(System.in);

        do {

            final int MAX_MISSES = 8; // edit this to appropriate number of misses allowed

            int missIndex = 0;
            int numMissesRemaining = MAX_MISSES;

            char[] secretWord = generateWord(); // call generate word method to pull random word
            char[] wordInProgress = new char[secretWord.length]; // generate blank char array same length as random word, representing guesser's word in progress
            char[] misses = new char[numMissesRemaining]; // create blank char array to fill with wrong guesses

            do {

                System.out.println("\n-=-=-=-=-=-=-=-=-=-=-=-=-=-");

                // print word in progress with blanks where letter is unknown
                System.out.print("\nWord: ");
                for (char letterInProgress : wordInProgress) {
                    if (letterInProgress == '\0') {
                        System.out.print("_ ");
                    } else {
                        System.out.print(letterInProgress + " ");
                    }
                }
                // print char array of missed letters
                System.out.print("\n\nMisses: ");

                for (char missChar : misses) {
                    System.out.print(missChar);
                }
                System.out.println();

                System.out.print("\nYour guess: ");
                String guess = sc.nextLine();              // get input from user
                char guessChar = guess.toCharArray()[0];    // grabs char from input string

                // basic game logic - check to see if guess letter matches
                // if match, place into wordInProgress array; if no match, it will go into misses array
                boolean missCheck = true;
                int numberOfTimes = 0;
                for (int i = 0; i < secretWord.length; i++) {
                    if (secretWord[i] == guessChar) {
//                System.out.println("That matched! " + i); // test code to ensure it worked as expected
                        wordInProgress[i] = guessChar;
                        missCheck = false; // it was a hit
                        numberOfTimes++; // adds one to total number of instances of letter in word
                    }
                }

                // if missCheck is still true after running through word, it assigns it to the next available index in misses array
                // and then increments the current index by one
                System.out.println();

                if (missCheck) {
                    System.out.println("'" + guessChar + "' is not in the word.");
                    misses[missIndex] = guessChar; // assign char to misses char array
                    missIndex++; // increment index by one
                    numMissesRemaining--;      // subtract one from remaining number of misses

                    System.out.println("You have " + numMissesRemaining + " more miss(es) remaining.");
                } else {
                    System.out.println("Correct! '" + guessChar + "' appears " + numberOfTimes + " time(s).");
                }

            } while (doesCharArrayContainBlank(wordInProgress) && numMissesRemaining != 0);

            // game ends; display you won message plus number of rounds
            if (!doesCharArrayContainBlank(wordInProgress)) {
                System.out.println("\n *** YOU WON! ***");
            } else {
                System.out.println("Sorry, you lost.");
                System.out.print("The word was: "); // show the secret word
                for (char wordChar : secretWord) {
                    System.out.print(wordChar);
                }
            }

        } while (!sc.nextLine().equals("quit"));

        System.out.println("Thanks for playing!");
    }

    private char[] generateWord() {
        Random rng = new Random();

        String[] wordSeed = {"javascript", "boolean", "integer", "definition", "signature", "variable", "parameter", "string", "mathematics",
            "method", "python", "scanner", "laptop", "keyboard", "monitor", "whiteboard", "programming",
            "donkeylobster", "netbeans", "google", "macbook", "motherboard", "terminal", "directory"}; // string array of possible words

        int randomWordIndex = rng.nextInt(wordSeed.length); // pulls random from hat of size wordSeed.length

        return wordSeed[randomWordIndex].toCharArray(); // returns randomly indexed word String as char array
    }

    private boolean doesCharArrayContainBlank(char[] test) {
        for (char c : test) {
            if (c == 0) {
                return true;
            }
        }
        return false;
    }

}
