<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator - Setup Calculator</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h1>Please fill in the following fields.</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-lg-2"></div>
                <div class="col-lg-8 well">
                    <form class="form-horizontal" action='InterestCalculatorServlet' method='POST'>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Interest Rate: </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input name="interest" type="number" class="form-control" placeholder="Interest">
                                    <div class="input-group-addon">%</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Principal Amount: </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <div class="input-group-addon">$</div>
                                    <input name="principal" type="number" class="form-control" placeholder="Amount">
                                    <div class="input-group-addon">.00</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Time Frame: </label>
                            <div class="col-sm-10">
                                <div class="input-group">
                                    <input name="timeFrame" type="number" class="form-control" placeholder="Number Of Years Invested">
                                    <div class="input-group-addon">/Yrs.</div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-sm-2 control-label">Compound Rate: </label>
                            <div class="col-sm-10">
                                <input name="compound" type="number" class="form-control" placeholder="e.i.- 365-daily, 12-monthly, 4-quarterly">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-lg-offset-10">
                                <input type="submit" class ="btn btn-success" value="Show me the money!"/>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="col-lg-2"></div>
            </div>
        </div>
    </body>
</html>
