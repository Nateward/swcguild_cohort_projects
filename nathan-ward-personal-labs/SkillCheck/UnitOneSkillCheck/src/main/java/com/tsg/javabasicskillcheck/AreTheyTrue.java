/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.javabasicskillcheck;

/**
 *
 * @author apprentice
 */
public class AreTheyTrue {

    public static void main(String[] args) {
        String result = "";
        
       result = howTrue(true, true);
        System.out.println(result);
        
        result = howTrue(false, false);
        System.out.println(result);
        
        result = howTrue(true, false);
        System.out.println(result);
        
        result = howTrue(false, true);
        System.out.println(result);

    }

    public static String howTrue(Boolean x, Boolean y) {

        String result = "";

        if ((x == false) && (y == false)) {
            result = "NEITHER";
        }
        else if ((x== true) && (y == true)) {
            result = "BOTH";
        }
        else if ((x == true) || (y == true)) {
            result = "ONLY ONE";
    }
        return result;
               
    }
}
