/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab3consolecalculator;

/**
 *
 * 
 * @author apprentice
 */
public class MathLogic {
    
//add method
    public float add(float firstNumber, float secondNumber) 
    {
        System.out.printf(firstNumber + " + " + secondNumber + " = %s", (firstNumber + secondNumber));
        return firstNumber + secondNumber;
    }
//substract method
    public float substract(float firstNumber, float secondNumber)
    {
        System.out.printf(firstNumber + " - " + secondNumber + " = %s", (firstNumber - secondNumber));
        return firstNumber - secondNumber;
    }
//multiply method
    public float multiply(float firstNumber, float secondNumber)
    {
        System.out.printf(firstNumber + " * " + secondNumber + " = %s", (firstNumber * secondNumber));
        return firstNumber * secondNumber;
    }
//divide method
    public float divide(float firstNumber, float secondNumber) 
    {
        System.out.printf(firstNumber + " / " + secondNumber + " = %s", (firstNumber / secondNumber));
        return firstNumber / secondNumber;
    }

}
