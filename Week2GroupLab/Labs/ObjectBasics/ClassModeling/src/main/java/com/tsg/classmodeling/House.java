/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class House {
    private int price;
    private int height;
    private int width;
    private int length;
    private String address;
    
   
    public House(int price, int height, int width, int length, String address) {
        this.price = price;
        this.height = height;
        this.width = width;
        this.length = length;
        this.address = address;
    }
    
    public int getPrice(){
        return price;
    }
    
    public void setPrice(int price){
        this.price = price;
    }
        
    
    public int getHeight(){
        return height;
    }
    
    public void setHeight(int height){
        this.height = height;
    }
    
    
    public int getWidth(){
        return width;
    }
    
    public void setWidth(int width){
        this.width = width;
    }
    
    
    public int getLength(){
        return length;
    }
    
    public void setLength(int length){
        this.length = length;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }
    
    
//    Behaviors
    ////////////
    
    /**
     * This behavior lets you know the house is heating.
     */
    public void heating()
    {
        System.out.println("The house is hot.");
    }
    
    
    /**
     * This behavior lets you know the house is cooling.
     */
    public void cooling()
    {
        System.out.println("The house is cool.");
    }
}
