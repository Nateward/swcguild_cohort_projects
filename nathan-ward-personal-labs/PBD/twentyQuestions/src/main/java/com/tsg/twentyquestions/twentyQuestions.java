/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.twentyquestions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class twentyQuestions {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        String type = "";
        String size = "";
        String guess = "";
        
        
        System.out.println("Think of an object, and I'll try to guess it.");
        
        
        System.out.println("Question 1) Is it animal, vegetable, or mineral?");
        System.out.println("> ");
        type = keyboard.next();        
        
        System.out.println("Question 2) Is it bigger than a breadbox?");
        System.out.println("> ");
        size = keyboard.next();
        
        if(type.equalsIgnoreCase("animal")){
            if(size.equalsIgnoreCase("yes")){
                guess = "wolly mammoth!";
            }
            else{
                guess = "Hamster.";
            }
        }
        else if(type.equalsIgnoreCase("vegetable")){
            if(size.equalsIgnoreCase("yes")){
                guess = "pumpkin.";
            }
            else{
                guess = "hamster.";
            }
            
        }
        else if(type.equalsIgnoreCase("mineral")){
            if(size.equalsIgnoreCase("yes")){
                guess = "truck.";
            }
            else{
                guess = "gold necklace.";
            }
            
        }
        else{
            guess = "thing I've never heard of.";
        }
        System.out.println("My guess is that you are thinking of a " + guess);
        System.out.println("I would ask you if I'm right, but I don't actually care.");
    }
    
}
