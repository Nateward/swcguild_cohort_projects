/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistqueue;

/**
 *
 * @author apprentice
 */
public interface LinkedListQueueInterface extends Iterable{
    public void enqueue(Object o);
    public Object dequeue();
    public int size();
    public boolean isEmpty();
}
