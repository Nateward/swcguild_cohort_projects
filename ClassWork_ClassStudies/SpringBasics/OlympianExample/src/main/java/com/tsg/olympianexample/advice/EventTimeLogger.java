/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.olympianexample.advice;

import java.io.File;
import java.io.FileOutputStream;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import org.aspectj.lang.ProceedingJoinPoint;

/**
 *
 * @author ilyagotfryd
 */
public class EventTimeLogger {
    
    public Object logEventTime(ProceedingJoinPoint jp)
    {
        Object result = null;
        try{
            long start = System.currentTimeMillis();
            result = jp.proceed();
            long end = System.currentTimeMillis();
            
            PrintWriter log = new PrintWriter(new FileOutputStream(new File("eventTimeLog.txt"), true));
            LocalDateTime timestamp = LocalDateTime.now();
            log.append("["+timestamp.toString() + "] run duration is " + (end - start) + " ms.\n" );
            log.flush();
            log.close();
            
        }catch (Throwable ex)
        {
            System.out.println("The following exception had occured" + ex.getMessage());
        }
        
        return result;
    }
    
}
