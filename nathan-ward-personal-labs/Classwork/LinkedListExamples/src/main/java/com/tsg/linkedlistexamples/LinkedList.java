/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistexamples;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface LinkedList extends Iterable{
    
    //adds to end of list
    void append(Object item);
    
    //gets a particular index
    Object get(int index);
    
    //Insert after particular index
    void insert(int index, Object item);
    
    boolean isEmpty();
    
    //Inserting elements at the begining
    void prepend(Object item);
    
    Object remove(int index);
    
    int size();
    
    
    
    
}
