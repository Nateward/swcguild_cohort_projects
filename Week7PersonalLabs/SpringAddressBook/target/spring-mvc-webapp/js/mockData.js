/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var testAddressData = [
    {
        idNumber: 1,
        firstName: "Nathan",
        lastName: "Ward",
        streetAddress: "110 Greenwich Rd. Unit #5",
        city: "Seville",
        state: "Ohio",
        zip: "44273"
    },
    {
        idNumber: 2,
        firstName: "Kat",
        lastName: "Ward",
        streetAddress: "110 Greenwich Rd. Unit #5",
        city: "Seville",
        state: "Ohio",
        zip: "44273"
    },
    {
        idNumber: 3,
        firstName: "Pita",
        lastName: "Pocket",
        streetAddress: "3585 Blake Rd.",
        city: "Wadsworth",
        state: "Ohio",
        zip: "44273"
    }

];

var dummyDetailsContact =
        {
            idNumber: 34,
            firstName: "Peter",
            lastName: "Parker",
            streetAddress: "3585 Loser St.",
            city: "New York City",
            state: "New York",
            zip: "67945"
        };

var dummyEditContact =
        {
            idNumber: 34,
            firstName: "Peter",
            lastName: "Parker",
            streetAddress: "3585 Loser St.",
            city: "New York City",
            state: "New York",
            zip: "67945"
        };