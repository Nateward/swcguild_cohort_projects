/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistfifonode;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface LinkedListFIFONode extends Iterable{
    
    public void enqueue(Object o);
    public Object deQueue();
//    public Object get(int index);
//    public void insert(int index, Object item);
    public int size();
    public boolean isEmpty();
}
