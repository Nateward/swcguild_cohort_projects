/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.abstractclassexample.calc;

import java.util.List;

/**
 *
 * @author apprentice
 */
public abstract class Calculation {
    protected String calculationName;
    protected String result;
    
    public void display(){
        System.out.println("calculation " + getCalculationName() + " result: " + getResult()); 
    }
    
    abstract public void Calculate(List<Double> values);

    /**
     * @return the calculationName
     */
    public String getCalculationName() {
        return calculationName;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }
}
