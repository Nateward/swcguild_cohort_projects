/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab4consoleio;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class ConsoleIO {

    private int int1;
    private int int2;

    private String word;

    private float float1;
    private float float2;

    private double double1;
    private double double2;

    Scanner keyboard = new Scanner(System.in);
    private int userInputInt;
    private float userInputFloat;
    private double userInputDouble;
    private String userInputString;

    /////////////////////
    ////Method 1////////
    ///////////////////
    public int method1(String userPrompt) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();
        try {
            userInputInt = Integer.parseInt(userInputString);
        } catch (Exception e) {
            System.out.println("Bad input!!\n");
            method1(userPrompt);
        }

        setInt1(userInputInt);
        return getInt1();

    }

    /////////////////////
    ////Method 2////////
    ///////////////////
    public int method2(String userPrompt, int min, int max) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();

        try {
            userInputInt = Integer.parseInt(userInputString);
        } catch (Exception e) {
            System.out.println("\nBad input!!");
            method2(userPrompt, min, max);
        }

        if (userInputInt >= min && userInputInt <= max) {
            setInt2(userInputInt);

        } else {

            System.out.println("\nBad input!!");
            method2(userPrompt, min, max);

        }
        return getInt2();
    }

    /////////////////////
    ////Method 3////////
    ///////////////////
    public String method3(String userPrompt) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();
        try {
        } catch (Exception e) {
            System.out.println("Bad input!!\n");
            method3(userPrompt);
        }

        setWord(userInputString);
        return getWord();

    }

    /////////////////////
    ////Method 4////////
    ///////////////////
    public float method4(String userPrompt) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();
        try {
            userInputFloat = Float.parseFloat(userInputString);
        } catch (Exception e) {
            System.out.println("Bad input!!\n");
            method4(userPrompt);
        }

        setFloat1(userInputFloat);
        return getFloat1();

    }

    /////////////////////
    ////Method 5////////
    ///////////////////
    public float method5(String userPrompt, float min, float max) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();

        try {
            userInputFloat = Float.parseFloat(userInputString);
        } catch (Exception e) {
            System.out.println("\nBad input!!");
            method5(userPrompt, min, max);
        }

        if (userInputFloat >= min && userInputFloat <= max) {
            setFloat2(userInputFloat);

        } else {

            System.out.println("\nBad input!!");
            method5(userPrompt, min, max);

        }
        return getFloat2();
    }

    /////////////////////
    ////Method 6////////
    ///////////////////
    public double method6(String userPrompt) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();
        try {
            userInputDouble = Double.parseDouble(userInputString);
        } catch (Exception e) {
            System.out.println("Bad input!!\n");
            method6(userPrompt);
        }

        setDouble1(userInputDouble);
        return getDouble1();

    }

    /////////////////////
    ////Method 7////////
    ///////////////////
    public double method7(String userPrompt, double min, double max) {

        System.out.println(userPrompt);
        userInputString = keyboard.nextLine();

        try {
            userInputDouble = Double.parseDouble(userInputString);
        } catch (Exception e) {
            System.out.println("\nBad input!!");
            method7(userPrompt, min, max);
        }

        if (userInputDouble >= min && userInputDouble <= max) {
            setDouble2(userInputDouble);

        } else {

            System.out.println("\nBad input!!");
            method7(userPrompt, min, max);

        }
        return getDouble2();
    }


    public int getInt1() {
        return int1;
    }

    public void setInt1(int int1) {
        this.int1 = int1;
    }

    public int getInt2() {
        return int2;
    }

    public void setInt2(int int2) {
        this.int2 = int2;
    }

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public float getFloat1() {
        return float1;
    }

    public void setFloat1(float float1) {
        this.float1 = float1;
    }

    public float getFloat2() {
        return float2;
    }

    public void setFloat2(float float2) {
        this.float2 = float2;
    }

    public double getDouble1() {
        return double1;
    }

    public void setDouble1(double double1) {
        this.double1 = double1;
    }

    public double getDouble2() {
        return double2;
    }

    public void setDouble2(double double2) {
        this.double2 = double2;
    }

}
