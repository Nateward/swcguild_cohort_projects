/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.addressbookv2.dao.AddressBookV2DAOimpl;
import com.tsg.addressbookv2.dto.Address;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author apprentice
 */
public class AddressBookDAOJUintTest {

    public AddressBookDAOJUintTest() {
    }

    AddressBookV2DAOimpl dao;
    Address address1;
    Address address2;
    Address address3;
    Address address4;

    @Before
    public void setUp() {
        dao = new AddressBookV2DAOimpl();
        address1 = new Address();
        address1.setFirstName("Nate");
        address1.setLastName("Ward");
        address1.setStreetAddress("110 Greenwich Rd.");
        address1.setCity("Medina");
        address1.setState("Ohio");
        address1.setZip(44273);

        address2 = new Address();
        address2.setFirstName("John");
        address2.setLastName("Ward");
        address2.setStreetAddress("115 Greenwich Rd.");
        address2.setCity("Seville");
        address2.setState("Ohio");
        address2.setZip(44273);

        address3 = new Address();
        address3.setFirstName("Jane");
        address3.setLastName("Smith");
        address3.setStreetAddress("105 Greenwich Rd.");
        address3.setCity("Seville");
        address3.setState("Ohio");
        address3.setZip(44273);

        address4 = new Address();
        address4.setFirstName("Bill");
        address4.setLastName("Smithfield");
        address4.setStreetAddress("105 Greenwich Rd.");
        address4.setCity("Seville");
        address4.setState("Maine");
        address4.setZip(44256);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
    @Test //Testing for creating an address and deleting an address
    public void createAddressTest() {

        //arrange
        dao.addAddress(address1);

        //act
        Address addy = dao.removeAddress(address1.getIdNumber());

        //assert
        Assert.assertEquals(address1, addy);
    }

    @Test   //Test to get all addresses by last name.
    public void getaddressesByLastName() {
        ArrayList addy = new ArrayList<>();

        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);

        //act
        addy = dao.findAddressByLastName(address1.getLastName());
        int listSize = addy.size();
        //assert
        Assert.assertEquals(2, listSize);
    }

    @Test   //Test to get all addresses by city name.
    public void getaddressesByCity() {
        ArrayList addy = new ArrayList<>();

        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);

        //act
        addy = dao.findAddressByCity(address2.getCity());
        int listSize = addy.size();
        //assert
        Assert.assertEquals(2, listSize);
    }

    @Test   //Test to get all addresses by state name grouped by city.
    public void getaddressesByState() {
        
        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        dao.addAddress(address4);

        //act
        Map<String, List<Address>> addressMap = dao.findAddressByState(address2.getState());
        int mapSize = addressMap.size();
        
        //assert
        Assert.assertEquals(2, mapSize);
    }
    
    @Test   //Test to get all addresses by state 
            //name grouped by city showing number of addresses in city.
    public void getaddressesByStateAndCitySize() {
        
        
        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        dao.addAddress(address4);

        //act
        Map<String, List<Address>> addressMap = dao.findAddressByState(address2.getState());
        List<Address> addressList = addressMap.get(address2.getCity());
        int addListSize = addressList.size();
        
        //assert
        Assert.assertEquals(2, addListSize);
    }
    
    @Test   //Test to get all addresses by state 
            //name grouped by city showing number of addresses in city.
    public void getaddressesByStateAndCityComparison() {
        
        
        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        dao.addAddress(address4);

        //act
        Map<String, List<Address>> addressMap = dao.findAddressByState(address2.getState());
        List<Address> addressList = addressMap.get(address2.getCity());
        int addListSize = addressList.size();
        
        //assert
        Assert.assertEquals(2, addListSize);
        Assert.assertEquals(addressList.get(0).getState(), addressList.get(1).getState());
        Assert.assertTrue(addressList.contains(address2));
        Assert.assertFalse(addressList.contains(address1));
        
        
    }
    
    @Test   //Test to get all addresses by zip.
    public void getAddressesByZip() {
        ArrayList addy = new ArrayList<>();

        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        dao.addAddress(address4);

        //act
        addy = dao.findAddressByZip(address1.getZip());
        int listSize = addy.size();
        //assert
        Assert.assertEquals(3, listSize);
    }
    
     @Test   //Test to get all addresses.
    public void getAllAddresses() {
        ArrayList addy = new ArrayList<>();

        //arrange
        dao.addAddress(address1);
        dao.addAddress(address2);
        dao.addAddress(address3);
        dao.addAddress(address4);

        //act
        addy = dao.findAllAddresses();
        int listSize = addy.size();
        //assert
        Assert.assertEquals(4, listSize);
    }
    
}
