/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var testDvdData = [
    {
        idNumber: 1,
        title: "Die Hard",
        releaseDate: "1980",
        rating: "R",
        director: "Hanz Gruber",
        studio: "MGM",
        notes: "Bruce Willis"
    },
    {
        idNumber: 2,
        title: "Sherlock Holomes",
        releaseDate: "2012",
        rating: "PG-13",
        director: "James Moriarty",
        studio: "WB",
        notes: "Got engaged to this, it was so good!"
    },
    {
        idNumber: 3,
        title: "Casino Royale",
        releaseDate: "2004",
        rating: "R",
        director: "Mr. Green",
        studio: "MGM",
        notes: "James, James Bond."
    }

];

var dummyDetailsContact =
        {
        idNumber: 23,
        title: "SpiderMan",
        releaseDate: "2004",
        rating: "PG-13",
        director: "Jack Kirby",
        studio: "Marvel",
        notes: "Oh Peter Parker :("
        };

var dummyEditContact =
        {
        idNumber: 23,
        title: "SpiderMan",
        releaseDate: "2004",
        rating: "PG-13",
        director: "Jack Kirby",
        studio: "Marvel",
        notes: "Oh Peter Parker :("
        };