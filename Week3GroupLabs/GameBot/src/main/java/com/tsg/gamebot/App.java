/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.gamebot;

import com.tsg.gamebot.games.Blackjack;
import com.tsg.gamebot.games.Game;
import com.tsg.gamebot.games.Hangman;
import com.tsg.gamebot.games.LuckySevens;
import com.tsg.gamebot.games.RockPaperScissors;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class App {

    public static void main(String[] args) {
        List<Game> gameBot = new ArrayList<>();
        int userChoice = 0;
        Scanner sc = new Scanner(System.in);
        int i = 0;

        gameBot.add(new RockPaperScissors());
        gameBot.add(new LuckySevens());
        gameBot.add(new Hangman());
        gameBot.add(new Blackjack());

        while (userChoice != (i + 1)) {
            for (i = 0; i < gameBot.size(); i++) {
                System.out.println((i + 1) + ".) " + gameBot.get(i).getGameName());
            }
            System.out.println((i + 1) + ".) Quit");
            System.out.println("");
            System.out.println("Please choose a game(1-" + (i + 1) + "): ");
            
            
            userChoice = sc.nextInt();
            
            if (userChoice >= 1 && userChoice <= gameBot.size()) {
                gameBot.get(userChoice - 1).playGame();
            }
        }
    }
}
