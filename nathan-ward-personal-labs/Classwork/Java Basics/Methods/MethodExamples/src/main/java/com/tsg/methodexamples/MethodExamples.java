/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.methodexamples;

/**
 *
 * @author apprentice
 */
public class MethodExamples {
    public static void main(String[] args) {
        doit();
        
//        this call will not store the result of the  add method
        add(7, 22);
        
//        this willstore the result of add
        int result = add(23, 42);
        
//        This will cause an exception see add(int, int) method
//        result = add(23, 0);

//        Sum is not within scope, because sum is inside the add method
//        System.out.println("The sum is " + sum);
        
    }
    
    public static void doit() {
//        this method takes no parameters and returns no values
        System.out.println("Hello");
        
    }
//    this method takes no parameters and returns an integer value
    public static int get5()
    {
        return 5;
    }
    
//    takes in a parameter and returns no values
    public static void silly(int i)
    {
        System.out.println("My parameter is: " + i);
    }
    
    public static int add(int a, int b)
    {
        int sum = 0;
        sum = a + b;
//        this line could possibly blow it up\/
//        int test = a / b;
        return sum;
    }
}
