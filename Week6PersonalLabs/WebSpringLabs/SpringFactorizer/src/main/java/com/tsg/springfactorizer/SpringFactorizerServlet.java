/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springfactorizer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@WebServlet(name = "SpringFactorizerServlet", urlPatterns = {"/SpringFactorizerServlet"})
public class SpringFactorizerServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("Factor.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        List<Integer> list = new ArrayList<>();

        int numToFactorize = Integer.parseInt(request.getParameter("quantity"));

        list = theFactors(numToFactorize);
        boolean prime = primeNum(numToFactorize);
        boolean perfect = primeNum(numToFactorize);

        request.setAttribute("quantity", numToFactorize);
        request.setAttribute("theFactorz", list);
        request.setAttribute("isItPrime", prime);
        request.setAttribute("isItPerfect", perfect);

        RequestDispatcher rd = request.getRequestDispatcher("Present.jsp");

        rd.forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public boolean primeNum(int x) {
        boolean prime = true;

        for (int i = 1; i < x; i++) {
            if (x % i == 0 && i != 1) {
                prime = false;
            }

        }
        return prime;
    }

    public boolean perfectNum(int x) {
        int perfect = 0;
        boolean perfection = false;

        for (int i = 1; i < x; i++) {
            if (x % i == 0) {
                perfect += i;
            }
        }
        if (perfect == x) {
            perfection = true;
        }
        return perfection;
    }

    public List<Integer> theFactors(int x) {

        List<Integer> list = new ArrayList<>();

        for (int i = 1; i < x; i++) {
            if (x % i == 0) {
                list.add(i);
            }
        }
        return list;
    }
}
