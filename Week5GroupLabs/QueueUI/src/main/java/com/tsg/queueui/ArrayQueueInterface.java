/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.queueui;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface ArrayQueueInterface extends Iterable{
    
    public void enQueue(Object o);
    public Object deQueue();
    public int size();
    public boolean isEmpty();
    
}
