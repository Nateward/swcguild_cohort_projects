/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd.dao;

import com.tsg.springdvd.dto.Dvd;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class DvdDAODbImpl implements DvdDAO{
    
    private static final String SQL_INSERT_Dvd
            = "insert into Dvd (title, releaseDate, mpaaRating, directorsName, studio, userNote) values (?, ?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_Dvd
            = "delete from Dvd where id = ?";

    private static final String SQL_UPDATE_Dvd 
            = "update Dvd set title = ?, releaseDate = ?, mpaaRating = ?, directorsName = ?, studio = ?, userNote = ? WHERE id = ?";
    
    private static final String SQL_SELECT_ALL_Dvds 
            = "select * from Dvd";
    
    private static final String SQL_SELECT_Dvd
            = "select * from Dvd where id = ?";
    
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    public List<Dvd> GetAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_Dvds, new DvdMapper());
    }

    @Override
    public Dvd GetById(int id) {
        try {
        return jdbcTemplate.queryForObject(SQL_SELECT_Dvd, new DvdMapper(), id);
        } catch (EmptyResultDataAccessException e) {
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Dvd Create(Dvd dto) {
        jdbcTemplate.update(SQL_INSERT_Dvd,
                dto.getTitle(),
                dto.getReleaseDate(),
                dto.getMpaaRating(),
                dto.getDirectorsName(),
                dto.getStudio(),
                dto.getUserNote());
        dto.setId(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return dto;
    }

    @Override
    public void Update(int id, Dvd dto) {
        jdbcTemplate.update(SQL_UPDATE_Dvd,
                dto.getTitle(),
                dto.getReleaseDate(),
                dto.getMpaaRating(),
                dto.getDirectorsName(),
                dto.getStudio(),
                dto.getUserNote(),
                dto.getId());
    }

    @Override
    public void Delete(int id) {
        jdbcTemplate.update(SQL_DELETE_Dvd, id);
    }

    @Override
    public List<Dvd> searchDvds(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return GetAll();
        }else{
            StringBuilder sQuery = new StringBuilder("select * from Dvd where ");
            
            int numParams = criteria.size();
            int paramPosition = 0;
            
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            
            Iterator<SearchTerm> iter = keySet.iterator();
            
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }
                
                sQuery.append(currentKey);
                sQuery.append(" = ? ");
                
                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new DvdMapper(), paramVals);
        }
    }
    
    private static final class DvdMapper implements RowMapper<Dvd> {

        @Override
        public Dvd mapRow(ResultSet rs, int i) throws SQLException {
            Dvd dto = new Dvd();
            dto.setId(rs.getInt("id"));
            dto.setTitle(rs.getString("title"));
            dto.setReleaseDate(rs.getString("releaseDate"));
            dto.setMpaaRating(rs.getString("mpaaRating"));
            dto.setDirectorsName(rs.getString("directorsName"));
            dto.setStudio(rs.getString("studio"));
            dto.setUserNote(rs.getString("userNote"));

            return dto;
        }

    }

}
