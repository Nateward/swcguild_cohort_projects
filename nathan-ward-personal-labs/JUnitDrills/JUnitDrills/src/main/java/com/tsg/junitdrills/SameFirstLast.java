/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrills;

/**
 *
 * @author apprentice
 */
public class SameFirstLast {

    public boolean SameFirstLast(int[] array) {
        if (array[array.length] > 1 ) {
            return array[0] == array[array.length-1];
        } else {
            return false;
        }
    }
}
