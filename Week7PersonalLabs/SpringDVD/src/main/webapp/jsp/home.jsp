<%-- 
    Document   : home
    Created on : 28-Mar-2016, 1:16:10 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/dvdvideo.jpg" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/displayDvdsList">Dvd List (No Ajax)</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h2>My Dvds</h2>
                    <table id="dvdTable" class="table table-hover">
                        <tr>
                            <th width="40%">Dvd Name</th>
                            <th width="30%">Director</th>
                            <th width="15%"></th>
                            <th width="15%"></th>
                        </tr>
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>
                <!-- start of add form -->
                <div class="col-md-6">
                    <h2 class="col-md-offset-4">Add New Dvd</h2>
                    <form class="form-horizontal" id="usrform" role="form">
                        <div class="form-group">
                            <label for="add-title" class="col-md-4 control-label" placeholder="Title">Title:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-title" placeholder="Title"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-release-date" class="col-md-4 control-label" placeholder="Release Date">Release Date:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-release-date" placeholder="Release Date"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-mpaaRating" class="col-md-4 control-label" placeholder="Rating">Rating:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-mpaaRating" placeholder="Rating"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-director" class="col-md-4 control-label">Director:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-director" placeholder="Director"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-studio" class="col-md-4 control-label">Studio:</label>
                            <div class="col-md-8">
                                <input type="tel" class="form-control" id="add-studio" placeholder="Studio"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-user-notes" class="col-md-4 control-label">Notes:</label>
                            <div class="col-md-8">
                                <textarea type="text" class="form-control" id="add-user-notes" form="usrform" placeholder="User Notes"></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-md-offset-10 col-md-2">
                                <button type="submit" id="add-button" class="btn btn-primary ">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="detailsModalLabel">Dvd Details</h4>
                    </div>
                    <div class="modal-body">
                        <h3 id="dvd-id"></h3>
                        <table class="table table-bordered">
                            <tr>
                                <th>Title:</th>
                                <td id="dvd-title"></td>
                            </tr>
                            <tr>
                                <th>Release:</th>
                                <td id="dvd-releaseDate"></td>
                            </tr>
                            <tr>
                                <th>MPAA Rating:</th>
                                <td id="dvd-rating"></td>
                            </tr>
                            <tr>
                                <th>Director:</th>
                                <td id="dvd-director"></td>
                            </tr>
                            <tr>
                                <th>Studio:</th>
                                <td id="dvd-studio"></td>
                            </tr>
                            <tr>
                                <th>Notes:</th>
                                <td id="dvd-notes"></td>
                            </tr>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-laballedby="editModalLabel" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">&times;</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title" id="editModalLabel">Edit Dvd</h4>
                    </div>
                    <div class="modal-body">
                        <input type="hidden" value="0" id="edit-id"/>
                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="edit-title" class="col-md-4 control-label" placeholder="Title">Title:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-title" placeholder="Casino Royale"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-releaseDate" class="col-md-4 control-label" placeholder="Release Date:">Release Date:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-releaseDate" placeholder="Release Date"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-rating" class="col-md-4 control-label" placeholder="MPAA Rating:">MPAA Rating:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-rating" placeholder="PG-13"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director" class="col-md-4 control-label">Director:</label>
                                <div class="col-md-8">
                                    <input type="text" class="form-control" id="edit-director" placeholder="Director"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-label">Studio:</label>
                                <div class="col-md-8">
                                    <input type="tel" class="form-control" id="edit-studio" placeholder="MGM"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-user-notes" class="col-md-4 control-label">Notes:</label>
                                <div class="col-md-8">
                                    <textarea class="form-control" id="edit-user-notes" form="usrform" placeholder="User Notes"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="pull-right">
                                    <button type="submit" id="edit-button" class="btn btn-primary" data-dismiss="modal">Edit Contact</button>
                                    <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="modal-footer">

                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
        <script src="${pageContext.request.contextPath}/js/dvdList.js"></script>
    </body>
</html>
