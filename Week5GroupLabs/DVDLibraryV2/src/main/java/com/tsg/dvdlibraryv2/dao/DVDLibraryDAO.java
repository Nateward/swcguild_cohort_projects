/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv2.dao;

import com.tsg.dvdlibraryv2.dto.DVD;

import java.util.ArrayList;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DVDLibraryDAO {

    Map<Integer, DVD> dvdList = new HashMap<>();
    Collection<DVD> dvds;
    private static final String DVD_FILE = "DVD.txt";
    static final String DELIMETER = "::";
    int index = 1;

    public void addDVD(DVD dvd) {
        dvd.setIndex(index);
        dvdList.put(index, dvd);

        index++;
    }

//    public void searchDVDByTitle(String title , String releseDate) {
//       dvdList.get(title);
//       if(index > 1)
//       {
//       dvdList.get(releseDate);
//       }
//
//    }
    public Collection<DVD> getAllDVDs() {
//          Set<Integer> keySet = dvdList.keySet();
//        Integer[] keyArray = new Integer[keySet.size()];
//        keyArray = keySet.toArray(keyArray);
//        return keyArray;  
        Collection<DVD> collectionOfDVDs = dvdList.values();

        return collectionOfDVDs;
    }

    public void removeDVD(Integer index) {
        dvdList.remove(index);
        //not right, i think
    }

    public ArrayList<DVD> getDVD(String titleRemove) {
        Collection<DVD> dvdValues = dvdList.values();
        ArrayList<DVD> returnedTitles = new ArrayList<>();
        for (DVD dvdValue : dvdValues) {
            if (dvdValue.getTitle().equalsIgnoreCase(titleRemove)) {
                returnedTitles.add(dvdValue);
            }
        }
        return returnedTitles;

    }

    public Collection<DVD> getDVD() {
        dvds = dvdList.values();
        return dvds;
    }

    public void writeLibrary() throws IOException {

        PrintWriter writer = new PrintWriter(new FileWriter(DVD_FILE));

        Collection<DVD> dvdListObjArray = dvdList.values();

        for (DVD dvd : dvdListObjArray) {

            writer.println(dvd.getTitle() + DELIMETER
                    + dvd.getReleaseDate() + DELIMETER
                    + dvd.getMpaaRating() + DELIMETER
                    + dvd.getDirectorsName() + DELIMETER
                    + dvd.getStudio() + DELIMETER
                    + dvd.getUserNote() + DELIMETER
                    + dvd.getIndex());

        }
        writer.flush();
        writer.close();

    }

    public void loadList() throws FileNotFoundException {

        Scanner sc = new Scanner(new BufferedReader(new FileReader(DVD_FILE)));
        String currentLine;
        String[] currentTokens;

        while (sc.hasNextLine()) {
            currentLine = sc.nextLine();
            currentTokens = currentLine.split(DELIMETER);

            DVD dvd = new DVD();
            dvd.setTitle(currentTokens[0]);
            dvd.setReleaseDate(currentTokens[1]);
            dvd.setMpaaRating(currentTokens[2]);
            dvd.setDirectorsName(currentTokens[3]);
            dvd.setStudio(currentTokens[4]);
            dvd.setUserNote(currentTokens[5]);
            dvd.setIndex(Integer.parseInt(currentTokens[6]));
            this.index = dvd.getIndex() + 1;
            dvdList.put(dvd.getIndex(), dvd);
        }
    }

}
