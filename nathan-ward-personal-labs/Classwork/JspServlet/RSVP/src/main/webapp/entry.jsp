<%-- 
    Document   : entry
    Created on : 24-Mar-2016, 1:46:32 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Home</title>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
    </head>
    <body>
        <div class='container-fluid'>
            <h1>I'm having a Party and you are invited!!!</h1>
            Can you attend?<br/>
            <form class="form-horizontal" action="RSVPServlet" method="POST">
                Yes<input type="radio" name="myAnswer" value="Yes" checked/>
                No<input type="radio" name="myAnswer" value="No" /><br/>
                Reason (if not attending):<br/>
                <select class="form-control" name="myReason">
                    <option value="Out of town">Out of town</option>
                    <option value="Scheduling conflict">Scheduling conflict</option>
                    <option value="I don't like you!">I don't like you!</option>
                </select>
                Notes:<br/>
                <input class="form-control" type="text" name="myNotes"/><br/>
                <input class="btn btn-success" type="submit" value="RSVP"/>
            </form>
        </div>
    </body>
</html>
