/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.warmup1;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class EnterYourAge {

    public static void main(String[] args) {
        int age = 0;
        String whatShouldBeDone;

       whatShouldBeDone = jobBasedOnAge("Please enter your age: ");
        System.out.println(whatShouldBeDone);

    }

    public static int enterAge(String userPrompt) {
        String ageEntered = "";
        int age = 0;
        Scanner keyboard = new Scanner(System.in);

        System.out.println(userPrompt);
        ageEntered = keyboard.nextLine();
        age = Integer.parseInt(ageEntered);

        return age;

    }

    public static String jobBasedOnAge(String userPrompt){
        int age = 0;
        int work = 18;
        int retirement = 65;
        String result = "";

        age = enterAge(userPrompt);

        if (age <= work) {
            result = "You should be in school!";
        }
        else if(age > 18 && age < retirement)
        {
            result = "You should be in work.";
        }
        else{
                result = "Enjoy retirement!";
                }

        return result;

    }

}
