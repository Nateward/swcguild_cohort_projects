/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbookv2;

import com.tsg.addressbookv2.dao.AddressBookDAO;
import com.tsg.addressbookv2.dto.Address;
import com.tsg.addressbookv2.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class AddressBookController {

    private AddressBookDAO addressBook = new AddressBookDAO();
    private ConsoleIO con = new ConsoleIO();

    public void run() {

        try {
            addressBook.loadAddressBook();
            con.print("Address book loaded.\n");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(AddressBookController.class.getName()).log(Level.SEVERE, null, ex);
            con.print("There was an error loading your address book.\n");
        }

        int userInput = 0;
        while (userInput != 7) {

            printMenu();
            userInput = con.getInteger("Please choose an option: ", 1, 7);
            switch (userInput) {
                case 1:
                    addAddress();
                    break;
                case 2:
                    printAddressByLastName();
                    break;
                case 3:
                    listNumberOfAddresses();
                    break;
                case 4:
                    listAllAddresses();
                    break;
                case 5:
                    removeAddress();
                    break;
                case 6:
                    saveFile();
                    break;
                default:
                    break;
            }
        }

        saveFile();

        con.print("Thank you for using Address Book 1.0.");

    }

    private void addAddress() {
        Address address = new Address();
        address.setFirstName(con.getString("First name: "));
        address.setLastName(con.getString("Last name: "));
        address.setStreetAddress(con.getString("Street address: "));
        address.setCity(con.getString("City: "));
        address.setState(con.getString("State: "));
        address.setZip(con.getInteger("Zip code: ", 0, 99999));
        addressBook.addAddress(address);
    }

    private void listAllAddresses() {
        ArrayList<Address> addresses = addressBook.findAllAddresses();
        for (Address address : addresses) {
            printAddress(address);
        }
    }

    public void printAddress(Address address) {
        con.print(address.getFirstName() + " " + address.getLastName());
        con.print(address.getStreetAddress());
        con.print(address.getCity() + ", " + address.getState() + "  " + address.getZip() + "\n");
    }

    private void removeAddress() {
        Address address = getAddressFromUser("Please enter last name:");
        if (address != null) {
            printAddress(address);
            if (con.getString("Are you sure? Y/N").equalsIgnoreCase("y")) {
                addressBook.deleteAddressbyId(address.getIdNumber());
                con.print(address.getFirstName() + " " + address.getLastName() + " removed succesfully.\n");
            } else {
                con.print("Removal canceled.\n");
            }
        } else {
            con.print("Removal canceled; name not found.\n");
        }
    }

    private Address getAddressFromUser(String prompt) {
        String userAddressLastName = con.getString(prompt);
        ArrayList<Address> addressChoices = addressBook.findAddressByLastName(userAddressLastName);
        if (addressChoices != null) {
            if (addressChoices.size() == 1) {
                return addressChoices.get(0);
            } else {
                con.print("More than one entry found:");
                for (int i = 0; i < addressChoices.size(); i++) {
                    Address address = addressChoices.get(i);
                    con.print((i + 1) + ": " + address.getFirstName() + " " + address.getLastName());
                }
                int userChoice = con.getInteger("Please select from above.", 1, addressChoices.size());
                return addressChoices.get(userChoice - 1);
            }
        } else {
            return null;
        }
    }

    private void printAddressByLastName() {
        Address address = getAddressFromUser("Please enter last name:");
        if (address != null) {
            printAddress(address);
        } else {
            con.print("Print canceled; name not found.\n");
        }

    }

    private void listNumberOfAddresses() {
        con.print("There are " + addressBook.size() + " addresses in the address book.");
    }

    private void saveFile() {
        con.print("Saving...");
        try {
            addressBook.writeAddressBook();
            con.print("Address book saved successfully!\n");
        } catch (IOException ex) {
            Logger.getLogger(AddressBookController.class.getName()).log(Level.SEVERE, null, ex);
            con.print("There was an error :(\n");
        }

    }

    private void printMenu() {
        con.print("1. Add address");
        con.print("2. Find address");
        con.print("3. Print number of addresses");
        con.print("4. List all addresses");
        con.print("5. Remove an address");
        con.print("6. Save file");
        con.print("7. Quit\n");
    }
}
