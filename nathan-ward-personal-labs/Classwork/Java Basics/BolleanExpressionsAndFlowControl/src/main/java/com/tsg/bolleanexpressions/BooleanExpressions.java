/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bolleanexpressions;

/**
 *
 * @author apprentice
 */
public class BooleanExpressions {
    public static void main(String[] args) {
        
        boolean test1 = 5 == 7;
        System.out.println("Is 5 == 7? " + test1);
        
        int operand1 = 42;
        
        System.out.println("Is " + operand1 + " !=21? " + test1);
        
        test1 = operand1 < 50;
        test1 = operand1 > 32;
        
        int operand2 = 96;
        test1 = operand2 >= operand1;
        test1 = 75 <= operand2;
        
        System.out.println("What is an oposite value of test1? " + !test1);
        
        test1 = !test1;
        
        boolean test2 = false;
        test1 = true;
        
        boolean result = test1 && test2;
        
        result = test1 || test2;
        
//        Xor ^ = false ^ true is true
        result = test1 ^ test2;
        
                
    }
    
}
