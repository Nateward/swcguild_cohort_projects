/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springvendingmachinemvc.dto;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class Change {

    private Integer totalInPennies = 0;

    private Integer userHasChange = 0;
    private Integer totalPennies;
    private Integer totalNickels;
    private Integer totalDimes;
    private Integer totalQuarters;
    private Integer totalDollars;
    // addToTotal(.99)
    // getTotal - expect .99

    /**
     *
     * @return total dollar amount in the machine as a double
     */
    public double getTotal() {
        double total = getTotalInPennies();
        total /= 100;
        return total;
    }

    /**
     *
     * @return total dollar amount in the machine as a double
     */
    public void getDollarsTotal() {
        double total = getTotalInPennies();
        this.totalDollars = (int) total / 100;
    }

    /**
     *
     * @return number of quarters that would be due to the customer as change in
     * current dollar amount held
     */
    public void getQuarters() {
        this.totalQuarters = getTotalInPennies() / 25;
    }

    /**
     *
     * @return number of dimes that would be due to the customer after quarters
     * are dispense
     */
    public void getDimes() {
        this.totalDimes = (getTotalInPennies() % 25) / 10;
    }

    /**
     *
     * @return number of nickels that would be due to the customer after
     * quarters & dimes are dispensed
     */
    public void getNickels() {
        this.totalNickels = ((getTotalInPennies() % 25) % 10) / 5;
    }

    /**
     *
     * @return number of pennies due to the customer after quarters, dimes,
     * nickels are dispensed
     */
    public void getPennies() {
        this.totalPennies = getTotalInPennies() % 5;
    }

    /**
     * adds addedMoney to total held in class; takes in double, converts to
     * integer (pennies)
     *
     * @param addedMoney double
     */
    public void addToTotal(double addedMoney) {
        int addedPennies = (int) (addedMoney * 100);
        this.setTotalInPennies((Integer) (this.getTotalInPennies() + addedPennies));

    }

    /**
     * reduces total by removedMoney
     *
     * @param removedMoney double
     */
    public void removeFromTotal(double removedMoney) {
        int removedPennies = (int) (removedMoney * 100);
        this.setTotalInPennies((Integer) (this.getTotalInPennies() - removedPennies));
    }

    /**
     * @return the totalInPennies
     */
    public Integer getTotalInPennies() {
        return totalInPennies;
    }

    /**
     * @param totalInPennies the totalInPennies to set
     */
    public void setTotalInPennies(Integer totalInPennies) {
        this.totalInPennies = totalInPennies;
    }

    /**
     * @return the userHasChange
     */
    public Integer getUserHasChange() {
        return userHasChange;
    }

    /**
     * @param userHasChange the userHasChange to set
     */
    public void setUserHasChange(Integer userHasChange) {
        this.userHasChange = userHasChange;
    }

    public Integer getTotalPennies() {
        return totalPennies;
    }

    public void setTotalPennies(Integer totalPennies) {
        this.totalPennies = totalPennies;
    }

    public Integer getTotalNickels() {
        return totalNickels;
    }

    public void setTotalNickels(Integer totalNickels) {
        this.totalNickels = totalNickels;
    }

    public Integer getTotalDimes() {
        return totalDimes;
    }

    public void setTotalDimes(Integer totalDimes) {
        this.totalDimes = totalDimes;
    }

    public Integer getTotalQuarters() {
        return totalQuarters;
    }

    public void setTotalQuarters(Integer totalQuarters) {
        this.totalQuarters = totalQuarters;
    }

    public Integer getTotalDollars() {
        return totalDollars;
    }

    public void setTotalDollars(Integer totalDollars) {
        this.totalDollars = totalDollars;
    }
    
    public void restUserChange(){
        setUserHasChange(null);
        setTotalDollars(null);
        setTotalQuarters(null);
        setTotalDimes(null);
        setTotalNickels(null);
        setTotalPennies(null);
    }
}
