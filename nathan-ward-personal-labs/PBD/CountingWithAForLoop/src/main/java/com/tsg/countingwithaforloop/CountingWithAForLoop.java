/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.countingwithaforloop;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CountingWithAForLoop {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        Random rnd = new Random();

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        String message = keyboard.nextLine();

        for (int n = 1; n <= 5; n++) {
            System.out.println(n + ". " + message);
        }

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        message = keyboard.nextLine();

        for (int n = 1; n <= 10; n++) {
            System.out.println(n + ". " + message);
        }

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        message = keyboard.nextLine();

        for (int n = 2; n <= 10; n = n + 2) {
            System.out.println(n + ". " + message);
        }

    }
}
