/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.statecapitals;

/**
 *
 * @author apprentice
 */
public class App {
    public static void main(String[] args) {
        StateAndCapital Test = new StateAndCapital();
        
        System.out.println("\nSTATES:\n============\n");
        Test.printStates();
        System.out.println("\nCapitals:\n=============\n");
        Test.printCapitals();
        System.out.println("\nSTATE/CAPITALS:\n==============\n");
        Test.printStateAndCapitals();
    }
}
