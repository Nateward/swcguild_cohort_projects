/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.dao;

import com.tsg.flooringmasteryspring.dto.Order;
import com.tsg.flooringmasteryspring.interfaces.OrdersInterface;
import com.tsg.flooringmasteryspring.ui.ConsoleIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class OrdersDAO implements OrdersInterface {

    HashMap<Integer, Order> orderList = new HashMap<>();
    ConsoleIO con = new ConsoleIO();
    private String date;
    private LocalDate whatDay = LocalDate.now();

    private Integer orderNumberIndex = 0;

    private final String ORDER_DELIMETER = ",";
    private String ORDER_FILE = "Order_" + getDate() + ".txt";

    public OrdersDAO() {
        whatDay = LocalDate.now();

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        this.date = whatDay.format(formatter);
        ORDER_FILE = "Order_" + this.date + ".txt";
        setORDER_FILE(ORDER_FILE);
        this.load(ORDER_FILE);
    }

//    public OrdersDAO(LocalDate date) {
//        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
//        this.date = date.format(formatter);
//        ORDER_FILE = "Order_" + this.date + ".txt";
//        this.load(ORDER_FILE);
//
//    }
    @Override
    public void createOrder(Order order) {

        order.setOrderDate(getDate());
        orderNumberIndex = orderList.size() + 1;
        order.setOrderNumber(this.orderNumberIndex);
        orderList.put(this.orderNumberIndex, order);
        this.orderNumberIndex++;
    }

    @Override
    public ArrayList<Order> getOrders() {

        Collection<Order> orders = orderList.values();

        ArrayList<Order> ordersResultList = new ArrayList<>();

        for (Order order : orders) {
            ordersResultList.add(order);
        }

        return ordersResultList;
    }

    @Override
    public Order getOrderByOrderNumber(Integer orderNumber) {
        return orderList.get(orderNumber);
    }

    @Override
    public void editOrder(Integer orderNumber, Order updatedOrder) {
        orderList.put(orderNumber, updatedOrder);
    }

    @Override
    public void deleteOrder(Integer orderId) {
        orderList.remove(orderId);

    }

    @Override
    public void load(String saveLocation) {

        try {
            Scanner sc;

            sc = new Scanner(new BufferedReader(new FileReader(saveLocation)));

            this.orderNumberIndex = 0;
            this.orderList = new HashMap<>();
            while (sc.hasNext()) {
                String currentLine = sc.nextLine();
                String[] tokens = currentLine.split(getORDER_DELIMETER());

                Order order = new Order();

                order.setOrderDate(getDate());
                order.setOrderNumber(Integer.parseInt(tokens[0]));
                order.setCustomerName(tokens[1]);
                order.setState(tokens[2]);
                order.setTaxRate(Double.parseDouble(tokens[3]));
                order.setProductType(tokens[4]);
                order.setArea(Double.parseDouble(tokens[5]));
                order.setLaborCostPerSqFt(Double.parseDouble(tokens[6]));
                order.setMaterialCostPerSqFt(Double.parseDouble(tokens[7]));
                order.setTotalCostPerSqFt(Double.parseDouble(tokens[8]));
                order.setTotalLaborCost(Double.parseDouble(tokens[9]));
                order.setTotalMaterialCost(Double.parseDouble(tokens[10]));
                order.setTax(Double.parseDouble(tokens[11]));
                order.setTotal(Double.parseDouble(tokens[12]));

                orderList.put(order.getOrderNumber(), order);
                if (this.orderNumberIndex <= order.getOrderNumber()) {

                    this.orderNumberIndex += 1;
                }

            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OrdersDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public void save(String saveLocation) {

        PrintWriter save = null;
        try {
            save = new PrintWriter(new FileWriter(saveLocation));
            Collection<Order> orders = orderList.values();
            for (Order order : orders) {
                save.println(order.getOrderNumber()
                        + getORDER_DELIMETER() + order.getCustomerName()
                        + getORDER_DELIMETER() + order.getState()
                        + getORDER_DELIMETER() + order.getTaxRate()
                        + getORDER_DELIMETER() + order.getProductType()
                        + getORDER_DELIMETER() + order.getArea()
                        + getORDER_DELIMETER() + order.getLaborCostPerSqFt()
                        + getORDER_DELIMETER() + order.getMaterialCostPerSqFt()
                        + getORDER_DELIMETER() + order.getTotalCostPerSqFt()
                        + getORDER_DELIMETER() + order.getTotalLaborCost()
                        + getORDER_DELIMETER() + order.getTotalMaterialCost()
                        + getORDER_DELIMETER() + order.getTax()
                        + getORDER_DELIMETER() + order.getTotal());

                save.flush();
            }
            save.close();
        } catch (IOException ex) {
            System.out.println("Sorry could not save 'Orders File'.");
            Logger.getLogger(OrdersDAO.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            save.close();
        }
    }

    /**
     * @return the ORDER_FILE
     */
    @Override
    public String getORDER_FILE() {
        return ORDER_FILE;
    }

    /**
     * @param ORDER_FILE the ORDER_FILE to set
     */
    @Override
    public void setORDER_FILE(String ORDER_FILE) {
        this.ORDER_FILE = ORDER_FILE;
    }

    /**
     * @return the ORDER_DELIMETER
     */
    @Override
    public String getORDER_DELIMETER() {
        return ORDER_DELIMETER;
    }

    /**
     * @return the date
     */
    @Override
    public String getDate() {
        return date;
    }

    /**
     * @param date the date to set
     */
    @Override
    public void setDate(String date) {
        this.date = date;
    }

    public LocalDate getWhatDay() {
        return whatDay;
    }

    public void setWhatDay(LocalDate whatDay) {

        orderList = new HashMap<>();
        this.whatDay = whatDay;

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        this.date = whatDay.format(formatter);
        ORDER_FILE = "Order_" + this.date + ".txt";
        this.load(ORDER_FILE);
    }

}
