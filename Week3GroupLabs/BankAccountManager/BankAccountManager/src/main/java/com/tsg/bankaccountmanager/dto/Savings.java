/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bankaccountmanager.dto;

/**
 *
 * @author apprentice
 */
public class Savings extends Account {

    private final Integer WITHDRAW_PENALTY = 50;

    @Override
    public void withdraw(Double withdrawAmount) {
        if (withdrawAmount > 0) {
            if ((withdrawAmount + WITHDRAW_PENALTY) <= availableBalance) {
                availableBalance = availableBalance - (withdrawAmount + WITHDRAW_PENALTY);
                totalBalance = totalBalance - (withdrawAmount + WITHDRAW_PENALTY);
            } else {
                System.out.println("Not enough funds.");
            }
        } else {
            System.out.println("You must enter a positive number.");
        }
        System.out.println("Your available balance is $" + this.availableBalance);
        System.out.println("Your total bank balance is $" + this.totalBalance + "\n");
    }
}
