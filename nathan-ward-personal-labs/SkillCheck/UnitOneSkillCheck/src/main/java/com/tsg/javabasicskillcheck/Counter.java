/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.javabasicskillcheck;

/**
 *
 * @author apprentice
 */
public class Counter {

    public static void main(String[] args) {
        to10();

        toN(3, 8, 200);
    }

    public static void to10() {
        for (int i = 1; i < 11; i++) {
            System.out.println(i);
        }
        System.out.println("\n");
    }

    public static void toN(int x, int y, int z) {
        for (int i = 1; i < (x + 1); i++) {
            System.out.println(i);
        }
        System.out.println("\n");

        for (int i = 1; i < (y + 1); i++) {
            System.out.println(i);
        }
        System.out.println("\n");
        for (int i = 1; i < (z + 1); i++) {
            System.out.println(i);
        }
    }
}
