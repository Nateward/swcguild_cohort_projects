/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.enteryourpin;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class EnterYourPin {
    public static void main( String[] args )
	{
		Scanner keyboard = new Scanner(System.in);
		int pin = 12345;

		System.out.println("WELCOME TO THE BANK OF MITCHELL.");
		System.out.print("ENTER YOUR PIN: ");
		int entry = keyboard.nextInt();

//                inside the ()'s is a statement that compares the variables entry and pin.
//                While variable entry is not equal to the variable pin, run what is in the while {}'s
//                their isn't an else if or else statement compared to a while loop
		while ( entry != pin )
		{
			System.out.println("\nINCORRECT PIN. TRY AGAIN.");
			System.out.print("ENTER YOUR PIN: ");
//                        3.) int type for entry is declared above the while loop.
//                        4.) if you delete "entry = keyboard.nextInt();" once you enter the wrong pin the while-loop lopp's forever'
			entry = keyboard.nextInt();
		}

		System.out.println("\nPIN ACCEPTED. YOU NOW HAVE ACCESS TO YOUR ACCOUNT.");
	}
    
}
