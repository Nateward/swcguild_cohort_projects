/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.inheritancespecialization.employee;

/**
 *
 * @author apprentice
 */
public class Employee {
    protected String name;
    private String ssn;
    
    public Employee(){
        name = "Jane Doe";
        ssn = "999-00-9999";
    }
    
    public Employee(String name, String ssn){
        this.name = name;
        this.ssn = ssn;
    }
    
    public void doWork(){
        System.out.println("I am working really hard! When is lunch?");
    }
    
    public void createObjectives(){
        System.out.println("Eat more sandwiches!!");
    }
    
    public void collectBonus(){
        System.out.println("Get 10% of your anual pay");
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * @return the ssn
     */
    public String getSsn() {
        return ssn;
    }

}
