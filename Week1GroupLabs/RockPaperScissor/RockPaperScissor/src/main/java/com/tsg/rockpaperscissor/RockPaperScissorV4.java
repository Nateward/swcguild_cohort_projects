package com.tsg.rockpaperscissor;


import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class RockPaperScissorV4 {

    public static void main(String[] args) {
        int playerChoice = 0;
        int numOfRound = 0;
        int totalRounds = 0;
        int win = 0;
        int loss = 0;
        int tie = 0;
        int goofs = 0;
        int outcome;

        Scanner keyboard = new Scanner(System.in);

        System.out.println("How many rounds would you like to play (choose between 1--10)? ");
        totalRounds = keyboard.nextInt();

        if (totalRounds < 1 || totalRounds > 10) {
            System.out.println("Well that wasn't a valid number of rounds");
            return;
        }
        while (numOfRound < totalRounds) {
            outcome = gameLogic();

            switch (outcome) {
                case 1:
                    win++;
                    break;
                case 2:
                    tie++;
                    break;
                case 3:
                    loss++;
                    break;
                default:
                    goofs++;
                    break;
            }

            numOfRound++;

            if (numOfRound == totalRounds) {
                System.out.println("\n You played " + numOfRound + " rounds. \n "
                        + "You won " + win + " times. \n You lost " + loss + " times."
                        + " \n We tied " + tie + " times.");
                if (goofs > 0) {
                    System.out.println("There were " + goofs + " errors on our fault. \n Please contact the louse that gave this to you so they can fix it.");
                }
                System.out.println("Would you like to play again?");
                System.out.println("\t 1)Yes!!!");
                System.out.println("\t 2)No");
                playerChoice = keyboard.nextInt();

                if (playerChoice == 1) {
                    System.out.println("How many rounds would you like to play again?");
                    totalRounds = keyboard.nextInt();
                    numOfRound = 0;
                    win = 0;
                    loss = 0;
                    tie = 0;
                    goofs = 0;
                } else {
                    System.out.println("Thanks for playing!");
                }
            }
        }
    }
    
    
    public static int gameLogic( ){
        Scanner keyboard = new Scanner(System.in);
        Random randomGenerator = new Random();
        int usersChoice, computersChoice, outcome;
        outcome = 0;
        
        
                System.out.println("Player choose your item: ");
                System.out.println("\t 1) Rock");
                System.out.println("\t 2) Paper");
                System.out.println("\t 3) Scissor\n");

                usersChoice = keyboard.nextInt();

                computersChoice = randomGenerator.nextInt(3) + 1;
              
        switch (usersChoice){
            case 1:
                switch (computersChoice){
                    case 1:
                    System.out.println("I picked a rock also. That's a tie. ");
                    outcome = 2;
                    break;
                
                    case 2 :
                    System.out.println("I picked paper. Get dunked on. Another loss for you.");    
                    outcome = 3;
                    break;
                    
                    case 3:
                    System.out.println("I picked scissors. Nerf rock. What a meaningless victory for you.");
                    outcome = 1;
                    break;
                    
                    default:
                    System.out.println("You dun goofed.");
                    outcome = 4;
                    break;
                }
                break;
            case 2: 
                switch (computersChoice){
                    case 1:
                    System.out.println("I picked a rock. Nerf paper. Enjoy your empty win.");    
                    outcome = 1;
                    break;
                    
                    case 2: 
                    System.out.println("I picked paper too. Real original. I 'guess' it's a tie.");
                    outcome = 2;
                    break;
                    
                    case 3:
                    System.out.println("I picked scissors. Get dunked on. I win again.");
                    outcome = 3;
                    break;
                    
                    default:
                    System.out.println("You dun goofed");
                    outcome = 4;
                    break;
                }
                break;
            case 3:
                switch(computersChoice){
                    case 1:
                    System.out.println("I picked a rock. All skill baby. Way to lose");  
                    outcome = 3;
                    break;
                    
                    case 2:
                    System.out.println("I picked paper. Way to pick an overpowered class. Enjoy your easy win.");    
                    outcome = 1;
                    break;
                    
                    case 3:
                    System.out.println("I picked scissors also. Guess that's a tie.");
                    outcome = 2;
                    break;
                    
                    default:
                    System.out.println("You dun goofed.");
                    outcome = 4;
                    break;
                } break;   
            default:
                System.out.println("You dun goofed");
                outcome = 4;
            }
            return outcome;  
        }           
    }
