<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>REST Dvd Library</title>
        <link href="css/bootstrap.min.css" rel="stylesheet">
        <link href="img/icon.png" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>REST Dvd Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="stats">Stats</a>
                    </li>
                </ul>
            </div>

            <!--Add a row to our container - this will hold the summary table and the new contact form.-->

            <div class="row">
                <!-- #2: Add a col to hold the summary table - have it take up half the row -->
                <div class="col-md-6">
                    <div id="dvdTableDiv">
                        <h2>My Dvds</h2>

                        <table id="dvdTable" class="table table-hover">
                            <tr>
                                <th width="40%">Dvd Name</th>
                                <th width="30%">Director</th>
                                <th width="15%"></th>
                                <th width="15%"></th>
                            </tr>
                            <!--
                             #3: This holds the list of contacts - we will add rows
                            dynamically
                             using jQuery
                            -->
                            <tbody id="contentRows"></tbody>
                        </table>
                    </div>
                </div> <!-- End col div -->
                <!--
                #4: Add col to hold the new contact form - have it take up the other half of the row
                -->
                <div class="col-md-6">

                    <div id="editFormDiv" style="display: none">
                        <h2 onclick="hideEditForm()">Edit Dvd</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-title" class="col-md-4 control-label">
                                    Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-title"
                                           placeholder="Title"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-releaseDate" class="col-md-4 control-label">
                                    Release Date:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-releaseDate"
                                           placeholder="Release Date"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-rating"
                                           placeholder="Rating"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-director" class="col-md-4 control-label">
                                    Director:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-director"
                                           placeholder="Director"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-studio" class="col-md-4 control-label">
                                    Studio:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-studio"
                                           placeholder="Studio"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="edit-notes" class="col-md-4 control-label">
                                    User Notes:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="edit-notes"
                                           placeholder="Thoughts on this movie..."/>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-4">
                                    <input type="hidden" id="edit-dvd-id">
                                    <button type="button"
                                            id="edit-cancel-button"
                                            class="btn btn-default"
                                            onclick="hideEditForm()">
                                        Cancel
                                    </button>
                                </div>
                                <div class="col-md-4">
                                    <button type="submit"
                                            id="edit-button"
                                            class="btn btn-default">
                                        Update Dvd
                                    </button>
                                </div>
                            </div>

                        </form>
                    </div>
                    <div id="addFormDiv">

                        <h2>Add New Contact</h2>

                        <form class="form-horizontal" role="form">
                            <div class="form-group">
                                <label for="add-title" class="col-md-4 control-label">
                                    Title:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-title"
                                           placeholder="Casino Royale"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-releaseDate" class="col-md-4 control-label">
                                    Release Date:
                                </label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-releaseDate"
                                           placeholder="2004"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-rating" class="col-md-4 control-label">
                                    MPAA Rating:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-rating"
                                           placeholder="MPAA Rating"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-director" class="col-md-4 control-label">
                                    Director:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-director"
                                           placeholder="James Bond"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-studio" class="col-md-4 control-label">
                                    Studio:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-studio"
                                           placeholder="MGM"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="add-notes" class="col-md-4 control-label">
                                    User Notes:</label>

                                <div class="col-md-8">
                                    <input type="text"
                                           class="form-control"
                                           id="add-notes"
                                           placeholder="Thoughts on this movie..."/>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-offset-4 col-md-8">
                                    <button type="submit"
                                            id="add-button"
                                            class="btn btn-default">
                                        Create Dvd
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div> <!-- End col div -->
            </div> <!-- End row div -->
        </div>
        <!-- #5: Placed at the end of the document so the pages load faster -->
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/restDvdList.js"></script>
    </body>
</html>