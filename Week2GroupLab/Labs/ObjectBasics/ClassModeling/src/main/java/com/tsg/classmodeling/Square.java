/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Square {
    private int side;
    private int angle;
    private int height;
    private float area;
    private float perimeter;

    public Square(int side, int angle, int height, float area, float perimeter) {
        this.side = side;
        this.angle = angle;
        this.height = height;
        this.area = area;
        this.perimeter = perimeter;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public int getAngle() {
        return angle;
    }

    public void setAngle(int angle) {
        this.angle = angle;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public float getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(float perimeter) {
        this.perimeter = perimeter;
    }
    
    //Behaviors
    
    
    /**
     * these are the behaviors of a square.
     */
    
    public void diagonal()
    {
        System.out.println("The diagonals of a square meets at a right angle");
    }
    
    public void angle()
    {
        System.out.println("All four angles of the square are equal");
    }
}
