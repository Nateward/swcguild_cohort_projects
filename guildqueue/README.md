# learn to build queue for a great win
1. Pull Queue repository:
		[http://ilyaGotfryd@bitbucket.thesoftwareguild.com/scm/~ilyagotfryd/guildqueue.git](http://ilyaGotfryd@bitbucket.thesoftwareguild.com/scm/~ilyagotfryd/guildqueue.git)
2. Load GuildQueue project and clean and build it
3. Create personal Maven Java App project, copy and adopt implementation of your queue by implementing
```
com.thesoftwareguild.queue.Queue<T>
```
 interface.
4. Add your name to the getAuthorName() method => return string literal.
5. Adopt your unit to the new interface or copy unit tests from GuildQueue project to exercise your queue.
6. Clean And Build
7. Commit your queue implementation to a repo and send me a link to your implementation in bitbucket.
> let's take it for a test drive.