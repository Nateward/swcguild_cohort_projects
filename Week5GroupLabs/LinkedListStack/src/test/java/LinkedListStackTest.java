/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.linkedliststack.LinkedListStackImpl;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LinkedListStackTest {

    public LinkedListStackTest() {
    }
    LinkedListStackImpl stack;

    @Before
    public void setUp() {
        stack = new LinkedListStackImpl();

    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void pushPopNormalTest() {
        //Arrange
        String a = "a";
        String b = "b";
        String c = "c";

        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);

        Object popedString1 = stack.pop();//1st pop c is taken out of stack
        Object popedString2 = stack.pop();//2nd pop, b is taken out of stack
        assertEquals(c, popedString1);
        assertEquals(b, popedString2);
    }

    @Test
    public void popTestForNull() {
        //Arrange

        //Act
        Object returnPopObject = stack.pop();
        assertTrue(returnPopObject == null);
    }

    @Test
    public void popTestForOneItem() {
        //Arrange
        String a = "a";
        stack.push(a);

        //Act
        Object popedString1 = stack.pop();//should return String a
        Object popedString2 = stack.pop();

        //Assert
        assertEquals(a, popedString1);
        assertEquals(null, popedString2);
    }

    @Test
    public void pushItemEqualsToArrayLenght() {
        //Arrange

        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);
        stack.push(d);
        stack.push(e);
        int size1 = stack.size();
        stack.pop();
        int size2 = stack.size();

        //Assert
        assertEquals(5, size1);
        assertEquals(4, size2);
    }

    @Test
    public void isEmptyTest() {
        //Arrange

        String a = "a";
        String b = "b";
        String c = "c";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);

        stack.pop();
        stack.pop();
        stack.pop();

        //Assert
        assertTrue(stack.isEmpty());
        stack.push(c);
        assertFalse(stack.isEmpty());
    }

    @Test
    public void iteratorTest() {
        //Arrange
        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);
        stack.push(d);
        stack.push(e);
        Iterator itr = stack.iterator();
        Object firstOut = itr.next();
        Object secondOut = itr.next();
        Object thirdOut = itr.next();
        Object fourthOut = itr.next();
        boolean hasNext1 = itr.hasNext();
        Object lastOut = itr.next();
        boolean hasNext2 = itr.hasNext();

        //assert
        assertEquals(e, firstOut);
        assertEquals(d, secondOut);
        assertTrue(hasNext1);
        assertEquals(b, fourthOut);
        assertEquals(a, lastOut);
        assertFalse(hasNext2);

    }
}
