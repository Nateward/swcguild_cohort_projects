/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.windowmaster;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasterV3 {

    public static void main(String[] args) {
        float height = 0;
        float width = 0;

        final float maxHeight = 25.5f;
        final float minHeight = 1.0f;
        final float maxWidth = 18.75f;
        final float minWidth = 1.0f;

        float areaOfWindow = 0;
        float cost = 0;
        float perimeterOfWindow = 0;

        height = rangeCheck(maxHeight, minHeight, "Please enter a window height between 1.0-25.5: ");
        width = rangeCheck(maxWidth, minWidth, "Please enter a window width between 1.0-18.75: ");

        areaOfWindow = height * width;
        perimeterOfWindow = 2 * (height + width);

        cost = ((3.50f * areaOfWindow) + (2.25f * perimeterOfWindow));

        System.out.println("Window height = " + height);
        System.out.println("Window width = " + width);
        System.out.println("Window area = " + areaOfWindow);
        System.out.println("Window preimeter = " + perimeterOfWindow);
        System.out.println("Total cost = " + cost);

    }



public static float getUserInput(String userPrompt) {
        String stringValue = "";
        float value = 0;
        Scanner sc = new Scanner(System.in);

        System.out.println(userPrompt);
        stringValue = sc.nextLine();
        value = Float.parseFloat(stringValue);

        return value;
    }

    public static float rangeCheck(float max, float min, String userInput) {
        float num = 0;
        do {
            num = getUserInput(userInput);
            }
        while(num > max || num < min);
        return num;
    }
    
}
