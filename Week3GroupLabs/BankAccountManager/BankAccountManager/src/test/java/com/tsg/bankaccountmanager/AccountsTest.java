/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bankaccountmanager;

import com.tsg.bankaccountmanager.dto.Account;
import com.tsg.bankaccountmanager.dto.Checking;
import com.tsg.bankaccountmanager.dto.Savings;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class AccountsTest {

    public AccountsTest() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    //junit -> create, deposit 500
    @Test// withdraw 450, total 50
    public void AccountWithdrawTest1() {
        Account account = new Account();
        account.deposit(500d);
        account.withdraw(450d);
        assertTrue(account.getAvailableBalance() == 50d);
        assertTrue(account.getTotalBalance() == 50d);
    }

    @Test// withdraw 550, total 500
    public void AccountWithdrawTest2() {
        Account account = new Account();
        account.deposit(500d);
        account.withdraw(550d);
        assertTrue(account.getAvailableBalance() == 500d);
        assertTrue(account.getTotalBalance() == 500d);
    }

    @Test // deposit 500, getbalance = 500, gettotal = 500
    public void AccountDepositTest1() {
        Account account = new Account();
        account.deposit(500d);
        assertTrue(account.getAvailableBalance() == 500d);
        assertTrue(account.getTotalBalance() == 500d);
    }

    @Test // deposit -500, getbalance = 0, gettotal = 0
    public void AccountDepositTest2() {
        Account account = new Account();
        account.deposit(-500d);
        assertTrue(account.getAvailableBalance() == 0);
        assertTrue(account.getTotalBalance() == 0);
    }

    @Test // deposit 20000, getbalance = 0, totalbalance = 20000
    public void AccountDepositTest3() {
        Account account = new Account();
        account.deposit(20000d);
        assertTrue(account.getAvailableBalance() == 0);
        assertTrue(account.getTotalBalance() == 20000d);
    }

    //junit -> create checking(), deposit 500
    @Test // withdraw 450, total 50
    public void CheckingWithdrawTest1() {
        Checking checking = new Checking();
        checking.deposit(500d);
        checking.withdraw(450d);
        assertTrue(checking.getAvailableBalance() == 50d);
        assertTrue(checking.getTotalBalance() == 50d);
    }

    @Test // withdraw 550, total -60
    public void CheckingWithdrawTest2() {
        Checking checking = new Checking();
        checking.deposit(500d);
        checking.withdraw(550d);
        assertTrue(checking.getAvailableBalance() == -60d);
        assertTrue(checking.getTotalBalance() == -60d);
    }

    @Test // withdraw 650, total 500
    public void CheckingWithdrawTest3() {
        Checking checking = new Checking();
        checking.deposit(500d);
        checking.withdraw(650d);
        assertTrue(checking.getAvailableBalance() == 500d);
        assertTrue(checking.getTotalBalance() == 500d);
    }

    //junit -> create savings(), deposit 500
    // withdraw 400, total 50
    public void SavingsWithdrawTest1() {
        Savings savings = new Savings();
        savings.deposit(500d);
        savings.withdraw(400d);
        assertTrue(savings.getAvailableBalance() == 50d);
        assertTrue(savings.getTotalBalance() == 50d);
    }

    // withdraw 550, total 500    
    public void SavingsWithdrawTest2() {
        Savings savings = new Savings();
        savings.deposit(500d);
        savings.withdraw(550d);
        assertTrue(savings.getAvailableBalance() == 500d);
        assertTrue(savings.getTotalBalance() == 500d);
    }
}
