/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbookv2.dao;

import com.tsg.addressbookv2.dto.Address;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class AddressBookV2DAOimpl implements AddressBookDAOInterface {

    ArrayList<Address> addressBook = new ArrayList<>();

    private Integer idIndex = 0;

    private static final String ADDRESS_FILE = "addressbook.txt";
    private final static String DELIMITER = "::";

    @Override
    public void addAddress(Address address) {

        address.setIdNumber(idIndex);
        addressBook.add(address);

        this.idIndex++;
    }

    @Override
    public Address removeAddress(Integer idIndex) {
        Address addressToDelete = new Address();
        for (int i = 0; i < addressBook.size(); i++) {
            if (addressBook.get(i).getIdNumber().equals(idIndex)) {
                addressToDelete = addressBook.get(i);
                addressBook.remove(addressBook.get(i));
            }
        }
        return addressToDelete;
    }

    @Override
    public ArrayList<Address> findAddressByLastName(String lastName) {
        ArrayList<Address> addressArrayList = new ArrayList<>();
        Collection<Address> addressCollection = addressBook.stream()
                .filter(a -> a.getLastName().contains(lastName))
                .collect(Collectors.toList());

        for (Address address : addressCollection) {
            addressArrayList.add(address);

        }
        return addressArrayList;
    }

    @Override
    public ArrayList<Address> findAddressByCity(String city) {
        ArrayList<Address> addressArrayList = new ArrayList<>();
        Collection<Address> addressCollection = addressBook.stream()
                .filter(a -> a.getCity().contains(city))
                .collect(Collectors.toList());

        for (Address address : addressCollection) {
            addressArrayList.add(address);

        }
        return addressArrayList;
    }

    @Override
    public Map<String, List<Address>> findAddressByState(String state) {
        Map<String, List<Address>> addressMap = addressBook.stream()
                .filter(addBook -> addBook.getState().equalsIgnoreCase(state))
                .collect(Collectors.groupingBy(a -> a.getCity()));

        return addressMap;
    }

    @Override
    public ArrayList<Address> findAddressByZip(Integer zip) {
        ArrayList<Address> addressArrayList = new ArrayList<>();
        Collection<Address> addressCollection = addressBook.stream()
                .filter(a -> a.getZip().equals(zip))
                .collect(Collectors.toList());

        for (Address address : addressCollection) {
            addressArrayList.add(address);

        }
        return addressArrayList;
    }

    @Override
    public ArrayList<Address> findAllAddresses() {
        ArrayList<Address> addressArrayList = new ArrayList<>();
        Collection<Address> addressCollection = addressBook.stream()
                .collect(Collectors.toList());

        for (Address address : addressCollection) {
            addressArrayList.add(address);

        }
        return addressArrayList;
    }

    @Override
    public Integer addressBookSize() {
        ArrayList<Address> addressArrayList = findAllAddresses();
        return addressArrayList.size();
    }
}
