/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dicedoubles;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class DiceDoubles {

    public static void main(String[] args) {
        int dice1;
        int dice2;

        Random r = new Random();

        dice1 = r.nextInt(6) + 1;
        dice2 = r.nextInt(6) + 1;

        while (dice1 != dice2) {

            dice1 = r.nextInt(6) + 1;
            dice2 = r.nextInt(6) + 1;

            System.out.println("Roll #1: " + dice1);
            System.out.println("Roll #2: " + dice2);
            System.out.println("Total: " + (dice2 + dice1));
        }
    }

}
