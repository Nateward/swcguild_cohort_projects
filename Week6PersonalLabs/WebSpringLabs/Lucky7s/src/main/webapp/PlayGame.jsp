<%-- 
    Document   : PlayGame
    Created on : 24-Mar-2016, 6:27:14 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Starting Bet</title>
    </head>
    <body>
        <h1>Please enter your starting bet.</h1>
        <form class="form-horizontal" action='Lucky7sServlet' method='POST'>
            <input class="form-control" type ="text" name="myRolls"/><br/>
            <input type="submit" class ="btn btn-success" value="Roll All The 7's"/>
        </form>
    </body>
</html>
