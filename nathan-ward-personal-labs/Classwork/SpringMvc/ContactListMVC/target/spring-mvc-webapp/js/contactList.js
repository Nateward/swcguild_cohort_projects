/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadContacts();

    $('#add-button').click(function (event) {

        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/ContactListMVC/contact',
            data: JSON.stringify({
                firstName: $('#add-first-name').val(),
                lastName: $('#add-last-name').val(),
                company: $('#add-company').val(),
                email: $('#add-email').val(),
                phone: $('#add-phone').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-first-name').val('');
            $('#add-last-name').val('');
            $('#add-company').val('');
            $('#add-email').val('');
            $('#add-phone').val('');
            loadContacts();
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: 'contact/' + $('#edit-contact-id').val(),
            data: JSON.stringify({
                firstName: $('#edit-firstName').val(),
                lastName: $('#edit-lastName').val(),
                company: $('#edit-company').val(),
                email: $('#edit-email').val(),
                phone: $('#edit-phone').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadContacts();
        });
    });
});


function loadContacts() {
    clearContactTable();

    var cTable = $("#contentRows");

    $.ajax({
        type: 'GET',
        url: "contacts"
    }).success(function (data, status) {
        $.each(data, function (index, contact) {

            cTable.append($('<tr>')
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-contact-id': contact.contactId,
                                'data-toggle': 'modal',
                                'data-target': '#detailsModal'
                            })
                            .text(contact.firstName + " " + contact.lastName)))
                    .append($('<td>').text(contact.company))
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-contact-id': contact.contactId,
                                'data-toggle': 'modal',
                                'data-target': '#editModal'
                            })
                            .text("Edit")))
                    .append($('<td>')
                            .append($('<a>').attr({
                                'onclick': 'deleteContact(' + contact.contactId + ')'}).text('Delete')))
                    );

        });
    });
}

function deleteContact(id) {
    var answer = confirm("Do you really want to delete this contact?");

    if (answer === true) {

        $.ajax({
            type: 'DELETE',
            url: 'contact/' + id
        }).success(function () {
            loadContacts();
        })

    }
}

function clearContactTable() {
    var contentRows = $("#contentRows");
    if ( contentRows.length == 0 ) throw "No Content Rows";
    contentRows.empty();

}

$('#detailsModal').on('show.bs.modal', function (event) {

    var element = $(event.relatedTarget);

    var contactId = element.data('contact-id');

    var modal = $(this);

    $.ajax({
        url: 'contact/' + contactId
    }).success(function (contact) {

        modal.find('#contact-id').text(contact.contactId);
        modal.find('#contact-firstName').text(contact.firstName);
        modal.find('#contact-lastName').text(contact.lastName);
        modal.find('#contact-company').text(contact.company);
        modal.find('#contact-phone').text(contact.phone);
        modal.find('#contact-email').text(contact.email);
    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);
    var contactId = element.data('contact-id');
    var modal = $(this);
    $.ajax({
        url: 'contact/' + contactId
    }).success(function (contact) {
        modal.find('#edit-contact-id').val(contact.contactId);
        modal.find('#contact-id').text(contact.contactId);
        modal.find('#edit-firstName').val(contact.firstName);
        modal.find('#edit-lastName').val(contact.lastName);
        modal.find('#edit-company').val(contact.company);
        modal.find('#edit-phone').val(contact.phone);
        modal.find('#edit-email').val(contact.email);
    });
});
    