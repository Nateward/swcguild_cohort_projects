/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.studentquizgrades;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class StudentGradesIO {

    ConsoleIO console = new ConsoleIO();

    HashMap<String, ArrayList<Double>> studentList = new HashMap<>();
    ArrayList<Double> quizscores = new ArrayList<>();
    Scanner sc = new Scanner(System.in);

    private String studentName;

    String userInput;
    int switchNumber;

    public StudentGradesIO() {
        studentList.put("Jacque Strappe", rndQuizes());
        studentList.put("Oliver Klozoff", rndQuizes());
        studentList.put("Seymour Butts", rndQuizes());
        studentList.put("Maya Buttreeks", rndQuizes());
        studentList.put("Hugh Jass", rndQuizes());

    }

    public ArrayList<Double> rndQuizes() {
        Random randomGenerator = new Random();
        ArrayList<Double> quizscores = new ArrayList<>();

        for (int i = 0; i < 3; i++) {
            double grade = randomGenerator.nextInt(101);
            quizscores.add(grade);
        }

        return quizscores;
    }

    public void QuizGradeLogic() {
        System.out.println("\n\n\n");
        System.out.println(" Welcome to the Student Grade Center");
        System.out.println("=============================================\n");

        while (switchNumber != 5) {

            System.out.println("\n Please choose what operation you would like to do:");
            System.out.println("\t 1.) Add a Student\n\t 2.) Remove a Student"
                    + "\n\t 3.) View List of Students\n\t 4.) View a Student\n\t 5.) Exit ");
            userInput = sc.nextLine();
            switchNumber = Integer.parseInt(userInput);

            switch (switchNumber) {
                case 1:
                    addStudent();
                    break;
                case 2:
                    removeStudent();
                    break;
                case 3:
                    viewStudents();
                    break;
                case 4:
                    viewAStudents();
                    break;
                case 5:
                    System.out.println("\n\nThank you!");
                    break;
                default:
                    System.out.println("\n\nYou picked something not in the list");
                    break;

            }
        }
    }

    public double printQuizAverage(String name) {
        Set<String> ListOfStudents = studentList.keySet();
        double x = 0;
        for (String key : ListOfStudents) {

            if (key.equalsIgnoreCase(name)) {
                ArrayList<Double> grades = studentList.get(key);
                for (int i = 0; i < grades.size(); i++) {
                    x = x + grades.get(i);
                }
                x = x / ListOfStudents.size();
            }

        }
        return x;
    }

    public void printAllAverageQuizScores() {
        Set<String> ListOfStudents = studentList.keySet();
        double x = 0;
        for (String key : ListOfStudents) {
            ArrayList<Double> grades = studentList.get(key);

            for (int i = 0; i < grades.size(); i++) {
                x = x + grades.get(i);
            }
            x = (x / ListOfStudents.size());

        }
        System.out.println(x / ListOfStudents.size());
    }

    public void addStudent() {
        ArrayList<Double> scores = new ArrayList<>();
        switchNumber = 0;
        while (switchNumber != 1) {
            System.out.println("\n\n Do you wish to continue?: ");
            System.out.println("\t 1.) Add Student\n\t 2.) Return to main menu");
            userInput = sc.nextLine();
            switchNumber = Integer.parseInt(userInput);
            switch (switchNumber) {
                case 1:
                    String name = console.method3("\nPlease enter the student's name : ");
                    scores = addQuizScores();
                    this.studentList.put(name, scores);
                    break;

                case 2:
                    QuizGradeLogic();
                    break;
                default:
                    System.out.println("\nYou picked something not in the list");
                    break;

            }
        }
    }

    public void viewStudents() {
        Set<String> values = studentList.keySet();

        for (String k : values) {
            System.out.println("_____________________________________________________________________________________________________________");

            System.out.println(k + " " + studentList.get(k));

        }
        System.out.println("_____________________________________________________________________________________________________________");
        System.out.println("\n Class Average: ");
        printAllAverageQuizScores();
        System.out.println("\n");
        System.out.println("_____________________________________________________________________________________________________________");

        System.out.println("\n\n");
        switchNumber = 0;

        System.out.println("Would you like to view a students quiz history?: ");
        System.out.println("\t 1.) Pick a  Student\n\t 2.) Return to main menu");
        userInput = sc.nextLine();
        switchNumber = Integer.parseInt(userInput);
        switch (switchNumber) {
            case 1:
                viewAStudents();
                break;

            case 2:
                QuizGradeLogic();
                break;
            default:
                System.out.println("You picked something not in the list");
                break;

        }

    }

    public void viewAStudents() {
        Set<String> values = studentList.keySet();

        String name = console.method3("\nPlease enter the student's name : ");
        double average;
        for (String key : values) {

            if (key.equalsIgnoreCase(name)) {

                System.out.println(key + " " + studentList.get(key));
                average = printQuizAverage(name);
                System.out.println("\n Students average quiz score: ");
                System.out.println(average);
            }

        }
    }

    public ArrayList addQuizScores() {
        ArrayList<Double> scores = new ArrayList<>();
        int numberOfQuizzes = console.method2("\nHow many quizzes would you like to enter?", 0, 100);
        for (int i = 0; i < numberOfQuizzes; i++) {
            scores.add(console.method7("Quiz #" + (i + 1), 0, 100));
        }
        return scores;
    }

    public void removeStudent() {
        String name = console.method3("\nPlease enter the student's name you would like to remove: ");
        
        System.out.println("Are you sure you would you like to remove" + name + "?: ");
        System.out.println("\t 1.) Yes. \n\t 2.) Return to main menu");
        userInput = sc.nextLine();
        switchNumber = Integer.parseInt(userInput);
        switch (switchNumber) {
            case 1:
                studentList.remove(name);
                break;

            case 2:
                QuizGradeLogic();
                break;
            default:
                System.out.println("You picked something not in the list");
                break;

        }
        
    }

    /**
     * @return the studentName
     */
    public String getStudentName() {
        return studentName;
    }

    /**
     * @param studentName the studentName to set
     */
    public void setStudentName(String studentName) {
        this.studentName = studentName;
    }
}

