/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.queueui.generics.ArrayQueueImpl_IlyasQueue;
import java.util.ArrayList;
import java.util.Iterator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class ArrayQueueJUnitTest {
    
    public ArrayQueueJUnitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void iteratorSnakeTest() {
        ArrayQueueImpl_IlyasQueue queue = new ArrayQueueImpl_IlyasQueue();
        ArrayList<Integer> inNumbers = new ArrayList<>();
        inNumbers.add(33);
        inNumbers.add(34);
        inNumbers.add(35);
        inNumbers.add(36);
        inNumbers.add(37);
        inNumbers.add(38);
        inNumbers.add(39);
        inNumbers.add(40);
        inNumbers.add(41);

        queue.enqueue(11);
        queue.printQueue();
        queue.enqueue(12);
        queue.printQueue();
        queue.enqueue(10);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(20);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(21);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(22);
        queue.printQueue();
        queue.enqueue(23);
        queue.printQueue();
        queue.enqueue(24);
        queue.printQueue();
        queue.enqueue(25);
        queue.printQueue();
        queue.enqueue(26);
        queue.printQueue();
        queue.enqueue(27);
        queue.printQueue();
        queue.enqueue(28);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(29);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(30);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(31);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(32);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(33);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(34);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(35);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(36);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(37);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(38);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(39);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(40);
        queue.printQueue();
        queue.dequeue();
        queue.printQueue();
        queue.enqueue(41);
        queue.printQueue();

        ArrayList<Integer> outNumbers = new ArrayList<>();

        Iterator iterator = queue.iterator();
        while (iterator.hasNext()) {

            String s = iterator.next().toString();
            outNumbers.add(Integer.parseInt(s));

        }

        Assert.assertEquals(inNumbers, outNumbers);

    }
}
