/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd.dao;

import com.tsg.springdvd.dto.Dvd;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface DvdDAO {
    
    //GetAll
    public List<Dvd> GetAll();
    //GetById
    public Dvd GetById(int id);
    //Create
    public Dvd Create(Dvd dto);
    //Update
    public void Update(int id, Dvd dto);
    //Delete
    public void Delete(int id);
    
    public List<Dvd> searchContacts(Map<SearchTerm, String> criteria);
    
}
