/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.windowmaster;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasterWithMethods {

    public static void main(String[] args) {
        float height = 0;
        float width = 0;

//        String stringHeight = "";
//        String stringWidth = "";

        final float maxHeight = 25.5f;
        final float minHeight = 1.0f;
        final float maxWidth = 25.5f;
        final float minWidth = 1.0f;

        float areaOfWindow = 0;
        float cost = 0;
        float perimeterOfWindow = 0;

//        Scanner sc = new Scanner(System.in);

//        System.out.println("Please enter window height: ");
//        stringHeight = sc.nextLine();

//--------Refactor Level I--------------------------------------------
//        height = getHeight();
//        
//        width = getWidth();

//-----------Refactor Level II-----------------------------------------
        String donkeyLobster = "Please enter window height: ";
        height = getUserInput(donkeyLobster);
        width = getUserInput("Please enter window width: ");

//        System.out.println("Please enter window width: ");
//        stringWidth = sc.nextLine();
//
//        height = Float.parseFloat(stringHeight);
//        width = Float.parseFloat(stringWidth);

        if (height > maxHeight || height < minHeight) {
            System.out.println("Height is out of range. Height should be between "
                    + minHeight + " - " + maxHeight);
        } else if (width > maxWidth || width < minWidth) {
            System.out.println("Width is out of range. Width should be between "
                    + minWidth + " - " + maxWidth);
        } else {

            areaOfWindow = height * width;
            perimeterOfWindow = 2 * (height + width);

            cost = ((3.50f * areaOfWindow) + (2.25f * perimeterOfWindow));

            System.out.println("Window height = " + height);
            System.out.println("Window width = " + width);
            System.out.println("Window area = " + areaOfWindow);
            System.out.println("Window preimeter = " + perimeterOfWindow);
            System.out.println("Total cost = " + cost);

        }

    }

    public static float getHeight() {
        String stringHeight = "";
        float height = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter window height: ");
        stringHeight = sc.nextLine();
        height = Float.parseFloat(stringHeight);
        
        return height;

    }
    
    public static float getWidth() {
        String stringWidth = "";
        float width = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter window width: ");
        stringWidth = sc.nextLine();
        width = Float.parseFloat(stringWidth);
        
        return width;
    }
    
    public static float getUserInput(String userPrompt){
        String stringValue = "";
        float value = 0;
        Scanner sc = new Scanner(System.in);
        
        System.out.println(userPrompt);
        stringValue = sc.nextLine();
        value = Float.parseFloat(stringValue);
        
        return value;
        
    }
}
