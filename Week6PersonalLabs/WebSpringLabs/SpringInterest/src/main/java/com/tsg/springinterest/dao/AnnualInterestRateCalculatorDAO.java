/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springinterest.dao;

import com.tsg.springinterest.dto.AnnualInterestEntry;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AnnualInterestRateCalculatorDAO {

    private List<AnnualInterestEntry> annualizedInterestList = null;

    private float interestRate = 0.0f;
    private float principal = 0.0f;
    private float compoundRate = 0.0f;
    private int years = 0;

    public AnnualInterestRateCalculatorDAO(float interestRate, float principal, float compoundRate, int years) {
        this.interestRate = interestRate;
        this.principal = principal;
        this.compoundRate = compoundRate;
        this.years = years;
        this.annualizedInterestList = null;
        this.annualizedInterestList = getAnnualizedInterestList();
    }

    //    This method and for-loop is for finding the compounded interest within a year
    private float compoundLoop(float money, float interestRate, float compoundRate) {
        float interestEarned = 0.0f;

        for (int i = 0; i < compoundRate; i++) {

            interestEarned = money * ((interestRate / 100) / compoundRate);
            money = (interestEarned + money);

        }
        return money;
    }

    private float earnInterest(float money, float principal) {

        float interestEarned = 0.0f;

        return interestEarned = money - principal;

    }

    /**
     * @return the annualizedInterestList
     */
    public List<AnnualInterestEntry> getAnnualizedInterestList() {
        if (annualizedInterestList == null) {
            annualizedInterestList = new ArrayList<AnnualInterestEntry>(100);
            float currentMoney = principal;
            for (int i = 0; i < years; i++) {
                AnnualInterestEntry aie = new AnnualInterestEntry();
                currentMoney = compoundLoop(currentMoney, interestRate, compoundRate);
                aie.setMoney(currentMoney);
                aie.setInterestEarned(earnInterest(currentMoney, principal));
                aie.setYear(i + 1);
                annualizedInterestList.add(aie);
            }
        }
        return annualizedInterestList;
    }

    /**
     * @return the interestRate
     */
    public float getInterestRate() {
        return interestRate;
    }

    /**
     * @param interestRate the interestRate to set
     */
    public void setInterestRate(float interestRate) {
        this.annualizedInterestList = null;
        this.interestRate = interestRate;
    }

    /**
     * @return the principal
     */
    public float getPrincipal() {
        return principal;
    }

    /**
     * @param principal the principal to set
     */
    public void setPrincipal(float principal) {
        this.annualizedInterestList = null;
        this.principal = principal;
    }

    /**
     * @return the years
     */
    public int getYears() {
        return years;
    }

    /**
     * @param years the years to set
     */
    public void setYears(int years) {
        this.annualizedInterestList = null;
        this.years = years;
    }

    /**
     * @return the compoundRate
     */
    public float getCompoundRate() {
        return compoundRate;
    }

    /**
     * @param compoundRate the compoundRate to set
     */
    public void setCompoundRate(float compoundRate) {
        this.annualizedInterestList = null;
        this.compoundRate = compoundRate;
    }

}
