/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab3consolecalculator;

import java.util.Scanner;

/**
 *
 * 
 * @author apprentice
 */
public class MathApp {

    public static void main(String[] args) {
        Scanner Sc = new Scanner(System.in);
        float firstNumber = 0.0f;
        float secondNumber = 0.0f;
        int userInput = 0;
        float results;
        MathLogic math = new MathLogic();

        System.out.println("Welcome to console calculator.");

        while (userInput != 5) {

            System.out.println("\n Please choose Operation you would like to do:");
            System.out.println("\t 1.) Add\n\t 2.) Substract\n\t 3.) Multiply\n\t 4.) Divide\n\t 5.) Exit ");
            userInput = Sc.nextInt();

            switch (userInput) {
                case 1:
                    System.out.println("\n Whats the first number you would like to use?");
                    firstNumber = Sc.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = Sc.nextFloat();
                    results = math.add(firstNumber, secondNumber);
                    break;
                case 2:
                    System.out.println("\n Whats the first number you would like to use in the calculator?");
                    firstNumber = Sc.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = Sc.nextFloat();
                    results = math.substract(firstNumber, secondNumber);
                    break;
                case 3:
                    System.out.println("\n Whats the first number you would like to use in the calculator?");
                    firstNumber = Sc.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = Sc.nextFloat();
                    results = math.multiply(firstNumber, secondNumber);
                    break;

                case 4:
                    System.out.println("\n Whats the first number you would like to use in the calculator?");
                    firstNumber = Sc.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = Sc.nextFloat();
                    results = math.divide(firstNumber, secondNumber);
                    break;
                case 5:
                    System.out.println("Thank you!");
                    break;
                default:
                    System.out.println("You picked something not in the list");
                    break;

            }
        }
    }

}
