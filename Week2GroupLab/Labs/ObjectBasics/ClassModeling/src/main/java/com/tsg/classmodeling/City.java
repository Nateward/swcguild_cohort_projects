/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class City {
 
    private String mayor;
    private int numPeople;
    private int numOfBusinesses;
    private int numOfHouses;

    public City(String mayor, int numOfHouses){
        
        this.mayor = mayor;
        this.numOfHouses = numOfHouses;
    
    }
    
    
    /**
     * @return the mayor
     */
    public String getMayor() {
        return mayor;
    }

    /**
     * @param mayor the mayor to set
     */
    public void setMayor(String mayor) {
        this.mayor = mayor;
    }

    /**
     * @return the numPeople
     */
    public int getNumPeople() {
        return numPeople;
    }

    /**
     * @param numPeople the numPeople to set
     */
    public void setNumPeople(int numPeople) {
        this.numPeople = numPeople;
    }
    
    //Behaviors
    ////////////
    
    
    public void citiesMood(){
        
    }
    
}
