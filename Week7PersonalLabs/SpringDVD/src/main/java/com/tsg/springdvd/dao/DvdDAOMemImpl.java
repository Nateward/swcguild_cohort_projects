/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd.dao;

import com.tsg.springdvd.dto.Dvd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class DvdDAOMemImpl implements DvdDAO {

    Map<Integer, Dvd> Dvds;

    public DvdDAOMemImpl() {
        this.Dvds = new HashMap<>();
    }

    @Override
    public List<Dvd> GetAll() {
        return Dvds.values().stream().collect(Collectors.toList());

    }

    @Override
    public Dvd GetById(int id) {
        Dvd result = null;
        for (Dvd dvd : Dvds.values()) {
            if (dvd.getId() == id) {
                result = dvd;
                break;
            }
        }
        return result;
    }

    @Override
    public Dvd Create(Dvd dto) {
        dto.setId(this.GetNextId());
        this.Dvds.put(dto.getId(), dto);
        return dto;
    }

    @Override
    public void Update(int id, Dvd dto) {
        if (Dvds.containsKey(id)) {
            Dvds.put(id, dto);
        }
    }

    @Override
    public void Delete(int id) {
        Dvds.remove(id);

    }

    @Override
    public List<Dvd> searchContacts(Map<SearchTerm, String> criteria) {
        String titleCriteria = criteria.get(SearchTerm.TITLE);
        String releaseDateCriteria = criteria.get(SearchTerm.RELEASEDATE);
        String mpaaCriteria = criteria.get(SearchTerm.MPAARATING);
        String directorCriteria = criteria.get(SearchTerm.DIRECTORSNAME);
        String studioCriteria = criteria.get(SearchTerm.STUDIO);
        String notesCriteria = criteria.get(SearchTerm.USERNOTE);

        Predicate<Dvd> titleFilter;
        Predicate<Dvd> releaseDateFilter;
        Predicate<Dvd> mpaaFilter;
        Predicate<Dvd> directorFilter;
        Predicate<Dvd> studioFilter;
        Predicate<Dvd> notesFilter;

        Predicate<Dvd> truePredicate = (c) -> {
            return true;
        };

        titleFilter = (titleCriteria == null || titleCriteria.isEmpty()) ? truePredicate : (c) -> c.getTitle().equals(titleCriteria);
        releaseDateFilter = (releaseDateCriteria == null || releaseDateCriteria.isEmpty()) ? truePredicate : (c) -> c.getReleaseDate().equals(releaseDateCriteria);
        mpaaFilter = (mpaaCriteria == null || mpaaCriteria.isEmpty()) ? truePredicate : (c) -> c.getMpaaRating().equals(mpaaCriteria);
        directorFilter = (directorCriteria == null || directorCriteria.isEmpty()) ? truePredicate : (c) -> c.getDirectorsName().equals(directorCriteria);
        studioFilter = (studioCriteria == null || studioCriteria.isEmpty()) ? truePredicate : (c) -> c.getStudio().equals(studioCriteria);
        notesFilter = (notesCriteria == null || notesCriteria.isEmpty()) ? truePredicate : (c) -> c.getUserNote().equals(notesCriteria);

        return Dvds.values().stream()
                .filter(titleFilter
                        .and(releaseDateFilter)
                        .and(mpaaFilter)
                        .and(directorFilter)
                        .and(studioFilter))
                .collect(Collectors.toList());
    }

    private Integer GetNextId() {
        Integer result = 0;
        for (Dvd dvd : Dvds.values()) {
            if (dvd.getId() > result) {
                result = dvd.getId();
            }
        }
        result++;
        return result;
    }

}
