/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.stillusingvariables;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class StillUsingVariables {
    public static void main(String[] args) {
        String name = "";
        int graduation = 0;
        
        Scanner userInput = new Scanner(System.in);
        
        System.out.println("Please enter your full name: ");
        name = userInput.nextLine();
        System.out.println("\n");
        System.out.println("Please enter the year you graduated: ");
        graduation = userInput.nextInt();
        System.out.println("\n");
        System.out.println("My name is " + name + " and I graduated in " + graduation);
        

        
    }
}
