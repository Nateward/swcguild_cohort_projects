/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springinterest;

import com.tsg.springinterest.dao.AnnualInterestRateCalculatorDAO;
import com.tsg.springinterest.dto.AnnualInterestEntry;
import java.io.IOException;
import java.text.NumberFormat;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@WebServlet(name = "InterestCalculatorServlet", urlPatterns = {"/InterestCalculatorServlet"})
public class InterestCalculatorServlet extends HttpServlet {


    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        RequestDispatcher rd = request.getRequestDispatcher("GetFigures.jsp");
        rd.forward(request, response);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        float interestRate = Float.parseFloat(request.getParameter("interest"));
        float principal = Float.parseFloat(request.getParameter("principal"));
        int years = Integer.parseInt(request.getParameter("timeFrame"));
        float compoundRate = Float.parseFloat(request.getParameter("compound"));

        AnnualInterestRateCalculatorDAO aircDAO = new AnnualInterestRateCalculatorDAO(interestRate, principal, compoundRate, years);
        List<AnnualInterestEntry> laie = aircDAO.getAnnualizedInterestList();
        
        request.setAttribute("figuresList", laie);
        request.setAttribute("finalMoney",laie.get(laie.size()-1).getMoney());
        
        RequestDispatcher rd = request.getRequestDispatcher("Result.jsp");
        rd.forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
