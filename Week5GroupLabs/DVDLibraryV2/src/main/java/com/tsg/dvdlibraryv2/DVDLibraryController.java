/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv2;

import com.tsg.dvdlibraryv2.dao.DVDLibraryDAO;
import com.tsg.dvdlibraryv2.dto.DVD;
import com.tsg.dvdlibraryv2.ui.ConsoleIO;
import java.io.IOException;

import java.util.ArrayList;
import java.util.Collection;

/**
 *
 * @author apprentice
 */
public class DVDLibraryController {

    ConsoleIO fido = new ConsoleIO();
    DVDLibraryDAO dvdList = new DVDLibraryDAO();

    void run() throws InterruptedException {
        try {
            dvdList.loadList();
            boolean runMenu = true;
            while (runMenu) {
                printMenu();
                Integer userInput = fido.readInt("Please enter a number for the menu above:", 1, 7);
                switch (userInput) {
                    case 1:
                        addDVD();
                        fido.print("Adding DVD to Collection.......");                        
                        Thread.sleep(600);
                        fido.print("DVD added to collection.......");
                        break;
                    case 2:
                        removeDVD();
                        fido.print("Deleting DVD f Collection.......");                        
                        Thread.sleep(600);
                        fido.print("DVD removed from collection");
                        break;
                    case 3:
                        fido.print("Listing All DVD from collection.....");
                        Thread.sleep(600);
                        showAllDVD();                        
                        break;
                    case 4:
                        fido.print("Displaying infromation of given DVD....");
                        Thread.sleep(600);
                        searchDVDByTitle();
                        
                        break;
                    case 5:
                        editDVD();
                        Thread.sleep(600);
                        fido.print("Editing DVD by title");
                        
                        
                        
                        break;
                    case 6:
                        fido.print("Quitting......");
                        Thread.sleep(600);
                        runMenu = false;
                        break;
                }
            }
            dvdList.writeLibrary();
        } catch (IOException e) {
            System.out.println("File not found ... ");
        } catch (InterruptedException e) {
            System.out.println("Interrupted exception");
        }
    }

    private void printMenu() {

        fido.print("1) Add a DVD to collection");
        fido.print("2) Remove a DVD from collection");
        fido.print("3) List all the DVDs from the collection");
        fido.print("4) Search a DVD by title");
        fido.print("5) Edit a DVD by title");
        fido.print("6) Exit");
    }

    private void addDVD() {
        fido.print("Enter information of your DVD you want to add ");
        String title = fido.readString("Title:");
        String releaseDate = fido.readString("Release Date:");
        String ratings = fido.readString("MPAA Ratings:");
        String directorName = fido.readString("Director:");
        String Studio = fido.readString("Studio:");
        String userNote = fido.readString("Notes:");

        DVD dvd = new DVD();
        dvd.setTitle(title);
        dvd.setReleaseDate(releaseDate);
        dvd.setMpaaRating(ratings);
        dvd.setDirectorsName(directorName);
        dvd.setStudio(Studio);
        dvd.setUserNote(userNote);

        dvdList.addDVD(dvd);
    }

    private void removeDVD() {
        String titleRemove = fido.readString("Please enter the title you want to delete.");
        ArrayList<DVD> dvdArrayList = dvdList.getDVD(titleRemove);

        DVD dvd;
        if (dvdArrayList.isEmpty()) {
            fido.print("No DVD with title " + titleRemove + " exist.");

        } else {
            //iterating through the arraylist to get the DVD object
            if (dvdArrayList.size() > 1) {
                //if more than one title exist loop through and let user see all the options
                dVDInfo(dvdArrayList);

                Integer userSelect = fido.readInt("There are " + dvdArrayList.size() + " records of " + titleRemove
                        + " which one do you want to select.", 1, dvdArrayList.size());
                userSelect = userSelect - 1; //index is from 1 to no. of DVD inside the arraylist. So userSelect has to match the 0 index of arraylist
                //select the DVD object user wants to select
                dvd = dvdArrayList.get(userSelect);
            } else {
                dvd = dvdArrayList.get(0);
            }
            //As user if this is the title you want to delete
            String userConfirmation = fido.readString("Are you sure you want to delete " + dvd.getTitle() + " which was released in " + dvd.getReleaseDate());
            if (userConfirmation.equalsIgnoreCase("yes")) {
                Integer index = dvd.getIndex();
                dvdList.removeDVD(index);
            } else {
                fido.print("Returning to main menu.......");
            }

        }

    }

    private void searchDVDByTitle() {

        String titleSearch = fido.readString("Please enter the title you want to Search.");
        ArrayList<DVD> dvdArrayList = dvdList.getDVD(titleSearch);

        DVD dvd;
        if (dvdArrayList.isEmpty()) {
            fido.print("No DVD with title " + titleSearch + " exist.");

        } else {
            if (dvdArrayList.size() == 1) {
                fido.print("We found 1 record with title " + titleSearch);
            } else {
                fido.print("We found " + dvdArrayList.size() + " record with title " + titleSearch);
            }

            dVDInfo(dvdArrayList);

        }
    }

    private void showAllDVD() {

        if (dvdList.getDVD().isEmpty()) {
            fido.print("Your DVD library is empty!");
        } else {
            Collection<DVD> dvdCollection = dvdList.getDVD();
            ArrayList<DVD> dvdArrayList = new ArrayList<>();
            for (DVD dvd : dvdCollection) {
                dvdArrayList.add(dvd);
            }
            dVDInfo(dvdArrayList);

        }
    }

    private void editDVD() {
        String titleRemove = fido.readString("Please enter the title you want to edit.");
        ArrayList<DVD> dvdArrayList = dvdList.getDVD(titleRemove);

        DVD dvd;
        if (dvdArrayList.isEmpty()) {
            fido.print("No DVD with title " + titleRemove + " exist.");
            //return;
        } else {
            //iterating through the arraylist to get the DVD object
            if (dvdArrayList.size() > 1) {
                //if more than one title exist loop through and let user see all the options

                dVDInfo(dvdArrayList);
                Integer userSelect = fido.readInt("There are " + dvdArrayList.size() + " records of " + titleRemove
                        + " which one do you want to select.", 1, dvdArrayList.size());
                userSelect = userSelect - 1; //index is from 1 to no. of DVD inside the arraylist. So userSelect has to match the 0 index of arraylist
                //select the DVD object user wants to select
                dvd = dvdArrayList.get(userSelect);
            } else {
                dvd = dvdArrayList.get(0);
            }
            //As user if this is the title you want to edit
            String userConfirmation = fido.readString("Are you sure you want to Edit " + dvd.getTitle() + " which was released in " + dvd.getReleaseDate());
            if (userConfirmation.equalsIgnoreCase("yes")) {
                String title = fido.readString("Title:");
                String releaseDate = fido.readString("Release Date:");
                String ratings = fido.readString("MPAA Ratings:");
                String directorName = fido.readString("Director:");
                String Studio = fido.readString("Studio:");
                String userNote = fido.readString("Notes:");

                dvd.setTitle(title);
                dvd.setReleaseDate(releaseDate);
                dvd.setMpaaRating(ratings);
                dvd.setDirectorsName(directorName);
                dvd.setStudio(Studio);
                dvd.setUserNote(userNote);
                dvdList.addDVD(dvd);
            } else {
                fido.print("Returning to main menu.......");
            }

        }
    }

    private void dVDInfo(ArrayList<DVD> dvdArrayList) {

        for (int i = 0; i < dvdArrayList.size(); i++) {

            fido.print("\n#" + (i + 1) + "");
            fido.print("Title       : " + dvdArrayList.get(i).getTitle());
            fido.print("Release Date: " + dvdArrayList.get(i).getReleaseDate());
            fido.print("MPAA Ratings: " + dvdArrayList.get(i).getMpaaRating());
            fido.print("Studio      : " + dvdArrayList.get(i).getStudio());
            fido.print("Director    : " + dvdArrayList.get(i).getDirectorsName());
            fido.print("Note        : " + dvdArrayList.get(i).getUserNote() + "\n");
        }
    }

}
