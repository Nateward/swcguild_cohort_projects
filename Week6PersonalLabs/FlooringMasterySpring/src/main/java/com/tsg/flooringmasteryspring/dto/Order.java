/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.dto;

import java.util.*;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class Order {
    
    private String orderDate;
    private Integer orderNumber;
    private String customerName;
    private String state;
    private Double taxRate;
    private String productType;
    private Double area;
    private Double laborCostPerSqFt;
    private Double materialCostPerSqFt;
    private Double totalCostPerSqFt;
    private Double totalLaborCost;
    private Double totalMaterialCost;
    private Double tax;
    private Double total;

    /**
     * @return the orderDate
     */
    public String getOrderDate() {
        return orderDate;
    }

    /**
     * @param orderDate the orderDate to set
     */
    public void setOrderDate(String orderDate) {
        this.orderDate = orderDate;
    }

    /**
     * @return the orderNumber
     */
    public Integer getOrderNumber() {
        return orderNumber;
    }

    /**
     * @param orderNumber the orderNumber to set
     */
    public void setOrderNumber(Integer orderNumber) {
        this.orderNumber = orderNumber;
    }

    /**
     * @return the customerName
     */
    public String getCustomerName() {
        return customerName;
    }

    /**
     * @param customerName the customerName to set
     */
    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    /**
     * @return the state
     */
    public String getState() {
        return state;
    }

    /**
     * @param state the state to set
     */
    public void setState(String state) {
        this.state = state;
    }

    /**
     * @return the taxRate
     */
    public Double getTaxRate() {
        return taxRate;
    }

    /**
     * @param taxRate the taxRate to set
     */
    public void setTaxRate(Double taxRate) {
        this.taxRate = taxRate;
    }

    /**
     * @return the productType
     */
    public String getProductType() {
        return productType;
    }

    /**
     * @param productType the productType to set
     */
    public void setProductType(String productType) {
        this.productType = productType;
    }

    /**
     * @return the area
     */
    public Double getArea() {
        return area;
    }

    /**
     * @param area the area to set
     */
    public void setArea(Double area) {
        this.area = area;
    }

    /**
     * @return the laborCostPerSqFt
     */
    public Double getLaborCostPerSqFt() {
        return laborCostPerSqFt;
    }

    /**
     * @param laborCostPerSqFt the laborCostPerSqFt to set
     */
    public void setLaborCostPerSqFt(Double laborCostPerSqFt) {
        this.laborCostPerSqFt = laborCostPerSqFt;
    }

    /**
     * @return the materialCostPerSqFt
     */
    public Double getMaterialCostPerSqFt() {
        return materialCostPerSqFt;
    }

    /**
     * @param materialCostPerSqFt the materialCostPerSqFt to set
     */
    public void setMaterialCostPerSqFt(Double materialCostPerSqFt) {
        this.materialCostPerSqFt = materialCostPerSqFt;
    }

    /**
     * @return the totalCostPerSqFt
     */
    public Double getTotalCostPerSqFt() {
        return totalCostPerSqFt;
    }

    /**
     * @param totalCostPerSqFt the totalCostPerSqFt to set
     */
    public void setTotalCostPerSqFt(Double totalCostPerSqFt) {
        this.totalCostPerSqFt = totalCostPerSqFt;
    }

    /**
     * @return the totalLaborCost
     */
    public Double getTotalLaborCost() {
        return totalLaborCost;
    }

    /**
     * @param totalLaborCost the totalLaborCost to set
     */
    public void setTotalLaborCost(Double totalLaborCost) {
        this.totalLaborCost = totalLaborCost;
    }

    /**
     * @return the totalMaterialCost
     */
    public Double getTotalMaterialCost() {
        return totalMaterialCost;
    }

    /**
     * @param totalMaterialCost the totalMaterialCost to set
     */
    public void setTotalMaterialCost(Double totalMaterialCost) {
        this.totalMaterialCost = totalMaterialCost;
    }

    /**
     * @return the tax
     */
    public Double getTax() {
        return tax;
    }

    /**
     * @param tax the tax to set
     */
    public void setTax(Double tax) {
        this.tax = tax;
    }

    /**
     * @return the total
     */
    public Double getTotal() {
        return total;
    }

    /**
     * @param total the total to set
     */
    public void setTotal(Double total) {
        this.total = total;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 17 * hash + Objects.hashCode(this.orderDate);
        hash = 17 * hash + Objects.hashCode(this.orderNumber);
        hash = 17 * hash + Objects.hashCode(this.customerName);
        hash = 17 * hash + Objects.hashCode(this.state);
        hash = 17 * hash + Objects.hashCode(this.taxRate);
        hash = 17 * hash + Objects.hashCode(this.productType);
        hash = 17 * hash + Objects.hashCode(this.area);
        hash = 17 * hash + Objects.hashCode(this.laborCostPerSqFt);
        hash = 17 * hash + Objects.hashCode(this.materialCostPerSqFt);
        hash = 17 * hash + Objects.hashCode(this.totalCostPerSqFt);
        hash = 17 * hash + Objects.hashCode(this.totalLaborCost);
        hash = 17 * hash + Objects.hashCode(this.totalMaterialCost);
        hash = 17 * hash + Objects.hashCode(this.tax);
        hash = 17 * hash + Objects.hashCode(this.total);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Order other = (Order) obj;
        if (!Objects.equals(this.orderDate, other.orderDate)) {
            return false;
        }
        if (!Objects.equals(this.customerName, other.customerName)) {
            return false;
        }
        if (!Objects.equals(this.state, other.state)) {
            return false;
        }
        if (!Objects.equals(this.productType, other.productType)) {
            return false;
        }
        if (!Objects.equals(this.orderNumber, other.orderNumber)) {
            return false;
        }
        if (!Objects.equals(this.taxRate, other.taxRate)) {
            return false;
        }
        if (!Objects.equals(this.area, other.area)) {
            return false;
        }
        if (!Objects.equals(this.laborCostPerSqFt, other.laborCostPerSqFt)) {
            return false;
        }
        if (!Objects.equals(this.materialCostPerSqFt, other.materialCostPerSqFt)) {
            return false;
        }
        if (!Objects.equals(this.totalCostPerSqFt, other.totalCostPerSqFt)) {
            return false;
        }
        if (!Objects.equals(this.totalLaborCost, other.totalLaborCost)) {
            return false;
        }
        if (!Objects.equals(this.totalMaterialCost, other.totalMaterialCost)) {
            return false;
        }
        if (!Objects.equals(this.tax, other.tax)) {
            return false;
        }
        if (!Objects.equals(this.total, other.total)) {
            return false;
        }
        return true;
    }
    
}
