/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.objectexamples;

/**
 *
 * @author apprentice
 */
public class Dog {
    
    private String furColor;
    private String breed;
    private String dogName;
    private int age;
    private float weight;
    private boolean isAlive;
    
    public Dog(String dogName, int age, float weight)
    {
        isAlive = true;
    }
    
    public String getDogName()
    {
        return dogName;
        
    }
    
    public void setDogName(String dogName)
    {
        this.dogName = dogName;
    }
    
    public int getAge()
    {
        return age;
    }
    
    public void setAge(int age)
    {
        this.age = age;
    }
    
    public float getWeight()
    {
        return weight;
    }
    
    /**
     * setting weight
     *  @param weight This will set weight of your dog as a float.
     **
     */
    public void setWeight(float weight)
    {
        this.weight = weight;
    }
    
    /**
     * This method will force your instance of a dog to sit
     */
    public void bark()
    {
        System.out.println("says: WOOF!!!");
    }
    
    
    public void sit()
    {
        System.out.println("Sitting.....");   
    }

    /**
     * @return the furColor
     */
    public String getFurColor() {
        return furColor;
    }

    /**
     * @param furColor the furColor to set
     */
    public void setFurColor(String furColor) {
        this.furColor = furColor;
    }

    /**
     * @return the breed
     */
    public String getBreed() {
        return breed;
    }

    /**
     * @param breed the breed to set
     */
    public void setBreed(String breed) {
        this.breed = breed;
    }

    /**
     * @return the isAlive
     */
    public boolean isIsAlive() {
        return isAlive;
    }

    /**
     * @param isAlive the isAlive to set
     */
    public void setIsAlive(boolean isAlive) {
        this.isAlive = isAlive;
    }
}
