package com.tsg.openweathermap;

import java.util.Map;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping({"/"})
public class HelloController {
        
    public HelloController() {
    }
    
    @RequestMapping(value="/", method=RequestMethod.GET)
    public String Home() {
        return "home";
    }
    
    @RequestMapping(value="/hello", method=RequestMethod.GET)
    public String Hello() {
        return "hello";
    }
}
