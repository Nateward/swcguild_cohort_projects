/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.interfaceexamples;

/**
 *
 * @author apprentice
 */
public class MyDebugLogger implements DebugLogging, Colorable{

    @Override
    public void displayStatus(String id) {
        System.out.println("ID of status goig into file " + id);  
    }

    @Override
    public void displayError(String error) {
                System.out.println("Error1: " + error);  

    }

    @Override
    public void logStatus(String id) {
        System.out.println("Status2: " + id);
    }

    @Override
    public void logError(String id) {
        System.out.println("Error2: " + id);
    }

    @Override
    public void setColor(String color) {
        System.out.println("Color1: " + color);
    }

    @Override
    public String getColor() {
        System.out.println("Color2: " + color);
    }
    
}
