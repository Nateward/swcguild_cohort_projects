/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv2.dao;

import com.tsg.dvdlibraryv2.dto.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibraryImpl  implements DVDLibraryV2Interface{
    ArrayList<DVD> dvdList = new ArrayList<>();
    Integer index = 0;

    @Override
    public void addDVD(DVD dvd) {
        dvd.setIndex(index);
        dvdList.add(dvd);
        index++;
    }

    @Override
    public DVD removeDVD(Integer index) {
        DVD dvd = new DVD();
        for (int i = 0; i < dvdList.size(); i++) {
            if (dvdList.get(i).getIndex().equals(index)) {
                dvd = dvdList.remove(i);
            }           
            
        }
        return dvd;
    }

    @Override
    public Collection<DVD> getDVDByTitle(String title) {
        
        Collection<DVD> dvdListByTitle = dvdList.stream()
                .filter(s ->s.getTitle().equalsIgnoreCase(title))
                .collect(Collectors.toList());
        return dvdListByTitle;
    }

    @Override
    public Collection<DVD> getDVDByMPAA(String MPAARating) {
        Collection<DVD> dvdListByMpaa = dvdList.stream()
                .filter(s ->s.getMpaaRating().equalsIgnoreCase(MPAARating))
                .collect(Collectors.toList());
        return dvdListByMpaa;
    }

    @Override
    public Map<String,List<DVD>> getDVDByDirector(String director) {
        Map<String,List<DVD>> directorNameByMPAA = dvdList.stream()
                .filter(l -> l.getDirectorsName().equalsIgnoreCase(director))
                .collect(Collectors.groupingBy(s ->s.getMpaaRating()));
        
        return directorNameByMPAA;
    }

    @Override
    public Collection<DVD> getDVDByStudio(String studio) {
        Collection<DVD> dvdListByStudio = dvdList.stream()
                .filter(s ->s.getStudio().equalsIgnoreCase(studio))
                .collect(Collectors.toList());
        return dvdListByStudio;
    }

    @Override
    public Collection<DVD> getNewestDVD() {
       /* dvdList.stream()
                .sorted((DVD a,DVD b)->{
                    if(Integer.parseInt(a.getReleaseDate())<Integer.parseInt(b.getReleaseDate())){
                        return -1;
                    }else if(Integer.parseInt(a.getReleaseDate())==Integer.parseInt(b.getReleaseDate())){
                        return 0;
                    }else{
                        return 1;
                    }
                }                
                );
        ArrayList<DVD> newDvds = new ArrayList<>();
        
        for (int i = 0; i < dvdList.size(); i++) {
            if (dvdList.get(i).getReleaseDate().equals(dvdList.get(0).getReleaseDate())) {
                newDvds.add(dvdList.get(i));
            }
        }*/
        
     int newestYear = dvdList.stream()
             .mapToInt(s -> Integer.parseInt(s.getReleaseDate()))
             .max().getAsInt();
     
     Collection<DVD> dvdCollection = dvdList.stream()
             .filter(s ->Integer.parseInt(s.getReleaseDate())==newestYear)
             .collect(Collectors.toList());   
                
          
                
       return dvdCollection;
   }

    @Override
    public Collection<DVD> getOldestDVD() {
        int oldestYear = dvdList.stream()
             .mapToInt(s -> Integer.parseInt(s.getReleaseDate()))
             .min().getAsInt();
      return  dvdList.stream()
                .filter(dvd -> Integer.parseInt(dvd.getReleaseDate())==oldestYear)
                .collect(Collectors.toList());
        
    }

    @Override
    public double getAverageNoOfNotesAssociate() {
        Collection<DVD> numOfDVDs = dvdList.stream()
                .collect(Collectors.toList());
        
        Collection<DVD> numOfNotes = dvdList.stream()
                .filter(n -> n.getUserNote() != null)
                .collect(Collectors.toList());
        
        double total = numOfDVDs.size();
        double totalNotes = numOfNotes.size();
        return (totalNotes / total);
    }
    
}
