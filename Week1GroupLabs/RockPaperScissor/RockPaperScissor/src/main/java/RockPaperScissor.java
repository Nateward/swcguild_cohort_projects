
import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class RockPaperScissor {
    public static void main(String[] args) {
        int rock, paper, scissor, computerChoice, playerChoice;
        
        rock = 1;
        paper = 2;
        scissor = 3;
        computerChoice = 0;
        playerChoice = 0;
        
        String result = "";
        
        Scanner keyboard = new Scanner(System.in);
        
        Random randomGenerator = new Random();
        
        System.out.println("Player choose your item: ");
        System.out.println("\t 1) Rock");
        System.out.println("\t 2) Paper");
        System.out.println("\t 3) Scissor\n");
        
        playerChoice = keyboard.nextInt();
        
        computerChoice = randomGenerator.nextInt(3) + 1;
        
        switch (playerChoice) {
            case 1:
                result = rockMethod(computerChoice);
                break;
            case 2:
                result = paperMethod(computerChoice);
                break;
            case 3:
                result = scissorMethod(computerChoice);
                break;
            default:
                System.out.println("You suck! You didn't enter the right thing!!");
                break;
        }
        
        System.out.println(result);
    }
    
    public static String rockMethod(int computerChoice){
        String result = "";
        
        switch (computerChoice) {
            case 2:
                result = "You Lose";
                break;
            case 3:
                result = "You Win!!!! Yay!!";
                break;
            default:
                result = "You and the computer tied.";
                break;
        }
        
        return result;            
    }
    
    
    
    
       public static String paperMethod(int computerChoice){
        String result = "";
        
        switch (computerChoice) {
            case 3:
                result = "You Lose";
                break;
            case 1:
                result = "You Win!!!! Yay!!";
                break;
            default:
                result = "You and the computer tied.";
                break;
        }
        
        return result;  
//    next method
    }
    
       
       
       public static String scissorMethod(int computerChoice){
        String result = "";
        
        switch (computerChoice) {
            case 1:
                result = "You Lose";
                break;
            case 2:
                result = "You Win!!!! Yay!!";
                break;
            default:
                result = "You and the computer tied.";
                break;
        }
        
        return result;  
    }
}