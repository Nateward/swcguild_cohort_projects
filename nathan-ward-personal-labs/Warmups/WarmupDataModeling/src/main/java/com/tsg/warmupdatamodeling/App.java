/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.warmupdatamodeling;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {
    
    public static void main(String[] args) {
        String userInput = "";
        int UserInputNumber = 0;
        Scanner keyboard = new Scanner(System.in);
//        
//        System.out.println("Input course name");
//        userInput = keyboard.nextLine();
//        System.out.println("Input course size");
//        UserInputNumber = keyboard.nextInt();

        Course math = new Course("geometry", "math201");
        Course otherCourse = new Course("Taco Dynamics in Suboptimal Enviroments before 1950's ", "ANT639");
        
                
        Student student1 = new Student("Flash", "Flash@web");
        Student student2 = new Student("Batman", "Batman@web");
        Student student3 = new Student("Arrow", "Arrow@web");
        
        math.addStudent(student1);
        math.addStudent(student2);
        math.addStudent(student3);
        
        otherCourse.addStudent(student2);
        otherCourse.addStudent(student3);
        
        math.printCourseReport();
        otherCourse.printCourseReport();
        
    }
}
