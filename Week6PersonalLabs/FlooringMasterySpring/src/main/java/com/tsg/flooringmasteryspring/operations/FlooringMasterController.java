/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.operations;


import com.tsg.flooringmasteryspring.dao.OrdersDAO;
import com.tsg.flooringmasteryspring.dao.ProductDAO;
import com.tsg.flooringmasteryspring.dao.TaxesDAO;
import com.tsg.flooringmasteryspring.dto.Order;
import com.tsg.flooringmasteryspring.dto.Product;
import com.tsg.flooringmasteryspring.ui.ConsoleIO;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.text.NumberFormat;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class FlooringMasterController {

    private String CONFIG_FILE = "config.txt";
    private OrdersDAO orderDAO;
    private ProductDAO productDAO;
    private TaxesDAO taxesDAO;
    private LocalDate date;
    NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
    ConsoleIO con = new ConsoleIO();
    Scanner sc = new Scanner(System.in);
    Integer userChoice = 0;
    private String runMode;

    public FlooringMasterController(OrdersDAO dao1, ProductDAO dao2, TaxesDAO dao3) {
        this.orderDAO = dao1;
        this.productDAO = dao2;
        this.taxesDAO = dao3;
    }
    
    

    public void run() {
        con.print("Welcome to the Flooring Master-POS");
        while (userChoice != 7) {
            printMenu();

            userChoice = con.getInteger("    Please choose menu option: ", 1, 7);

            switch (userChoice) {
                case 1:
                    displayOrders();
                    break;
                case 2:
                    createOrder();
                    break;
                case 3:
                    editOrder();
                    break;
                case 4:
                    removeOrder();
                    break;
                case 5:
                    saveCurrentOrder();
                    break;
                case 6:
                    changDate();
                default:
                    break;
            }

        }

        userChoice = con.getInteger("Do you want to save these changes? \n\t1.) Yes \n\t2.) No ");
        if (userChoice == 1) {
            con.print("Your changes will now be saved");
            saveCurrentOrder();
        }

        con.print("Thank you for choosing Flooring Master!");

    }

    public void printMenu() {
        con.print("******************************************************************************\n");
        con.print("\n*\t\t\t\tFlooring Program");
        con.print("*");
        setDate();
        con.print("\n*\t\t\tWorking Date:" + getDate());
        con.print("*");
        con.print("\n* 1.) Display an Orders");
        con.print("\n* 2.) Create an Order");
        con.print("\n* 3.) Edit an Order");
        con.print("\n* 4.) Remove an Order");
        con.print("\n* 5.) Save Session");
        con.print("\n* 6.) Change Date");
        con.print("\n* 7.) Quit\n");
        con.print("*\n");
        con.print("******************************************************************************\n");

    }

    public void displayOrders() {
        this.displayOrderSummary(this.orderDAO.getOrders());
        Order displayOrder = null;
        Integer orderNumber;
        ArrayList<Order> numberOfOrders = orderDAO.getOrders();
        do {
            orderNumber = con.getInteger("Enter Order Number");
            displayOrder = this.orderDAO.getOrderByOrderNumber(orderNumber);
            if (displayOrder == null) {
                con.print("Wrong number! No orders found");
            }
        } while (numberOfOrders.size() > 0 && displayOrder == null);
        printOrder(displayOrder);
    }

    public void displayOrderSummary(ArrayList<Order> orders) {
        for (Order item : orders) {
            con.print(item.getOrderNumber() + ": " + item.getCustomerName());
            if (item == null) {
                break;
            }
        }
    }

    public void createOrder() {

        Order order = new Order();
        BusinessLogic logic = new BusinessLogic();

        userChoice = 0;
        while (userChoice != 1) {

            DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");

            order.setOrderDate(getDate().format(formatter));

            String newName;
            boolean injection = true;

            while (injection == true) {
                newName = con.getString("Please enter customer name: ");

                if (newName.contains(orderDAO.getORDER_DELIMETER())) {
                    con.print("You can not use " + orderDAO.getORDER_DELIMETER() + " in a persons name.");
                } else {
                    order.setCustomerName(newName);
                    injection = false;
                }
            }

            List<String> taxList = taxesDAO.getStateList();
            int i;
            for (i = 0; i < taxList.size(); i++) {
                String state = taxList.get(i);
                con.print((i + 1) + ".) " + state);
            }
            userChoice = con.getInteger("Please pick a state: ", 1, 4);
            userChoice--;
            order.setState(taxList.get(userChoice));
            order.setTaxRate(taxesDAO.getTaxRate(taxList.get(userChoice)));

            order.setArea(con.getDouble("Please enter the total square footage you wish to floor: ", 0, Double.MAX_VALUE));

            List<String> productList = productDAO.getProductList();

            i = 0;
            for (i = 0; i < productList.size(); i++) {
                String material = productList.get(i);
                con.print((i + 1) + ".) " + material);
            }

            userChoice = con.getInteger("Please choose a material type to be floored: ", 1, 4);
            userChoice--;
            String key = productList.get(userChoice);
            Product product = productDAO.getProduct(key);

            order.setProductType(product.getProductType());
            order.setMaterialCostPerSqFt(product.getCostPerSqFt());
            order.setLaborCostPerSqFt(product.getLaborCostPerSqFt());
            order.setTotalLaborCost(logic.getTotalLaborCost(order.getLaborCostPerSqFt(), order.getArea()));
            order.setTotalMaterialCost(logic.getTotalMaterialCost(order.getMaterialCostPerSqFt(), order.getArea()));
            order.setTotalCostPerSqFt(logic.getTotalCostPerSqFt(order.getMaterialCostPerSqFt(), order.getLaborCostPerSqFt()));
            order.setTax(logic.getTax(order.getTaxRate(), logic.getSubTotal(order.getTotalCostPerSqFt(), order.getArea())));
            order.setTotal(logic.getTotal(logic.getSubTotal(order.getTotalCostPerSqFt(), order.getArea()), order.getTax()));

            printOrder(order);

            userChoice = con.getInteger("Is this order correct? \n\t1.) Yes \n\t2.) No ");
        }

        orderDAO.createOrder(order);
    }

    public void editOrder() {

        BusinessLogic logic = new BusinessLogic();

        Order displayOrder = null;
        Integer orderNumber;
        ArrayList<Order> numberOfOrders = orderDAO.getOrders();
        userChoice = 0;

        do {
            this.displayOrderSummary(this.orderDAO.getOrders());
            orderNumber = con.getInteger("Enter Order Number");
            displayOrder = this.orderDAO.getOrderByOrderNumber(orderNumber);

            if (displayOrder == null) {
                con.print("Wrong number! No orders found");
                this.printOrder(displayOrder);
            } else {
                while (userChoice != 5) {

                    con.print("\n\nWhich fields would you like to edit?\n\n");
                    con.print("\t1.) Name");
                    con.print("\t2.) State");
                    con.print("\t3.) Total Sqaure Footage");
                    con.print("\t4.) Choose New Flooring Type");
                    con.print("\t5.) Return to main menu and save");

                    userChoice = con.getInteger("\nEdit Field: ", 1, 5);
                    switch (userChoice) {
                        case 1:
                            String newName;
                            boolean injection = true;

                            while (injection == true) {
                                newName = con.getString("Please enter customer name: ");

                                if (newName.contains(orderDAO.getORDER_DELIMETER())) {
                                    con.print("You can not use " + orderDAO.getORDER_DELIMETER() + " in a persons name.");
                                } else {
                                    displayOrder.setCustomerName(newName);
                                    injection = false;
                                }
                            }

                            break;
                        case 2:
                            List<String> taxList = taxesDAO.getStateList();
                            int i;
                            for (i = 0; i < taxList.size(); i++) {
                                String state = taxList.get(i);
                                con.print((i + 1) + ".) " + state);
                            }
                            userChoice = con.getInteger("Please pick a state: ", 1, 4);
                            userChoice--;
                            displayOrder.setState(taxList.get(userChoice));
                            displayOrder.setTaxRate(taxesDAO.getTaxRate(taxList.get(userChoice)));
                            displayOrder.setTax(logic.getTax(displayOrder.getTaxRate(), logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea())));
                            displayOrder.setTotal(logic.getTotal(logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea()), displayOrder.getTax()));
                            printOrder(displayOrder);

                            break;
                        case 3:
                            displayOrder.setArea(con.getDouble("Please enter the "
                                    + "total square footage you wish to floor: ", 0, Double.MAX_VALUE));

                            Product productType = this.productDAO.getProduct(displayOrder.getProductType());

                            displayOrder.setMaterialCostPerSqFt(productType.getCostPerSqFt());
                            displayOrder.setLaborCostPerSqFt(productType.getLaborCostPerSqFt());
                            displayOrder.setTotalLaborCost(logic.getTotalLaborCost(displayOrder.getLaborCostPerSqFt(), displayOrder.getArea()));
                            displayOrder.setTotalMaterialCost(logic.getTotalMaterialCost(displayOrder.getMaterialCostPerSqFt(), displayOrder.getArea()));
                            displayOrder.setTotalCostPerSqFt(logic.getTotalCostPerSqFt(displayOrder.getMaterialCostPerSqFt(), displayOrder.getLaborCostPerSqFt()));
                            displayOrder.setTax(logic.getTax(displayOrder.getTaxRate(), logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea())));
                            displayOrder.setTotal(logic.getTotal(logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea()), displayOrder.getTax()));

                            printOrder(displayOrder);

                            break;
                        case 4:
                            List<String> productList = productDAO.getProductList();

                            i = 0;
                            for (i = 0; i < productList.size(); i++) {
                                String material = productList.get(i);
                                con.print((i + 1) + ".) " + material);
                            }

                            userChoice = con.getInteger("Please choose a material type to be floored: ", 1, 4);
                            userChoice--;
                            String key = productList.get(userChoice);
                            Product product = productDAO.getProduct(key);
                            displayOrder.setProductType(product.getProductType());

                            displayOrder.setMaterialCostPerSqFt(product.getCostPerSqFt());
                            displayOrder.setLaborCostPerSqFt(product.getLaborCostPerSqFt());
                            displayOrder.setTotalLaborCost(logic.getTotalLaborCost(displayOrder.getLaborCostPerSqFt(), displayOrder.getArea()));
                            displayOrder.setTotalMaterialCost(logic.getTotalMaterialCost(displayOrder.getMaterialCostPerSqFt(), displayOrder.getArea()));
                            displayOrder.setTotalCostPerSqFt(logic.getTotalCostPerSqFt(displayOrder.getMaterialCostPerSqFt(), displayOrder.getLaborCostPerSqFt()));
                            displayOrder.setTax(logic.getTax(displayOrder.getTaxRate(), logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea())));
                            displayOrder.setTotal(logic.getTotal(logic.getSubTotal(displayOrder.getTotalCostPerSqFt(), displayOrder.getArea()), displayOrder.getTax()));

                            printOrder(displayOrder);
                            break;
                    }

                    if (userChoice != 5) {

                        userChoice = con.getInteger("Are you done editing this order? \n\t1.) Yes \n\t2.) No ");
                        if (userChoice == 1) {
                            orderDAO.editOrder(displayOrder.getOrderNumber(), displayOrder);
                            break;
                        } else {

                        }
                    }
                    orderDAO.editOrder(displayOrder.getOrderNumber(), displayOrder);

                }
            }
        } while (numberOfOrders.size() > 0 && displayOrder == null);
        if (displayOrder != null) {

        }

    }

    public void removeOrder() {
        this.displayOrderSummary(this.orderDAO.getOrders());
        Order displayOrder = null;
        Integer orderNumber;
        ArrayList<Order> numberOfOrders = orderDAO.getOrders();

        do {
            orderNumber = con.getInteger("Enter Order Number");
            displayOrder = this.orderDAO.getOrderByOrderNumber(orderNumber);
            if (displayOrder == null) {
                con.print("Wrong number! No orders found");
            }
        } while (numberOfOrders.size() > 0 && displayOrder == null);
        if (displayOrder != null) {
            printOrder(displayOrder);
            con.print("Do you want to delete this order?");
            Integer wantToRemove = con.getInteger("\n\t1.) Yes \n\t2.) No", 1, 2);
            if (wantToRemove == 1) {
                con.print("\n\nDeleting...");
                orderDAO.deleteOrder(orderNumber);
            } else {
                con.print("Your order is still in the system.");
            }
        }

    }

    public void saveCurrentOrder() {

        try {
            runMode = this.loadConfig();
        } catch (FileNotFoundException ex) {
            con.print("Could not load config file.");
        }
        if (runMode.equalsIgnoreCase("Production")) {
            orderDAO.save(orderDAO.getORDER_FILE());
        }
    }

    //Should create a new file.
    private void changDate() {
        //Prompt date
        saveCurrentOrder();
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("MMddyyyy");
        date = null;
        do {
            String dateString = con.getString("Enter date(MMddyyyy)");
            try {
                date = LocalDate.parse(dateString, formatter);
            } catch (Exception e) {
                con.print("Invalid format");
            }

        } while (getDate() == null);
        orderDAO.setWhatDay(date);
    }

    private void printOrder(Order order) {

        String printThis;
        if (order != null) {

            con.print("\n\n");
            con.printf("Date Created:\t\t\t %s\n\n", order.getOrderDate());
            if (order.getOrderNumber() == null) {
                String quote = "Quote";
                con.printf("Order Number:\t\t\t %s\n\n", quote);
            } else {
                con.printf("Order Number:\t\t\t %d\n\n", order.getOrderNumber());
            }
            con.printf("Order Name:\t\t\t %s\n\n", order.getCustomerName());
            con.printf("State:\t\t\t\t %s\n\n", order.getState());
            con.printf("Tax Rate:\t\t\t %.2f\n\n", order.getTaxRate());
            con.printf("Product Type:\t\t\t %s\n\n", order.getProductType());
            con.printf("Total Square Feet:\t\t %s\n\n", defaultFormat.format(order.getArea()));
            con.printf("Labor Cost:\t\t\t %s\n\n", defaultFormat.format(order.getLaborCostPerSqFt()));
            con.printf("Material Cost:\t\t\t %s\n\n", defaultFormat.format(order.getMaterialCostPerSqFt()));
            con.printf("Cost Per Square Foot:\t\t %s\n\n", defaultFormat.format(order.getTotalCostPerSqFt()));
            con.printf("Total Labor Cost:\t\t %s\n\n", defaultFormat.format(order.getTotalLaborCost()));
            con.printf("Total Material Cost:\t\t %s\n\n", defaultFormat.format(order.getTotalMaterialCost()));
            con.printf("Tax:\t\t\t\t %s\n\n", defaultFormat.format(order.getTax()));
            con.printf("Total:\t\t\t\t %s\n\n", defaultFormat.format(order.getTotal()));

        }
    }

    public String loadConfig() throws FileNotFoundException {

        sc = new Scanner(new BufferedReader(new FileReader(CONFIG_FILE)));
        while (sc.hasNextLine()) {
            return sc.nextLine();
        }
        return "";
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return date;
    }

    private void setDate() {
        LocalDate whatDay = orderDAO.getWhatDay();
        this.date = whatDay;
    }

}
