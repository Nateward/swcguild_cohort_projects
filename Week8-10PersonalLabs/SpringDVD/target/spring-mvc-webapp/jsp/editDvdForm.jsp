<%-- 
    Document   : NewContactFormNoAjax
    Created on : 29-Mar-2016, 10:24:07 AM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Dvd Library - Edit Dvd</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/dvdvideo.jpg" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
        </div>
        <div class="container">
            <h1>Edit Dvd Form</h1>
            <a href="displayDvdsList">View Dvd Library</a>
            <hr/>
            <sf:form modelAttribute="dvd" class="form-horizontal" id="usrform" role="form" action="editDvd" method="POST">
                <div class="form-group">
                    <label for="edit-title" class="col-md-4 control-label">Title:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-title" path="title" placeholder="Title"/>
                        <sf:errors path="title" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-release-date" class="col-md-4 control-label">Release Date:</label>
                        <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-release-date" path="releaseDate" placeholder="Release Date:"/>
                        <sf:errors path="releaseDate" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-mpaaRating" class="col-md-4 control-label">MPAA Rating:</label>
                        <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-mpaaRating" path="mpaaRating" placeholder="MPAA Rating"/>
                        <sf:errors path="mpaaRating" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-director" path="directorsName" placeholder="Director"/>
                        <sf:errors path="directorsName" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-studio" path="studio" placeholder="Studio"/>
                        <sf:errors path="studio" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-user-notes" class="col-md-4 control-label">Notes:</label>
                        <div class="col-md-8">
                        <sf:textarea class="form-control" id="edit-user-notes" form="usrform" path="userNote" placeholder="User Notes"/>
                        <sf:hidden path="id"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="edit-button" class="btn btn-info">Edit Dvd</button>
                    </div>
                </div>
            </sf:form>
        </div>
    </body>
</html>
