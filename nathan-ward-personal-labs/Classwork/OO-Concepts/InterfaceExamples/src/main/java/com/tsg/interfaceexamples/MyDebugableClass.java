/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.interfaceexamples;

/**
 *
 * @author apprentice
 */
public class MyDebugableClass implements Debuggable{

    @Override
    public void displayStatus(String id) {
        System.out.println("Status ID is: " + id);
    }

    @Override
    public void displayError(String error) {
        System.out.println("ERROR!! Things broe like so: " + error);
    }
    
}
