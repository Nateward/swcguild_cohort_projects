/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.contactlistmvc.validation;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class ValidationErrorContainer {
    
    private List<ValidationError> validationErrors = new ArrayList<>();
    
    public void addvalidationError(String field, String message){
        ValidationError error = new ValidationError(field, message);
        validationErrors.add(error);
    }
    
    public List<ValidationError> getfieldErrors(){
        return validationErrors;
    }
}
