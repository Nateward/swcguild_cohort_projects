/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistqueue;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class LinkedListQueueDriverApp {
    public static void main(String[] args) {
        LinkedListQueueImpl ll = new LinkedListQueueImpl();
        
        ll.enqueue("a");
        ll.enqueue("b");
        ll.enqueue("c");
        ll.enqueue("d");
        ll.enqueue("e");
        ll.enqueue("f");
        ll.dequeue();
        
       Iterator i= ll.iterator();
       
        for (Object object : ll) {
            System.out.println(object);
        }
        
    }
}
