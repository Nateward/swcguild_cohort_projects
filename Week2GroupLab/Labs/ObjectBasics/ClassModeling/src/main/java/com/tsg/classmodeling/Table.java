/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Table {
    private int height;
    private int width;
    private float price;
    private int numDrawers;
    private String tableType;

    public Table(int height, int width, float price, int numDrawers, String tableType) {
        this.height = height;
        this.width = width;
        this.price = price;
        this.numDrawers = numDrawers;
        this.tableType = tableType;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public float getPrice() {
        return price;
    }

    public void setPrice(float price) {
        this.price = price;
    }

    public int getNumDrawers() {
        return numDrawers;
    }

    public void setNumDrawers(int numDrawers) {
        this.numDrawers = numDrawers;
    }

    public String getTableType() {
        return tableType;
    }

    public void setTableType(String tableType) {
        this.tableType = tableType;
    }
    
    //Behaviors
    
    
    /**
     * these Behaviors lets you know how the table drawers does.
     */
    public void drawers()
    {
        System.out.println("The drawers opens and closes");
    }
    
}
