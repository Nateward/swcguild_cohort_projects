/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springvendingmachinemvc.dao;

import com.tsg.springvendingmachinemvc.dto.Beer;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nate Ward <nateward.nw@gmail.com>
 */
public class VendingMachineDAO {

    private Map<String, Beer> vendingMachine = new TreeMap<>();
    private static final String BEER_FILE = "/thefridge.txt";
    private static final String DELIMITER = "::";

    public VendingMachineDAO() {
        try {
            loadBeerList();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(VendingMachineDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Reduces quantity of beer by one.
     *
     * @param itemPosition String
     */
    public void sellBeer(String itemPosition) {
        Beer beer = vendingMachine.get(itemPosition);
        int currentQuantity = beer.getQuantity();
        beer.setQuantity(currentQuantity - 1);
    }

    /**
     *
     * @param itemPosition String
     * @return Beer object at itemPosition
     */
    public Beer getBeerByItemPosition(String itemPosition) {
        return vendingMachine.get(itemPosition);
    }

    /**
     *
     * @return list of all Beers in vending machine
     */
    public ArrayList<Beer> getAllBeers() {
        Collection<Beer> beers = vendingMachine.values();
        ArrayList<Beer> beerArrList = new ArrayList<>();
        for (Beer beer : beers) {
            beerArrList.add(beer);
        }

        return beerArrList;
    }

    /**
     * Adds addQty to beer quantity
     *
     * @param beer Beer
     * @param addQty int
     */
    public void addBeerQty(Beer beer, int addQty) {
        int currentQuantity = beer.getQuantity();
        beer.setQuantity(currentQuantity + addQty);
    }

    /**
     * Writes vending machine file to BEER_FILE String in following order.
     * itemPosition :: name :: cost :: quantity
     *
     * @throws IOException
     */
    public void writeBeerList() throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(BEER_FILE));
        Collection<Beer> beers = vendingMachine.values();

        for (Beer beer : beers) {
            out.println(beer.getItemPosition() + DELIMITER + beer.getName() + DELIMITER + beer.getCost() + DELIMITER + beer.getQuantity());
        }

        out.flush();
        out.close();

    }

    /**
     * Reads vending machine file from BEER_FILE String in following order.
     * itemPosition :: name :: cost :: quantity
     *
     * @throws FileNotFoundException
     */
    public void loadBeerList() throws FileNotFoundException {
//        Scanner sc = new Scanner(new BufferedReader(new FileReader(BEER_FILE)));
//
//        while (sc.hasNextLine()) {
//            String currentLine = sc.nextLine();
//            String[] tokens = currentLine.split(DELIMITER);
//
//            Beer beer = new Beer();
//
//            beer.setItemPosition(tokens[0]);
//            beer.setName(tokens[1]);
//            beer.setCost(Double.parseDouble(tokens[2]));
//            beer.setQuantity(Integer.parseInt(tokens[3]));
//
//            this.vendingMachine.put(tokens[0], beer);

//A2::Stone Go-To::6.5::8
//A3::Grt Lks Chillwave::6.06::5
//B1::Dragon's Milk::13.48::13
//B2::Sthrn Tier Live::4.5::20
//B3::Fat Tire::4.01::15
//C1::Anti-Hero::6.99::7
//C2::The Kind IPA::4.79::8
//C3::Nitro Milk Stout::4.81::9
        Beer beer = new Beer();
        beer.setItemPosition("A1");
        beer.setName("Bell's Hopslam");
        beer.setCost(5.0);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("A2");
        beer.setName("Stone Go-To");
        beer.setCost(6.5);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("A3");
        beer.setName("Dragon's Milk");
        beer.setCost(13.48);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("B1");
        beer.setName("Southern Teir Live");
        beer.setCost(4.5);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("B2");
        beer.setName("Fat Tire");
        beer.setCost(4.01);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("B3");
        beer.setName("Revolution Anti-Hero");
        beer.setCost(6.99);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("C1");
        beer.setName("Great Lakes Chill Wave");
        beer.setCost(4.79);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("C2");
        beer.setName("The Kind IPA");
        beer.setCost(4.81);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

        beer = new Beer();
        beer.setItemPosition("C3");
        beer.setName("Nitro Milk Stout");
        beer.setCost(4.81);
        beer.setQuantity(5);

        this.vendingMachine.put(beer.getItemPosition(), beer);

    }

}
