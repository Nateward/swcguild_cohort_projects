/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class School {
    private String nameOfSchool;
    private int numOfStudents;
    private String address;
    private boolean summerBreak;

    public School(String nameOfSchool, int numOfStudents, String address, boolean summerBreak) {
        this.nameOfSchool = nameOfSchool;
        this.numOfStudents = numOfStudents;
        this.address = address;
        this.summerBreak = summerBreak;
    }

    /**
     * @return the nameOfSchool
     */
    public String getNameOfSchool() {
        return nameOfSchool;
    }

    /**
     * @param nameOfSchool the nameOfSchool to set
     */
    public void setNameOfSchool(String nameOfSchool) {
        this.nameOfSchool = nameOfSchool;
    }

    /**
     * @return the numOfStudents
     */
    public int getNumOfStudents() {
        return numOfStudents;
    }

    /**
     * @param numOfStudents the numOfStudents to set
     */
    public void setNumOfStudents(int numOfStudents) {
        this.numOfStudents = numOfStudents;
    }

    /**
     * @return the address
     */
    public String getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
     * @return the summerBreak
     */
    public boolean isSummerBreak() {
        return summerBreak;
    }

    /**
     * @param summerBreak the summerBreak to set
     */
    public void setSummerBreak(boolean summerBreak) {
        this.summerBreak = summerBreak;
    }
    
    
   public void clean(){
       
   } 
    
}
