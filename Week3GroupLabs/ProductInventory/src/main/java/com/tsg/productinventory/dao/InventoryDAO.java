/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.productinventory.dao;

import com.tsg.productinventory.dto.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class InventoryDAO {

    HashMap<Integer, Product> inventory = new HashMap<>();
    private final String DELIMITER = "::";
    private final String PRODUCT_FILE = "InventoryList.txt";

    public void loadInventoryFile() throws FileNotFoundException {
        Scanner sc = new Scanner(new BufferedReader(new FileReader(PRODUCT_FILE)));

        while (sc.hasNext()) {
            String currentLine = sc.nextLine();
            String[] tokens = currentLine.split(DELIMITER);

            Product product;

            if (tokens[0].equals("Beer")) {
                product = new Beer();

                product.setProductType(tokens[0]);
                product.setSku(Integer.parseInt(tokens[1]));
                product.setName(tokens[2]);
                product.setCost(Double.parseDouble(tokens[3]));
                product.setPrice(Double.parseDouble(tokens[4]));
                product.setQuantityOnHand(Integer.parseInt(tokens[5]));
                product.setSafetyStock(Integer.parseInt(tokens[6]));
                ((Beer) product).setAbv(Double.parseDouble(tokens[7]));
            } else if (tokens[0].equals("Cigar")) {
                product = new Cigar();

                product.setProductType(tokens[0]);
                product.setSku(Integer.parseInt(tokens[1]));
                product.setName(tokens[2]);
                product.setCost(Double.parseDouble(tokens[3]));
                product.setPrice(Double.parseDouble(tokens[4]));
                product.setQuantityOnHand(Integer.parseInt(tokens[5]));
                product.setSafetyStock(Integer.parseInt(tokens[6]));
                ((Cigar) product).setCountryOfOrigin(tokens[7]);
            } else if (tokens[0].equals("Glassware")) {
                product = new Glassware();

                product.setProductType(tokens[0]);
                product.setSku(Integer.parseInt(tokens[1]));
                product.setName(tokens[2]);
                product.setCost(Double.parseDouble(tokens[3]));
                product.setPrice(Double.parseDouble(tokens[4]));
                product.setQuantityOnHand(Integer.parseInt(tokens[5]));
                product.setSafetyStock(Integer.parseInt(tokens[6]));
                ((Glassware) product).setType(tokens[7]);
            } else {
                product = new Product();

                product.setProductType(tokens[0]);
                product.setSku(Integer.parseInt(tokens[1]));
                product.setName(tokens[2]);
                product.setCost(Double.parseDouble(tokens[3]));
                product.setPrice(Double.parseDouble(tokens[4]));
                product.setQuantityOnHand(Integer.parseInt(tokens[5]));
                product.setSafetyStock(Integer.parseInt(tokens[6]));
            }

            inventory.put(product.getSku(), product);

        }
    }

    public void writeToInventoryFile() throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(PRODUCT_FILE));

        Collection<Product> productValues = inventory.values();

        for (Product product : productValues) {
            if (product instanceof Beer) {
                out.println(product.getProductType() + DELIMITER + product.getSku() + DELIMITER
                        + product.getName() + DELIMITER + product.getCost()
                        + DELIMITER + product.getPrice() + DELIMITER
                        + product.getQuantityOnHand() + DELIMITER + product.getSafetyStock()
                        + DELIMITER + ((Beer) product).getAbv());
            } else if (product instanceof Cigar) {
                out.println(product.getProductType() + DELIMITER + product.getSku() + DELIMITER
                        + product.getName() + DELIMITER + product.getCost()
                        + DELIMITER + product.getPrice() + DELIMITER
                        + product.getQuantityOnHand() + DELIMITER + product.getSafetyStock()
                        + DELIMITER + ((Cigar) product).getCountryOfOrigin());
            } else if (product instanceof Glassware) {
                out.println(product.getProductType() + DELIMITER + product.getSku() + DELIMITER
                        + product.getName() + DELIMITER + product.getCost()
                        + DELIMITER + product.getPrice() + DELIMITER
                        + product.getQuantityOnHand() + DELIMITER + product.getSafetyStock()
                        + DELIMITER + ((Glassware) product).getType());
            } else {
                out.println(product.getProductType() + DELIMITER + product.getSku() + DELIMITER
                        + product.getName() + DELIMITER + product.getCost()
                        + DELIMITER + product.getPrice() + DELIMITER
                        + product.getQuantityOnHand() + DELIMITER + product.getSafetyStock());
            }
        }

        out.flush();
        out.close();
    }

    public void addProduct(Product product) {
        inventory.put(product.getSku(), product);
    }

    public double getTotalValueOfInventory() {
        Set<Integer> keys = inventory.keySet();
        double total = 0;
        for (Integer key : keys) {
            total += (inventory.get(key).getQuantityOnHand() * inventory.get(key).getCost());
        }

        return total;
    }
    
    public Collection<Product> getAllProducts () {
        return inventory.values();
    }
    public Product getProductBySku(Integer sku) {
        return inventory.get(sku);
    }

    public void addQtyToProduct(Product product, Integer qtyToAdd) {
        product.setQuantityOnHand(product.getQuantityOnHand() + qtyToAdd);
    }

    public void remQtyFromProduct(Product product, Integer qtyToRem) {
        product.setQuantityOnHand(product.getQuantityOnHand() - qtyToRem);
    }

    public double getTotalValueOfProduct(Integer sku) {
        Product product = inventory.get(sku);
        return product.getCost() * product.getQuantityOnHand();
    }

}
