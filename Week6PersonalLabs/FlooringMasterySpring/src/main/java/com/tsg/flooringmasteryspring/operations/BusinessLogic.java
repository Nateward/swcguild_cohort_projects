/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.operations;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class BusinessLogic {

    public Double getTotalLaborCost(Double labor, Double area) {
        return labor * area;
    }

    public Double getTotalMaterialCost(Double area, Double materialsCostPerSqFt) {
        return area * materialsCostPerSqFt;
    }
    
    public Double getTotalCostPerSqFt(Double laborCostPerSqFt, Double materialCostPerSqFt) {
        return laborCostPerSqFt + materialCostPerSqFt;
    }
    
    public Double getSubTotal(Double costPerSqFt, Double area) {
        return costPerSqFt * area;
    }
            
    public Double getTax(Double subTotal,Double taxRate) {
        return subTotal * (taxRate / 100);
    }
    
    public Double getTotal(Double subTotal, Double tax) {
        return subTotal + tax;
    }
    
   
}
