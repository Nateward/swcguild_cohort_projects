/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.productinventory;

import com.tsg.productinventory.dao.InventoryDAO;
import com.tsg.productinventory.dto.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class JUnitTesting {

    public JUnitTesting() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    public void SetupFakeDB(InventoryDAO inventory) {
        Product beer = new Beer();
        beer.setProductType("Beer");
        beer.setSku(123);
        beer.setName("Stone");
        beer.setCost(5.0);
        beer.setPrice(10.0);
        beer.setSafetyStock(5);
        beer.setQuantityOnHand(100);
        inventory.addProduct(beer);

        Product cigar = new Cigar();
        cigar.setProductType("Cigar");
        cigar.setSku(456);
        cigar.setName("Cuban");
        ((Cigar) cigar).setCountryOfOrigin("Cuba");
        cigar.setCost(2.5);
        cigar.setPrice(5.0);
        cigar.setSafetyStock(10);
        cigar.setQuantityOnHand(200);
        inventory.addProduct(cigar);

        Product glassware = new Glassware();
        glassware.setProductType("Glassware");
        glassware.setSku(789);
        glassware.setName("IPA glass");
        ((Glassware) glassware).setType("Tulip");
        glassware.setCost(1.50);
        glassware.setPrice(3.50);
        glassware.setSafetyStock(20);
        glassware.setQuantityOnHand(50);
        inventory.addProduct(glassware);

    }

    @Test // 500 + 500 + 75 = 1075
    public void GetTotalValueInventoryTest1() {
        InventoryDAO inventory = new InventoryDAO();
        SetupFakeDB(inventory);

        double result = inventory.getTotalValueOfInventory();

        assertEquals(1075, result, 0);

    }

    @Test // using SKU 456 from fakeDB to see return product.getProductType == true.
    public void getProductTypeTest1() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product product = inventory.getProductBySku(456);

        assertEquals("Cigar", product.getProductType());
    }

    @Test // using SKU 123 from fakeDB to see if return product is-a cigar.
    public void getProductTypeTest2() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product product = inventory.getProductBySku(123);
        assertFalse(product instanceof Cigar);
    }

    @Test // using SKU 456 from fakeDB to see return product.getCountryOfOrigin == true.
    public void getProductCountryOriginTest() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product product = inventory.getProductBySku(456);

        assertEquals("Cuba", ((Cigar) product).getCountryOfOrigin());
    }

    @Test // add product, verify it comes back with set properties
    public void AddProductTest1() {
        InventoryDAO inventory = new InventoryDAO();

        Product cigar = new Cigar();
        cigar.setProductType("Cigar");
        cigar.setSku(456);
        cigar.setName("Cuban");
        ((Cigar) cigar).setCountryOfOrigin("Cuba");
        cigar.setCost(2.5);
        cigar.setPrice(5.0);
        cigar.setSafetyStock(10);
        cigar.setQuantityOnHand(200);
        inventory.addProduct(cigar);

        Product product = inventory.getProductBySku(456);

        assertEquals("Cigar", product.getProductType());
    }

    @Test // add product, verify it comes back with set properties
    public void AddProductTest2() {
        InventoryDAO inventory = new InventoryDAO();

        Product cigar = new Cigar();
        cigar.setProductType("Cigar");
        cigar.setSku(456);
        cigar.setName("Cuban");
        ((Cigar) cigar).setCountryOfOrigin("Cuba");
        cigar.setCost(2.5);
        cigar.setPrice(5.0);
        cigar.setSafetyStock(10);
        cigar.setQuantityOnHand(200);
        inventory.addProduct(cigar);

        Product product = inventory.getProductBySku(456);

        assertEquals("Cuba", ((Cigar) product).getCountryOfOrigin());
    }

    @Test // add product, verify it comes back as instanceof Cigar
    public void AddProductTest3() {
        InventoryDAO inventory = new InventoryDAO();

        Product cigar = new Cigar();
        cigar.setProductType("Cigar");
        cigar.setSku(456);
        cigar.setName("Cuban");
        ((Cigar) cigar).setCountryOfOrigin("Cuba");
        cigar.setCost(2.5);
        cigar.setPrice(5.0);
        cigar.setSafetyStock(10);
        cigar.setQuantityOnHand(200);
        inventory.addProduct(cigar);

        Product product = inventory.getProductBySku(456);

        assertTrue(product instanceof Cigar);
    }

    @Test // should go from 200 + 100 => 300
    public void AddQtyToProductTest1() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product productTest = inventory.getProductBySku(456);
        inventory.addQtyToProduct(productTest, 100);

        assertEquals(300, productTest.getQuantityOnHand());

    }

    @Test // should go from 200 - 100 => 100
    public void RemQtyFromProductTest1() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product productTest = inventory.getProductBySku(456);
        inventory.remQtyFromProduct(productTest, 100);

        assertEquals(100, productTest.getQuantityOnHand());

    }

    @Test // should go from 200 - 300 => -100
    public void RemQtyFromProductTest2() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        Product productTest = inventory.getProductBySku(456);
        inventory.remQtyFromProduct(productTest, 300);

        assertEquals(-100, productTest.getQuantityOnHand());

    }

    @Test // value should be 200 * 2.5 = 500
    public void GetTotalValueOfProductTest1() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        assertEquals(500, inventory.getTotalValueOfProduct(456), 0);

    }

    @Test // value should be 100 * 5.0 = 500
    public void GetTotalValueOfProductTest2() {
        InventoryDAO inventory = new InventoryDAO();

        SetupFakeDB(inventory);

        assertEquals(500, inventory.getTotalValueOfProduct(123), 0);

    }

}
