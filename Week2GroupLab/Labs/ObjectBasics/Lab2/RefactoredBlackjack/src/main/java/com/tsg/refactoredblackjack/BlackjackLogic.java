/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.refactoredblackjack;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class BlackjackLogic {
    public static void gameLogic(){
    
     int playerCard1 = 0;
        int playerCard2 = 0;
        int playerTotal = 0;
        Scanner keyboard = new Scanner(System.in);
        String userInput = "";

        String whoWins = "";

        int dealerCard1 = 0;
        int dealerCard2 = 0;
        int dealerTotal = 0;

        playerCard1 = getRandom();
        playerCard2 = getRandom();

        dealerCard1 = getRandom();

        playerTotal = getTotal(playerCard1, playerCard2);
        System.out.println("Welcome to Blackjack!\n\n");

        System.out.println("Dealer holds:");
        System.out.println(dealerCard1);

        System.out.println("Players cards:");
        System.out.println(playerCard1 + " & " + playerCard2);

        do {
            System.out.println("Do you want to HIT or STAY?: ");

            userInput = keyboard.next();
            if (userInput.equalsIgnoreCase("hit")) {
                playerCard1 = getRandom();

                System.out.println("Previous hands total:");
                System.out.println(playerTotal);

                playerTotal = getTotal(playerTotal, playerCard1);

                System.out.println("You pulled a: ");
                System.out.println(playerCard1 + "\n");
                if (playerTotal > 21) {
                    System.out.println("BUST!! ");
                    break;
                }
            } else {
                break;
            }
        } while (playerTotal <= 21);

        System.out.println("Player holds a total of: " + playerTotal);
        if (playerTotal < 21) {
            do {
                dealerCard2 = getRandom();
                dealerTotal = getTotal(dealerTotal, dealerCard2);

            } while (dealerTotal <= 16);

            System.out.println("Dealer got:\n" + dealerTotal);
            whoWins = setWhoWins(playerTotal, dealerTotal);
            System.out.println(whoWins);

        }
    }
        
    public static int getTotal(int card1, int card2) {
        int total = card1 + card2;

        return total;
    }
        
         public static int getRandom() {
        Random draw = new Random();
        int card = 0;

        card = draw.nextInt(10) + 2;
        return card;
    }

    public static String setWhoWins(int player, int dealer) {
        if ((player > dealer || player < 21) && (dealer < player || dealer > 21)) {
            return "Player wins!!!!";
        } else if (dealer == player) {
            return "Dealer wins.";
        } else {
            return "Dealer wins.";
        }

    }

    
}

