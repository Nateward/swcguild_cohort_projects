/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function (event) {
    $('#zip-button').click(function (event) {
        event.preventDefault();
        var zipCode = $('#add-zip').val();
        var openWeather = "http:/" + "/api.openweathermap.org/data/2.5/weather?zip=" + zipCode + ",us&units=imperial&appid=110b4054c57d0a3858e8eb8104e95799";
        $.ajax({
            type: 'GET',
            url: '' + openWeather
        }).success(function (data, status) {
            fillCurrentWeatherContent(data, status);
        });
    });

    $('#city-button').click(function (event) {
        event.preventDefault();
        var cityName = $('#add-city').val();
        var openWeather = "http:/" + "/api.openweathermap.org/data/2.5/weather?q=" + cityName + ",us&units=imperial&appid=110b4054c57d0a3858e8eb8104e95799";
        $.ajax({
            type: 'GET',
            url: '' + openWeather
        }).success(function (data, status) {
            $('#city-name-status').text('');
            $('#weather-spacing').hide();
            $('#weather-content').show();

            fillCurrentWeatherContent(data, status);
        });
        var openWeather = "http:/" + "/api.openweathermap.org/data/2.5/forecast/daily?q=" + cityName + ",us&units=imperial&appid=110b4054c57d0a3858e8eb8104e95799";
        $.ajax({
            type: 'GET',
            url: '' + openWeather
        }).success(function (data, status) {
            $('#city-name-status').text('');
            fillForecastDaysContent(data, status);
            $('#5-Day-Forecast').show();
        });
    });
});

function fillCurrentWeatherContent(openWeather, status) {


    $('#weather-description').text('');
    $('#weather-temp').text('');

    $('#city-name-status').text(openWeather.name);
    $('#weather-temp').text(openWeather.main.temp + '°F');

    var weatherStatus = $(openWeather.weather);
    var numOfConditions = weatherStatus.length - 1;
    var weatherIcon = $(openWeather.weather[0].icon);

    for (var i = 0; i < numOfConditions; i++) {
        $('#weather-description').append(openWeather.weather[i].description + '/');
    }
    $('#weather-description').append(openWeather.weather[numOfConditions].description);
    $('#weather-status-img').append().attr({
        'src': 'OpenWeatherMap/../img/openWeatherIcons/' + weatherIcon.selector + '.png'
    });

}

function fillForecastDaysContent(openWeather, status) {
    $('#forecast-Days-Content').empty();

    var listOfConditions = $(openWeather.list);
    var numOfConditions = $(listOfConditions.length - 1);

    for (var i = 0; i < numOfConditions; i++) {


        var weatherCondition = $(listOfConditions[i].weather[0].description);
        var weatherTemps = listOfConditions[i].temp;
        var weatherIcon = $(listOfConditions[i].weather[0].icon.selector);

        var td = '<td>';

        td += '<img src: "OpenWeatherMap/../img/openWeatherIcons/' + weatherIcon + '.png"><br/> Low/High: ' + weatherTemps.min + '°F - ' + weatherTemps.max + '°F <br/>' + weatherCondition + '</td>';
               
    }

}
;