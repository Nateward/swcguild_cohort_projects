/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd;

import com.tsg.springdvd.dao.DvdDAO;
import com.tsg.springdvd.dto.Dvd;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@Controller
public class HomeDvdController {

    private final DvdDAO dao;

    @Inject
    public HomeDvdController(DvdDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/displayDvdsList", method = RequestMethod.GET)
    public String displayDvdList(Model model) {

        List<Dvd> dList = dao.GetAll();
        model.addAttribute("dvdList", dList);
        return "displayDvds";
    }

    @RequestMapping(value = "displayNewDvdForm", method = RequestMethod.GET)
    public String displayNewDvdForm(Model model) {
        
        Dvd dvd = new Dvd();
        model.addAttribute("dvd", dvd);
        
        return "newDvdForm";
    }

    @RequestMapping(value = "/addNewDvd", method = RequestMethod.POST)
    public String addNewDvd(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result) {
        
//        String title = req.getParameter("title");
//        String releaseDate = req.getParameter("releaseDate");
//        String mpaaRating = req.getParameter("rating");
//        String director = req.getParameter("director");
//        String studio = req.getParameter("studio");
//        String userNotes = req.getParameter("notes");
//
//        Dvd dvd = new Dvd();
//        dvd.setTitle(title);
//        dvd.setReleaseDate(releaseDate);
//        dvd.setMpaaRating(mpaaRating);
//        dvd.setDirectorsName(director);
//        dvd.setStudio(studio);
//        dvd.setUserNote(userNotes);
        
        if (result.hasErrors()) {
            return "newDvdForm";
        }
        dao.Create(dvd);

        return "redirect:displayDvdsList";

    }

    @RequestMapping(value = "/deleteDvd", method = RequestMethod.GET)
    public String deleteDvd(HttpServletRequest req) {
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));

        dao.Delete(dvdId);

        return "redirect:displayDvdsList";
    }
    
    @RequestMapping(value = "/diplayEditDvdFrom", method = RequestMethod.GET)
    public String displayEditDvdForm(HttpServletRequest req, Model model){
        
        int dvdId = Integer.parseInt(req.getParameter("dvdId"));
        
        Dvd dvd = dao.GetById(dvdId);
        
        model.addAttribute("dvd", dvd);
        
        return "editDvdForm";
    }
    
    @RequestMapping(value = "/editDvd", method = RequestMethod.POST)
    public String editDvd(@Valid @ModelAttribute("dvd") Dvd dvd, BindingResult result){
        
        if (result.hasErrors()) {
            return "editDvdForm";
        }
        
        dao.Update(dvd.getId(), dvd);
        
        return "redirect:displayDvdsList";
    }
}
