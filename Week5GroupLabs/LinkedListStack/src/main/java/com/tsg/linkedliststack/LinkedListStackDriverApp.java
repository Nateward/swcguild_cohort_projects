/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedliststack;

/**
 *
 * @author apprentice
 */
public class LinkedListStackDriverApp {
    public static void main(String[] args) {
        LinkedListStackImpl impl = new LinkedListStackImpl();
        impl.push("Sample One");
        impl.push("Sample two");
        impl.push("Sample three");
        impl.push("Sample four");
        impl.push("Sample five");
        System.out.println(impl.pop());
        System.out.println(impl.pop());
    }
}
