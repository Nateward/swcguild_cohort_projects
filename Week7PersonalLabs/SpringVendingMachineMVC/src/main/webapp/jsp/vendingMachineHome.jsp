<%-- 
    Document   : vendingMachineHome
    Created on : 30-Mar-2016, 5:18:12 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Beer Vending Machine</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/css/myStyles.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/fonts/digital7.regular.ttf" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/dvdvideo.jpg" rel="Shortcut icon">

    </head>
    <body>
        <div id="page_backing" class="container-fluid well">
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="text-center">Welcome To The Beer Vending Machine</h1>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="container">
                        <c:forEach var ="beer" items="${beerList}" varStatus="stepper" step="1" begin="0">
                            <%--<c:forEach var ="beer" items="${beerList}">--%>
                            <form modelAttribute="beer" name="position" id="usrform" role="form" action="getBeerToVend" method="POST">
                                <span class="col-lg-4" id="${beer.itemPosition}">
                                    <img/>
                                    <h4>${beer.name} || $${beer.cost} [qty - ${beer.quantity} ]</h4>
                                    <!--<div form="usrform" value="${beer.itemPosition}" class="col-lg-12 btn btn-lg btn-success">${beer.itemPosition}</div>-->
                                    <input form="usrform" value="${beer.itemPosition}" type="submit" name="position" class="col-lg-12 btn btn-lg btn-success"/>
                                    <!--<input type="hidden" value="${beer.itemPosition}" name="position"/>-->
                                    <!--<input name="change" value="${param.change}" type="hidden"/>-->
                                </span>
                            </form>
                        </c:forEach>
                    </div>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <br/>
                    <div class="container">
                        <div class="row">
                            <h1 class="col-lg-12">
                                <div class="col-lg-9">
                                    <p>${message}<br/></p>
                                    <br/>
                                    <br/>
                                    <c:if test="${change.userHasChange > 0}">
                                        Dollars: ${change.totalDollars}<br/>
                                        Quarters ${change.totalQuarters}<br/>
                                        Dimes: ${change.totalDimes}<br/>
                                        Nickels: ${change.totalNickels}<br/>
                                        Pennies: ${change.totalPennies}<br/>
                                    </c:if>
                                </div>

                                    <div id="totalDisplay" class="col-lg-3 label label-default">
                                    ${total}
                                </div>
                            </h1>
                            <div class="col-lg-offset-9">
                                <br/>
                                <br/>
                                <br/>
                                <a class="col-lg-12 btn btn-info" href="${pageContext.request.contextPath}/add5Dollar">Add $5 Dollars</a>
                                <br/>
                                <a class="col-lg-12 btn btn-info" href="${pageContext.request.contextPath}/addDollar">Add A Dollar</a>
                                <br/>
                                <a class="col-lg-12 btn btn-info" href="${pageContext.request.contextPath}/addQuarter">Add A Quarter</a>
                                <br/>
                                <a class="col-lg-12 btn btn-info" href="${pageContext.request.contextPath}/addDime">Add A Dime</a>
                                <br/>
                                <a class="col-lg-12 btn btn-info" href="${pageContext.request.contextPath}/addNickel">Add A Nickel</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
