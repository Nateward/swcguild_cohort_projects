/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.ineritancespecializationexample.employees;

import com.tsg.ineritancespecializationexample.employees.Employee;

/**
 *
 * @author ilyagotfryd
 */
public class Manager extends Employee {
    
    private String title;
    
    public Manager()
    {
        super("Nemo", "098-76-4321");
        title = "PM";
        //this("Nemo", "098-76-4321"); - also possible
    }
    
    public Manager(String name, String ssn)
    {
        super(name,ssn);
        title = "GM";
    }
    
    public void hire()
    {
        System.out.println("Welcome aboard!");
    }
    
    public void fire()
    {
        System.out.println("Your services are no longer required.");
    }
    
    public void givePerformanceReview()
    {
        System.out.println(name + "said: You did well, try to do better!");
    }
    
    @Override
    public void collectBonus()
    {
        System.out.println("Collect your 20% of anual pay.");
    }
    
    @Override
    public void createObjectives()
    {
        System.out.println("Make everyone work harder!!!");
        super.createObjectives();
    }
    
}
