/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.stackexamples;

import com.tsg.stackexample.generic.ArrayStackGeneric;
import com.tsg.stackexample.generic.StackInterfaceGeneric;
import java.util.Iterator;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class StackDriverApp {

    public static void main(String[] args) {
        
        StackInterface stack = new ArrayStack();

        StackInterfaceGeneric<String> genericStack = new ArrayStackGeneric<>();
        
        String a = "a";
        String b = "b";
        String c = "42";
        String d = "d";
        String e = "e";
        StackInterface f = new ArrayStack();

        System.out.println("Pushing " + a);
        stack.push(a);
        
        System.out.println("\nPushing " + b);
        stack.push(b);
        
        System.out.println("\nPushing " + c);
        stack.push(c);
        
        System.out.println("\nPushing " + d);
        stack.push(d);
        
        System.out.println("\nPushing " + e);
        stack.push(e);
        
        System.out.println("\nPushing " + f);
        stack.push(f);
        
        Iterator itr = stack.iterator();
        System.out.println("Printing values in step using iterator...");
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        System.out.println("Printing stack values using enhanced for loop...");
        for (Object o : stack) {
            System.out.println(o);
        }
        

        System.out.println("\nPopping... ");
        System.out.println("value : " + stack.pop());

        System.out.println("\nPopping... ");
        System.out.println("value : " + stack.pop());

        System.out.println("\nPopping... ");
        System.out.println("value : " + stack.pop());

        System.out.println("\nPopping... ");
        System.out.println("value : " + stack.pop());

        System.out.println("\nPopping... ");
        System.out.println("value : " + stack.pop());

    }
}
