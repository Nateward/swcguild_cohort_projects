/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.twomorequestions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class TwoMoreQuestions {
    public static void main(String[] args) {
        
        Scanner keyboard = new Scanner(System.in);
        
        String type = "";
        String living = "";
        String guess = "";
        
        System.out.println("TWO MORE QUESTIONS, BABY!\n");
        
        System.out.println("Think of an something and I'll try to guess it.");
        
        
        System.out.println("Question 1) Does it stay inside or outside or both?");
        System.out.println(">>> ");
        type = keyboard.next();        
        
        System.out.println("Question 2) Is it a living thing?");
        System.out.println(">>> ");
        living = keyboard.next();
        
        if(type.equalsIgnoreCase("inside") && living.equalsIgnoreCase("yes")){
                guess = " cat!";
            }
        
        if(type.equalsIgnoreCase("inside") && living.equalsIgnoreCase("no")){
            guess = " zombie!!!";
        }
        
        
        if((type.equalsIgnoreCase("outside")) && (living.equalsIgnoreCase("yes"))){
            guess = "n ocelot!";
        }
        if(type.equalsIgnoreCase("outside") && living.equalsIgnoreCase("no")){
            guess = " zombie!!!";
        }
        
        
        if(type.equalsIgnoreCase("both") && living.equalsIgnoreCase("yes")){
            guess = " dog!";
        }
        
        if(type.equalsIgnoreCase("both") && living.equalsIgnoreCase("noS")){
            guess = " zombie!";
        }
        
        System.out.println("My guess is that you are thinking of a" + guess);
        System.out.println("I would ask you if I'm right, but I don't actually care.");
    }
    
}
