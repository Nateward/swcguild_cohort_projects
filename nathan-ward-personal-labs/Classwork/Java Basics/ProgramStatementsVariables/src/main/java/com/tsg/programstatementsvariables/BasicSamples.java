/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.programstatementsvariables;

/**
 *
 * @author apprentice
 */

/*
this  is a main method
we can have multi line comments here.
*/
public class BasicSamples {
    public static void main(String[] args) {
        System.out.println("Print something here" /*something else here*/);
        System.out.println("Test this as well"); // single line commment here
        
//        
//        System.out.println("Print something here");
//        System.out.println("Test this as well"); 

         boolean testBool = true;
         char testChar = 'Z';
         testChar = '\n';
        System.out.println("Sample string with \n some new lines.");
        testChar = '\u1234';
        System.out.println("Unicode character here: " + testChar);
        
        
//        This is a declaration and assingment
        double testDouble = 3.14;
        testDouble = 3.14E-89;
        testDouble = 3.66d;
        float testFloat = 3.141234f;
        
        int testInt = 189;
        //compiler would not allow me to make this assignment
//        testInt = 9999999999999999999999999999999;

        testInt = 0XFA3D; //this is hexidecimal
        
//        This is exclusively declaring a variable
        long SampleDeclaration;
        System.out.println("sampleDeclaration");
        
        int sum = 5 + 2;
    }
    
}
