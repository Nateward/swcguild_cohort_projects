/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.interfaceexamples;

/**
 *
 * @author ilyagotfryd
 */
public interface Colorable {
    public void setColor(String color);
    public String getColor();
}
