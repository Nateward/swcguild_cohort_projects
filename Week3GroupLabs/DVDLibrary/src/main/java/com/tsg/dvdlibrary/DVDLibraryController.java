/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrary;

import com.tsg.addressbook.ui.ConsoleIO;
import com.tsg.dvdlibrary.dao.DVDLibraryDAO;
import com.tsg.dvdlibrary.dto.DVD;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class DVDLibraryController {

    private DVDLibraryDAO dvdLibrary = new DVDLibraryDAO();
    private ConsoleIO con = new ConsoleIO();

    public void run() {
        try {
            dvdLibrary.loadDvdLibrary();
            con.print("Dvd library loaded.\n");
        } catch (FileNotFoundException ex) {
            Logger.getLogger(DVDLibraryController.class.getName()).log(Level.SEVERE, null, ex);
            con.print("Their was an error loading oyur dvd library?");
        }

        int userInput = 0;
        while (userInput != 7) {
            printMenu();

            userInput = con.getInteger("Please choose an option: ", 1, 7);

            switch (userInput) {

                case 1:
                    addDvd();
                    break;
                case 2:
                    deleteDvd();
                    break;
                case 3:
                    editDvd();
                    break;
                case 4:
                    listAllDvds();
                    break;
                case 5:
                    printDvdInfo();
                    break;
                case 6:
                    saveFile();
                    break;
                default:
                    break;

            }
        }
         saveFile();
            con.print("Thank for using Dvd Library 1.0!!");

    }

    private void printMenu() {
        con.print("1.) Add DVD");
        con.print("2.) Remove DVD");
        con.print("3.) Edit DVD");
        con.print("4.) List all DVDs");
        con.print("5.) Print DVD Info");
        con.print("6.) Save File");
        con.print("7.) Quit\n");
    }

    private void addDvd() {
        DVD dvd = new DVD();

        dvd.setTitle(con.getString("Title: "));
        dvd.setDirector(con.getString("Director: "));
        dvd.setReleaseDate(con.getString("Release Date: "));
        dvd.setStudio(con.getString("Studio: "));
        dvd.setMpaaRating(con.getString("MPAA Rating: "));
        dvd.setReviewerRating(con.getInteger("Your Rating(1-5): ", 1, 5));
        dvd.setNotes(con.getString("Your Notes: "));

        dvdLibrary.addDvd(dvd);
    }

    private void deleteDvd() {
        DVD dvd = getDvdFromUser("Please enter title: ");
        if (dvd != null) {
            printDvd(dvd);
            if (con.getString("Are you sure? Y/N ").equalsIgnoreCase("y")) {
                dvdLibrary.deleteDvdById(dvd.getIndexNumber());
                con.print(dvd.getTitle() + " removed successfully.\n");
            } else {
                con.print("Removal cancelled.\n");
            }
        } else {
            con.print("Removal cancelled. Title not found!\n");
        }
    }

    private DVD getDvdFromUser(String prompt) {
        String userDvdName = con.getString(prompt);

        ArrayList<DVD> dvdResults = dvdLibrary.getDvdByName(userDvdName);

            if (dvdResults.size() == 1) {
                return dvdResults.get(0);
            } else if(dvdResults.size() > 1) {
                con.print("More that one entry found.");
                for (int i = 0; i < dvdResults.size(); i++) {
                    DVD dvd = dvdResults.get(i);
                    con.print((i + 1) + ": " + dvd.getTitle());
                }
                int userChoice = con.getInteger("Please select from above. ", 1, dvdResults.size());
                return dvdResults.get(userChoice - 1);
            }
        
            return null;
        
    }

    private void listAllDvds() {
        ArrayList<DVD> dvds = dvdLibrary.getAllDvds();

        for (DVD dvd : dvds) {
            printDvd(dvd);
        }
    }

    private void printDvdInfo() {
        DVD dvd = getDvdFromUser("Please enter Title: ");

        if (dvd != null) {
            printDvd(dvd);
        } else {
            con.print("\nPrint cancelled. Title not found.\n");
        }
    }

    private void saveFile() {
        con.print("Saving...");

        try {
            dvdLibrary.writeDvdLibrary();
            con.print("DVD library saved successfully!!\n");
        } catch (IOException ex) {
            Logger.getLogger(DVDLibraryController.class.getName()).log(Level.SEVERE, null, ex);
            con.print("Error: Could not save library.\n");
        }

    }

    private void editDvd() {
        DVD dvd = getDvdFromUser("Please enter Title: ");
        int userChoice = 0;
        while (userChoice != 8) {

            printEditMenu(dvd);

            userChoice = con.getInteger("Please select a field to edit: ");

            switch (userChoice) {
                case 1:
                    dvd.setTitle(con.getString("New Title: "));
                    break;
                case 2:
                    dvd.setDirector(con.getString("New Director: "));
                    break;
                case 3:
                    dvd.setReleaseDate(con.getString("New Release Date: "));
                    break;
                case 4:
                    dvd.setStudio(con.getString("New Studio: "));
                    break;
                case 5:
                    dvd.setMpaaRating(con.getString("New MPAA Rating: "));
                    break;
                case 6:
                    dvd.setReviewerRating(con.getInteger("New Rating(1-5): ", 1, 5));
                    break;
                case 7:
                    dvd.setNotes(con.getString("New Notes: "));
                    break;
                default:
                    break;
            }
        }
        con.print("\n\nChanges successfully saved!");
    }

    private void printEditMenu(DVD dvd) {
        con.print("1.) Title: " + dvd.getTitle());
        con.print("2.) Director: " + dvd.getDirector());
        con.print("3.) Release Date: " + dvd.getReleaseDate());
        con.print("4.) Studio: " + dvd.getStudio());
        con.print("5.) MPAA: " + dvd.getMpaaRating());
        String ratingAsStars = "";
        for (int i = 0; i < dvd.getReviewerRating(); i++) {
            ratingAsStars += "*";
        }
        con.print("6.) Your Rating: " + ratingAsStars);
        con.print("7.) Notes: " + dvd.getNotes());
        con.print("8.) Save & Return to Menu");
    }

    private void printDvd(DVD dvd) {
        con.print("Title: " + dvd.getTitle());
        con.print("Director: " + dvd.getDirector());
        con.print("Release Date: " + dvd.getReleaseDate());
        con.print("Studio: " + dvd.getStudio());
        con.print("MPAA: " + dvd.getMpaaRating());
        String ratingAsStars = "";
        for (int i = 0; i < dvd.getReviewerRating(); i++) {
            ratingAsStars += "*";
        }
        con.print("Your Rating: " + ratingAsStars);
        con.print("Notes: " + dvd.getNotes());
        con.print("");
    }

}
