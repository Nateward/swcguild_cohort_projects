/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibraryv2.dao;

import com.tsg.dvdlibraryv2.dto.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface DVDLibraryV2Interface {
    public void addDVD(DVD dvd);
    public DVD removeDVD(Integer index);
    public Collection<DVD> getDVDByTitle(String title);
    public Collection<DVD> getDVDByMPAA(String MPAARating);
    public Map<String,List<DVD>> getDVDByDirector(String director);
    public Collection<DVD> getDVDByStudio(String studio);
    public Collection<DVD> getNewestDVD();
    public Collection<DVD> getOldestDVD();
    public double getAverageNoOfNotesAssociate();
    
}
