/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook.dao;

import com.tsg.springaddressbook.dto.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.function.Predicate;
import java.util.stream.Collectors;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AddressBookDAOImpl implements AddressBookDAO {

    Map<Integer, Address> AddressMap;

    public AddressBookDAOImpl() {
        this.AddressMap = new HashMap<>();
    }

    @Override
    public List<Address> GetAll() {
        return AddressMap.values().stream().collect(Collectors.toList());

    }

    @Override
    public Address GetById(int id) {
        Address result = null;
        for (Address address : AddressMap.values()) {
            if (address.getIdNumber()== id) {
                result = address;
                break;
            }
        }
        return result;
    }

    @Override
    public Address Create(Address dto) {
        dto.setIdNumber(this.GetNextId());
        this.AddressMap.put(dto.getIdNumber(), dto);
        return dto;
    }

    @Override
    public void Update(int id, Address dto) {
        if (AddressMap.containsKey(id)) {
            AddressMap.put(id, dto);
        }
    }

    @Override
    public void Delete(int id) {
                AddressMap.remove(id);
    }

    @Override
    public List<Address> searchContacts(Map<SearchTerm, String> criteria) {
        String firstNameCriteria = criteria.get(SearchTerm.FIRSTNAME);
        String lastNameCriteria = criteria.get(SearchTerm.LASTNAME);
        String streetCriteria = criteria.get(SearchTerm.STREETADDRESS);
        String cityCriteria = criteria.get(SearchTerm.CITY);
        String stateCriteria = criteria.get(SearchTerm.STATE);
        String zipCriteria = criteria.get(SearchTerm.ZIP);

        Predicate<Address> firstNameFilter;
        Predicate<Address> lastNameFilter;
        Predicate<Address> streetFilter;
        Predicate<Address> cityFilter;
        Predicate<Address> stateFilter;
        Predicate<Address> zipFilter;
        
        Predicate<Address> truePredicate = (c) -> {
            return true;
        };

        firstNameFilter = (firstNameCriteria == null || firstNameCriteria.isEmpty()) ? truePredicate : (c) -> c.getFirstName().equals(firstNameCriteria);
        lastNameFilter = (lastNameCriteria == null || lastNameCriteria.isEmpty()) ? truePredicate : (c) -> c.getLastName().equals(lastNameCriteria);
        streetFilter = (streetCriteria == null || streetCriteria.isEmpty()) ? truePredicate : (c) -> c.getStreetAddress().equals(streetCriteria);
        cityFilter = (cityCriteria == null || cityCriteria.isEmpty()) ? truePredicate : (c) -> c.getCity().equals(cityCriteria);
        stateFilter = (stateCriteria == null || stateCriteria.isEmpty()) ? truePredicate : (c) -> c.getState().equals(stateCriteria);
        zipFilter = (zipCriteria == null || zipCriteria.isEmpty()) ? truePredicate : (c) -> c.getZip().equals(zipCriteria);

        return AddressMap.values().stream()
                .filter(firstNameFilter
                        .and(lastNameFilter)
                        .and(streetFilter)
                        .and(cityFilter)
                        .and(stateFilter)
                        .and(zipFilter))
                .collect(Collectors.toList());
    }
    
    private Integer GetNextId() {
        Integer result = 0;
        for (Address address : AddressMap.values()) {
            if (address.getIdNumber()> result) {
                result = address.getIdNumber();
            }
        }
        result++;
        return result;
    }

}
