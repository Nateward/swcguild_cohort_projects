/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.operations;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.aspectj.lang.JoinPoint;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AuditAspect {

    LocalDateTime today = LocalDateTime.now();
    private final String AUDIT_FILE = "AuditFile.txt";
    private final String AUDIT_DELIMITER = "::";

    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
    String formattedDate = today.format(formatter);

    public void timeLoggerMethod(JoinPoint jp) throws IOException {

        PrintWriter save = null;

        save = new PrintWriter(new FileOutputStream(new File(AUDIT_FILE), true));
        
        save.append(formattedDate + "-" + jp.getSignature().getDeclaringTypeName() 
                + " with method: " + jp.getSignature().getName() + "\n");
        save.flush();
        save.close();
        
    }
   

    public String getAUDIT_DELIMITER() {
        return AUDIT_DELIMITER;
    }
}
