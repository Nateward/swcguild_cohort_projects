/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.elseandif;

/**
 *
 * @author apprentice
 */
public class ElseAndIf {
    public static void main( String[] args )
	{
		int people = 30;
		int cars = 40;
		int buses = 15;
                
//                if cars(40) are greater than people(30) print out this line in the console.
		if ( cars > people )
		{
			System.out.println( "We should take the cars." );
		}
                
//                if cars(40) are less than people(30) print out this line in the console.
		else if ( cars < people )
		{
			System.out.println( "We should not take the cars." );
		}
                
//                if cars and people are equal do this, or print this line out
		else
		{
			System.out.println( "We can't decide." );
		}

//               if buses(15) are greater than cars(40) print out this line in the console.
		if ( buses > cars )
		{
			System.out.println( "That's too many buses." );
		}
                
//                if buses(15) is less than cars(40) print out this line in the console.
		else if ( buses < cars )
		{
			System.out.println( "Maybe we could take the buses." );
		}
                
//                if buses and cars are equal print this line out
		else
		{
			System.out.println( "We still can't decide." );
		}

//                if people(30) are greater than buses(15) print out this line in the console.
		if ( people > buses )
		{
			System.out.println( "All right, let's just take the buses." );
		}
                
//                if people are equal  or less than buses print this on the console.
//                And if i delete the else statement here and people were less 
//                than buses nobody would know if they should just take the buses or if they should just stay home. 
		else
		{
			System.out.println( "Fine, let's stay home then." );
		}

	}
    
}
