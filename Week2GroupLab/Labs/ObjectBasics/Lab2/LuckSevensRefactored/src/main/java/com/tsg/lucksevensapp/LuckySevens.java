/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lucksevensapp;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens {

    public static void gameLogic() {
        Scanner userInput = new Scanner(System.in);
        Random randomGenerator = new Random();
        int bet, startingBet, roll, currentRoll, bestBet, bestRoll;

        bet = 0;
        startingBet = 0;
        roll = 0;
        currentRoll = 0;
        bestBet = 0;
        bestRoll = 0;

        System.out.println("Enter starting Bet:");
        startingBet = userInput.nextInt();
        bet = startingBet;

        while (bet > 0) {
            if (bet > 0) {
                int die1 = randomGenerator.nextInt(6) + 1;
                int die2 = randomGenerator.nextInt(6) + 1;
                int dieTotal = (die1 + die2);

                if (dieTotal == 7) {
                    roll = roll + 1;
                    currentRoll = currentRoll + 1;
                    bet = bet + 4;
                } else {
                    roll = roll + 1;
                    currentRoll = currentRoll + 1;
                    bet = bet - 1;
                }
                if (bet > bestBet) {
                    bestBet = bet;
                    bestRoll = currentRoll;
                }
            }

        }

        System.out.println("Starting bet was: " + startingBet);
        System.out.println("Your are broke after: " + roll + " roll(s).");
        System.out.println("You should have quit after " + bestRoll + " roll(s) when had " + bestBet + ".");
    }
}
