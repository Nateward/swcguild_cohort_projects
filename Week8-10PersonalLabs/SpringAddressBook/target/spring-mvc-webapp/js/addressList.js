/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function () {
    loadAddress();
})

function loadAddress() {
    clearAddressTable();

    var aTable = $("#contentRows");

    $.each(testAddressData, function (index, address) {
        aTable.append($('<tr>')
                .append($('<td>').append($('<a>')
                        .attr({
                            'data-address-id': address.idNumber,
                            'data-toggle': '#detailsModal'
                        })
                        .text(address.firstName + " " + address.lastName)))
                .append($('<td>').text(address.city))
                .append($('<td>').text(address.state))
                .append($('<td>').append($('<a>')
                        .attr({
                            'data-address-id': address.idNumber,
                            'data-toggle': 'modal',
                            'data-target': '#editModal'
                        })
                        .text("Edit")))
                .append($('<td>').text("Delete"))
                );
    });
}

function clearAddressTable() {
    $("contentRows").empty();
}

$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var contactId = element.data('address-id');

    var modal = $(this);

//Stopped here
    modal.find('#address-id').text(dummyDetailsContact.idNumber);
    modal.find('#address-firstName').text(dummyDetailsContact.firstName);
    modal.find('#address-lastName').text(dummyDetailsContact.lastName);
    modal.find('#address-address').text(dummyDetailsContact.streetAddress);
    modal.find('#address-city').text(dummyDetailsContact.city);
    modal.find('#address-state').text(dummyDetailsContact.state);
    modal.find('#address-zip').text(dummyDetailsContact.zip);
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var contactId = element.data('address-id');

    var modal = $(this);

    modal.find('#address-id').text(dummyEditContact.idNumber);
    modal.find('#edit-firstName').val(dummyEditContact.firstName);
    modal.find('#edit-lastName').val(dummyEditContact.lastName);
    modal.find('#edit-street').val(dummyEditContact.streetAddress);
    modal.find('#edit-city').val(dummyEditContact.city);
    modal.find('#edit-state').val(dummyEditContact.state);
    modal.find('#edit-zip').val(dummyEditContact.zip);
});