/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.contactlistmvc.dao;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public enum SearchTerm {
    FIRST_NAME("firstName"),
    LAST_NAME("lastName"), 
    COMPANY("company"), 
    PHONE("phone"), 
    EMAIL("email");
    
    private String fieldName;
    
    private SearchTerm( String fieldName ) {
        this.fieldName = fieldName;
    }
    
    
    public String getFieldName() {
        return fieldName;
    }
}
