/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.statecapitalslab2;

import java.util.HashMap;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class Capitals {

    HashMap<String, Capitals> dataList = new HashMap<>();
    Scanner Sc = new Scanner(System.in);
    
    private float userInput;    

    private String capitalName;
    private double population;
    private double squaremiles;

    public Capitals() {

        dataList.put("Alabama", new Capitals("Montgomery", 155.4, 374536));
        dataList.put("Alaska", new Capitals("Juneau", 2716.7, 9876));
        dataList.put("Arizona", new Capitals("Phoenix", 474.9, 4192887));
        dataList.put("Arkansas", new Capitals("Little Rock", 116.2, 877091));
        dataList.put("California", new Capitals("Sacramento", 97.2, 2527123));
        dataList.put("Colorado", new Capitals("Denver", 153.4, 2552195));
        dataList.put("Connecticut", new Capitals("Hartford", 17.3, 1212381));
        dataList.put("Delaware", new Capitals("Dover", 22.4, 162310));
        dataList.put("District of Columbia", new Capitals("Washington", 61.4, 9051961));
        dataList.put("Florida", new Capitals("Tallahassee", 95.7, 367413));
        dataList.put("Georgia", new Capitals("Atlanta", 131.7, 5268860));
        dataList.put("Hawaii", new Capitals("Honolulu", 85.7, 953207));
        dataList.put("Idaho", new Capitals("Boise", 63.8, 616561));
        dataList.put("Illinois", new Capitals("Springfield", 54, 208182));
        dataList.put("Indiana", new Capitals("Indianapolis", 361.5, 1756221));
        dataList.put("Iowa", new Capitals("Des Moines", 75.8, 580255));
        dataList.put("Kansas", new Capitals("Topeka", 56, 230824));
        dataList.put("Kentucky", new Capitals("Frankfort", 14.7, 70758));
        dataList.put("Louisiana", new Capitals("Baton Rouge", 76.8, 802484));
        dataList.put("Maine", new Capitals("Augusta", 55.4, 117114));
        dataList.put("Maryland", new Capitals("Annapolis", 6.73, 238495));
        dataList.put("Massachusetts", new Capitals("Boston", 48.4, 4522858));
        dataList.put("Michigan", new Capitals("Lansing", 35, 464036));
        dataList.put("Minnesota", new Capitals("Saint Paul", 52.8, 3502891));
        dataList.put("Mississippi", new Capitals("Jackson", 104.9, 539057));
        dataList.put("Missouri", new Capitals("Jefferson City", 27.3, 149807));
        dataList.put("Montana", new Capitals("Helena", 14, 74801));
        dataList.put("Nebraska", new Capitals("Lincoln", 74.6, 302157));
        dataList.put("Nevada", new Capitals("Carson City", 143.4, 234879));
        dataList.put("New Hampshire", new Capitals("Concord", 64.3, 234567));
        dataList.put("New Jersey", new Capitals("Trenton", 7.66, 366513));
        dataList.put("New Mexico", new Capitals("Santa Fe", 37.3, 183732));
        dataList.put("New York", new Capitals("Albany", 21.4, 857592));
        dataList.put("North Carolina", new Capitals("Raleigh", 114.6, 1130490));
        dataList.put("North Dakota", new Capitals("Bismarck", 26.9, 108779));
        dataList.put("Ohio", new Capitals("Columbus", 210.3, 1967066));
        dataList.put("Oklahoma", new Capitals("Oklahoma City", 607, 1252987));
        dataList.put("Oregon", new Capitals("Salem", 45.7, 390738));
        dataList.put("Pennsylvania", new Capitals("Harrisburg", 8.11, 647390));
        dataList.put("Rhode Island", new Capitals("Providence", 18.5, 1630956));
        dataList.put("South Carolina", new Capitals("Columbia", 125.2, 913797));
        dataList.put("South Dakota", new Capitals("Pierre", 13, 9876543));
        dataList.put("Tennessee", new Capitals("Nashville", 473.3, 1582264));
        dataList.put("Texas", new Capitals("Austin", 251.5, 1716291));
        dataList.put("Utah", new Capitals("Salt Lake City", 109.1, 1124197));
        dataList.put("Vermont", new Capitals("Montpelier", 10.2, 567543));
        dataList.put("Virginia", new Capitals("Richmond", 60.1, 1231675));
        dataList.put("Washington", new Capitals("Olympia", 16.7, 234670));
        dataList.put("West Virginia", new Capitals("Charleston", 31.6, 304214));
        dataList.put("Wisconsin", new Capitals("Madison", 68.7, 561505));
        dataList.put("Wyoming", new Capitals("Cheyenne", 21.1, 91738));

    }

    public Capitals(String capitalName, double population, double squaremiles) {
        this.capitalName = capitalName;
        this.population = population;
        this.squaremiles = squaremiles;
        userInput = 0;
    }

    public void printAll(){
        Set<String> keys = dataList.keySet();
        
        System.out.println("Please enter a population number to see the states that equal or exceed this amount: ");
        userInput = Sc.nextFloat();
        
        for (String key : keys) {
            
            Capitals test = dataList.get(key);
            
            if (test.population >= userInput) {
            
                System.out.println(key + " - " + test.capitalName + " - " + test.population + " - " + test.squaremiles + "\n");
            }
            
            
            
            
        }
    }
}
