/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.InterestCalculatorRefactor;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class InterestCalculatorOO {
    
    public InterestCalculatorOO(){
        
    }
    
    public void compoundInterestC(){
        
        Scanner keyboard = new Scanner(System.in);
        
        float interestRate = 0.0f;
        float principal = 0.0f;
        int years = 0;
        float interestEarned = 0.0f;
        float money = 0.0f;
        float compoundRate = 0.0f;

        System.out.println("What is the annual interest: ");
        interestRate = keyboard.nextFloat();

        System.out.println("What is the initial amount of principal: ");
        principal = keyboard.nextFloat();

        System.out.println("What is the number of years the money is to stay in the fund: ");
        years = keyboard.nextInt();

        System.out.println("What is the number times the interest is compounded(e.i.- 365-daily, 12-monthly, 4-quarterly): ");
        compoundRate = keyboard.nextFloat();

        money = principal;

//        This for-loop is for the number of years

        for (int i = 1; i < years + 1; i++) {

            System.out.println("For year " + i + " I started with " + money + " dollars.");

            money = compoundLoop(money, interestRate, compoundRate);
            interestEarned = money - principal;

            System.out.println("For year " + i + " I earned " + interestEarned
                    + " and ended with " + money + " dollars.");

        }

        System.out.println("I started with " + principal + " dollars, and after "
                + years + "years, at " + interestRate + " percent interest, coumpounded "
                + compoundRate + " times annualy, I ended up with " + money + " dollars.");

    }

    
//    This method and for-loop is for finding the compounded interest within a year
    public static float compoundLoop(float money, float interestRate, float compoundRate) {
        float interestEarned = 0.0f;

        for (int i = 0; i < compoundRate; i++) {

            interestEarned = money * ((interestRate / 100) / compoundRate);
            money = (interestEarned + money);

        }
        return money;
    }
    
    
}
