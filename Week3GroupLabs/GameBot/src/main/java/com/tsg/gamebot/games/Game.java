/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.gamebot.games;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public interface Game {

    public void playGame();

    public String getGameName();

}
