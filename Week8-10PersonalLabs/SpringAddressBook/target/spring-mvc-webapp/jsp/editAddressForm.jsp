<%-- 
    Document   : NewContactFormNoAjax
    Created on : 29-Mar-2016, 10:24:07 AM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/phone-book.png" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
        </div>
        <div class="container">
            <h1>Edit Address Form</h1>
            <a href="displayAddressBook">View Address Book</a>
            <hr/>
            <sf:form modelAttribute="address" class="form-horizontal" role="form" action="editAddress" method="POST">
                <div class="form-group">
                    <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-first-name" path="firstName" placeholder="First Name"/>
                        <sf:errors path="firstName" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-last-name" path="lastName" placeholder="Last Name"/>
                        <sf:errors path="lastName" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-street" class="col-md-4 control-label">Street Address:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-street" path="streetAddress" placeholder="Street Address"/>
                        <sf:errors path="streetAddress" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-city" class="col-md-4 control-label">City:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-city" path="city" placeholder="City"/>
                        <sf:errors path="city" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-state" class="col-md-4 control-label">State:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-state" path="state" placeholder="State"/>
                        <sf:errors path="state" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-zip" class="col-md-4 control-label">Zip:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control" id="edit-zip" path="zip" placeholder="Zip"/>
                        <sf:errors path="zip" cssClass="text-danger"></sf:errors>
                        <sf:hidden path="idNumber"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="edit-button" class="btn btn-info">Edit Address</button>
                    </div>
                </div>
            </sf:form>
        </div>
    </body>
</html>
