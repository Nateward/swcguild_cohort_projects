<%-- 
    Document   : Result
    Created on : 26-Mar-2016, 4:30:21 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Interest Calculator - Results</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4 well">
                    <h1>Interest Results</h1>
                    <h3>For year 1, you started with ${param.principal} dollars.<br/></h3>
                    <p>
                        <c:forEach var = "figure" items="${figuresList}">
                            <p>For year ${figure.year} I earned ${figure.interestEarned} and ended with ${figure.money} dollars.<br/></p>
                        </c:forEach>
                        I started with ${param.principal} dollars, and after ${param.years} years, at ${param.interestRate} percent interest, compounded ${param.compoundRate} times annually, I ended up with ${finalMoney}  dollars.<br/>
                    </p>
                </div>
                <div class="col-lg-4"></div>
            </div>
            <div class="row">

                <div class="col-lg-5"></div>
                <div class="col-lg-7">
                    <a class="btn btn-success" href="InterestCalculatorServlet">Play Again</a>
                </div>
            </div>
        </div>        

    </body>
</html>
