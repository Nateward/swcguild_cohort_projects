-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 06, 2016 at 04:19 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `TheVenue_test`
--

-- --------------------------------------------------------

--
-- Table structure for table `Author`
--

CREATE TABLE IF NOT EXISTS `Author` (
  `AuthorId` int(11) NOT NULL AUTO_INCREMENT,
  `AuthorName` varchar(50) NOT NULL,
  PRIMARY KEY (`AuthorId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `Author`
--

INSERT INTO `Author` (`AuthorId`, `AuthorName`) VALUES
(1, 'Ryan'),
(2, 'Pankaj');

-- --------------------------------------------------------

--
-- Table structure for table `Authority`
--

CREATE TABLE IF NOT EXISTS `Authority` (
  `UserName` varchar(20) NOT NULL,
  `Authority` varchar(20) NOT NULL,
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Authority`
--

INSERT INTO `Authority` (`UserName`, `Authority`) VALUES
('admin', 'ROLE_ADMIN'),
('admin', 'ROLE_USER'),
('employee', 'ROLE_USER');

-- --------------------------------------------------------

--
-- Table structure for table `Category`
--

CREATE TABLE IF NOT EXISTS `Category` (
  `CategoryId` int(11) NOT NULL AUTO_INCREMENT,
  `CategoryName` varchar(50) NOT NULL,
  PRIMARY KEY (`CategoryId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=51 ;

-- --------------------------------------------------------

--
-- Table structure for table `NavBarLocation`
--

CREATE TABLE IF NOT EXISTS `NavBarLocation` (
  `NavBarLocationId` int(11) NOT NULL AUTO_INCREMENT,
  `PageFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`NavBarLocationId`),
  KEY `PageFk` (`PageFk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=27 ;

--
-- Dumping data for table `NavBarLocation`
--

INSERT INTO `NavBarLocation` (`NavBarLocationId`, `PageFk`) VALUES
(3, NULL),
(4, NULL),
(5, NULL),
(6, NULL),
(7, NULL),
(8, NULL),
(9, NULL),
(10, NULL),
(11, NULL),
(12, NULL),
(13, NULL),
(14, NULL),
(15, NULL),
(16, NULL),
(17, NULL),
(18, NULL),
(19, NULL),
(20, NULL),
(1, 104),
(2, 105);

-- --------------------------------------------------------

--
-- Table structure for table `Page`
--

CREATE TABLE IF NOT EXISTS `Page` (
  `PageId` int(11) NOT NULL AUTO_INCREMENT,
  `URLTitle` varchar(60) NOT NULL,
  `Title` varchar(60) NOT NULL,
  `NavName` varchar(20) NOT NULL,
  `Heading` varchar(200) DEFAULT NULL,
  `Body` longtext NOT NULL,
  `ParentPageFk` int(11) DEFAULT NULL,
  PRIMARY KEY (`PageId`),
  UNIQUE KEY `Title` (`Title`),
  UNIQUE KEY `URLTitle` (`URLTitle`),
  KEY `ParentPageFk` (`ParentPageFk`),
  KEY `ParentPageFk_2` (`ParentPageFk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=116 ;

-- --------------------------------------------------------

--
-- Table structure for table `Post`
--

CREATE TABLE IF NOT EXISTS `Post` (
  `PostId` int(11) NOT NULL AUTO_INCREMENT,
  `Title` varchar(200) NOT NULL,
  `UrlTitle` varchar(200) NOT NULL,
  `AuthorFk` int(11) NOT NULL,
  `Body` longtext NOT NULL,
  `PublishDate` date NOT NULL,
  `StartDate` datetime DEFAULT NULL,
  `EndDate` datetime DEFAULT NULL,
  `StatusFk` int(11) NOT NULL,
  PRIMARY KEY (`PostId`),
  KEY `authorFk` (`AuthorFk`),
  KEY `statusFk` (`StatusFk`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=59 ;

-- --------------------------------------------------------

--
-- Table structure for table `PostCategory`
--

CREATE TABLE IF NOT EXISTS `PostCategory` (
  `PostFk` int(11) NOT NULL,
  `CategoryFk` int(11) NOT NULL,
  KEY `blogFk` (`PostFk`),
  KEY `CategoryFk` (`CategoryFk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `PostTag`
--

CREATE TABLE IF NOT EXISTS `PostTag` (
  `PostFk` int(11) NOT NULL,
  `TagFk` int(11) NOT NULL,
  KEY `blogFk` (`PostFk`),
  KEY `tagFk` (`TagFk`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `Status`
--

CREATE TABLE IF NOT EXISTS `Status` (
  `StatusId` int(11) NOT NULL AUTO_INCREMENT,
  `StatusName` varchar(20) NOT NULL,
  PRIMARY KEY (`StatusId`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Status`
--

INSERT INTO `Status` (`StatusId`, `StatusName`) VALUES
(1, 'Published'),
(2, 'Archived'),
(3, 'Draft'),
(4, 'Unapproved');

-- --------------------------------------------------------

--
-- Table structure for table `Tag`
--

CREATE TABLE IF NOT EXISTS `Tag` (
  `TagId` int(11) NOT NULL AUTO_INCREMENT,
  `TagName` varchar(20) NOT NULL,
  PRIMARY KEY (`TagId`),
  UNIQUE KEY `name` (`TagName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=85 ;

-- --------------------------------------------------------

--
-- Table structure for table `User`
--

CREATE TABLE IF NOT EXISTS `User` (
  `UserId` int(11) NOT NULL AUTO_INCREMENT,
  `UserName` varchar(20) NOT NULL,
  `Email` varchar(50) NOT NULL,
  `Password` varchar(50) NOT NULL,
  `Enabled` tinyint(1) NOT NULL,
  PRIMARY KEY (`UserId`),
  KEY `UserName` (`UserName`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `User`
--

INSERT INTO `User` (`UserId`, `UserName`, `Email`, `Password`, `Enabled`) VALUES
(1, 'admin', '', 'password', 1),
(2, 'employee', '', 'password', 1);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `Authority`
--
ALTER TABLE `Authority`
  ADD CONSTRAINT `Authority_ibfk_1` FOREIGN KEY (`UserName`) REFERENCES `User` (`UserName`);

--
-- Constraints for table `Post`
--
ALTER TABLE `Post`
  ADD CONSTRAINT `Post_ibfk_1` FOREIGN KEY (`AuthorFk`) REFERENCES `Author` (`AuthorId`),
  ADD CONSTRAINT `Post_ibfk_2` FOREIGN KEY (`StatusFk`) REFERENCES `Status` (`StatusId`);

--
-- Constraints for table `PostCategory`
--
ALTER TABLE `PostCategory`
  ADD CONSTRAINT `PostCategory_ibfk_1` FOREIGN KEY (`PostFk`) REFERENCES `Post` (`PostId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `PostCategory_ibfk_2` FOREIGN KEY (`CategoryFk`) REFERENCES `Category` (`CategoryId`);

--
-- Constraints for table `PostTag`
--
ALTER TABLE `PostTag`
  ADD CONSTRAINT `PostTag_ibfk_1` FOREIGN KEY (`PostFk`) REFERENCES `Post` (`PostId`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `PostTag_ibfk_2` FOREIGN KEY (`TagFk`) REFERENCES `Tag` (`TagId`) ON DELETE NO ACTION ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
