/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.gamebot.games;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class RockPaperScissors implements Game{
    
    private final String gameName = "Rock-Paper-Scissor";
    
    
    @Override
    public String getGameName() {
        return this.gameName;
    }
    
    
    @Override
    public void playGame() {

        Scanner sc = new Scanner(System.in);
        int rounds = GetRounds();
        int counter = 1;
        int numberUserWins = 0;
        int numberComputerWins = 0;
        int numberTies = 0;
        boolean playAgain;

        if ((rounds >= 1) && (rounds <= 10)) {

            do {
                do {

                    DisplayHeading(counter);
                    int userChoice = GetUserChoice();
                    int computerChoice = GetComputerChoice();
                    int winner = GetResults(userChoice, computerChoice);

                    switch (winner) {

                        case 0:
                            numberTies++;
                            break;

                        case 1:
                            numberUserWins++;
                            break;

                        case 2:
                            numberComputerWins++;
                            break;

                    }

                    DisplayRoundResults(userChoice, computerChoice, winner);
                    counter++;


                } while (counter <= rounds);

            DisplayGameResults(numberTies, numberUserWins, numberComputerWins);
            
            playAgain = PlayAgain();
            
            }while(playAgain == true);

        } else {

            System.out.println("That's not an option. See ya later, alligator!");

        }
        
        System.out.println("Thanks for playing. Goodbye!");

    }

    public static void DisplayHeading(int r) {

        System.out.println("\n------------------------------------------------------------------------");
        System.out.println("\nRound #" + r + ":");
        System.out.println("\nPlease pick your weapon (1/2/3)");
        System.out.println("1 = rock");
        System.out.println("2 = paper");
        System.out.println("3 = scissors");
        System.out.print("\nRock, Paper, Scissors, GO!: ");

    }

    public static int GetResults(int userChoice, int computerChoice) {

        int winner;

        if ((userChoice == computerChoice)) {

            winner = 0;

        } else if ((userChoice == 1) && (computerChoice == 2)) {

            winner = 1;

        } else if ((userChoice == 2) && (computerChoice == 3)) {

            winner = 1;

        } else if ((userChoice == 3) && (computerChoice == 1)) {

            winner = 1;

        } else {

            winner = 2;

        }

        return winner;

    }

    public static void DisplayRoundResults(int userChoice, int computerChoice, int winner) {

        String[] weaponText = {"null", "rock", "paper", "scissors"};

        System.out.println("You picked " + weaponText[userChoice] + ".");
        System.out.println("I picked " + weaponText[computerChoice] + ".");

        switch (winner) {

            case 0:
                System.out.println("\nWe tied.");
                break;

            case 1:
                System.out.println("\nYou won!");
                break;

            case 2:
                System.out.println("\nI won.");
                break;

        }

    }

    public static void DisplayGameResults(int numberTies, int numberUserWins, int numberComputerWins) {

        System.out.println("\n------------------------------------------------------------------------");
        System.out.println("\nOVERALL RESULTS");
        System.out.println("---------------");
        System.out.println("We tied: " + numberTies);
        System.out.println("You won: " + numberUserWins);
        System.out.println("I won: " + numberComputerWins);
        System.out.println("");

        if (numberUserWins > numberComputerWins) {

            System.out.println("You won the game!");

        } else if (numberComputerWins > numberUserWins) {

            System.out.println("I won the game!");

        } else {

            System.out.println("It's a tie!");

        }

        System.out.println("");

    }

    public static int GetUserChoice() {

        Scanner sc = new Scanner(System.in);
        int userChoice = sc.nextInt();
        return userChoice;

    }

    public static int GetComputerChoice() {

        Random r = new Random();
        int computerChoice = r.nextInt(3) + 1;
        return computerChoice;

    }

    public static int GetRounds() {

        Scanner sc = new Scanner(System.in);
        int rounds;
        System.out.println("Let's play Rock, Paper, Scissors!\nHow many rounds do you want to battle? (1 to 10)");
        rounds = sc.nextInt();
        return rounds;

    }
    
    public static boolean PlayAgain() {
        
        Scanner sc = new Scanner(System.in);
        boolean response = true, flag;
        String playAgain;
        
        do
        {
            System.out.println("Do you want to play again? (Yes/No)");
            playAgain = sc.nextLine();
            flag = true;
            
            switch (playAgain) {
                case "yes":
                case "Yes":
                    response = true;
                    break;
                case "no":
                case "No":
                    response = false;
                    break;
                default:
                    System.out.println("\nThat is an invalid response. Please answer the question: ");
                    flag = false;
                    break;
            }
        
        }while (flag == false);
        return response;
    }
    
}
