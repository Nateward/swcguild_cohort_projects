<%-- 
    Document   : Factor
    Created on : 25-Mar-2016, 8:05:39 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer</title>
    </head>
    <body>
        <h1>Please enter a number.</h1>
        <form class="form-horizontal" action='SpringFactorizerServlet' method='POST'>
            <input class="form-control" type="number" name="quantity" min="1" >
            <input type="submit" class ="btn btn-success" value="Show all the numbers"/>
        </form>
    </body>
</html>
