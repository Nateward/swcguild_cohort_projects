/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdv3.dao;
import com.swcguild.dvdlibrary.dto.Dvd;
import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 *
 * @author apprentice
 */
public class DVDLibraryImpl  implements DvdLibraryDao{
    ArrayList<Dvd> dvdList = new ArrayList<>();
    Integer index = 0;
    
    

    @Override
    public void add(Dvd dvd) {
        dvd.setId(index);
        dvdList.add(dvd);
        index++;
    }

    @Override
    public void remove(int index) {
        Dvd dvd = new Dvd();
        for (int i = 0; i < dvdList.size(); i++) {
            Dvd xdvd = dvdList.get(i);
            if (xdvd.getId() == index) {
                dvd = dvdList.remove(i);
            }           
            
        }
    }
    
    @Override
    public List<Dvd> listAll(){
        return dvdList;
    }
    
    @Override
    public Dvd getById(int index){
        Dvd dvd = new Dvd();
        for (int i = 0; i < dvdList.size(); i++) {
            Dvd xdvd = dvdList.get(i);
            if (xdvd.getId() == index) {
               return dvd;
            }           
            
        }
    return null;
    }

    @Override
    public List<Dvd> getByTitle(String title) {
        
        List<Dvd> dvdListByTitle = dvdList.stream()
                .filter(s ->s.getTitle().equalsIgnoreCase(title))
                .collect(Collectors.toList());
        return dvdListByTitle;
    }

    
    public List<Dvd> getByRating(String MPAARating) {
        List<Dvd> dvdListByMpaa = dvdList.stream()
                .filter(s ->s.getMpaaRating().equalsIgnoreCase(MPAARating))
                .collect(Collectors.toList());
        return dvdListByMpaa;
    }

    
    public Map<String,List<Dvd>> getByDirector(String director) {
        Map<String,List<Dvd>> directorNameByMPAA = dvdList.stream()
                .filter(l -> l.getDirector().equalsIgnoreCase(director))
                .collect(Collectors.groupingBy(s ->s.getMpaaRating()));
        
        return directorNameByMPAA;
    }

    @Override
    public List<Dvd> getByStudio(String studio) {
        List<Dvd> dvdListByStudio = dvdList.stream()
                .filter(s ->s.getStudio().equalsIgnoreCase(studio))
                .collect(Collectors.toList());
        return dvdListByStudio;
    }

    
    public Collection<Dvd> getNewestDVD() {
       /* dvdList.stream()
                .sorted((Dvd a,Dvd b)->{
                    if(Integer.parseInt(a.getReleaseDate())<Integer.parseInt(b.getReleaseDate())){
                        return -1;
                    }else if(Integer.parseInt(a.getReleaseDate())==Integer.parseInt(b.getReleaseDate())){
                        return 0;
                    }else{
                        return 1;
                    }
                }                
                );
        ArrayList<Dvd> newDvds = new ArrayList<>();
        
        for (int i = 0; i < dvdList.size(); i++) {
            if (dvdList.get(i).getReleaseDate().equals(dvdList.get(0).getReleaseDate())) {
                newDvds.add(dvdList.get(i));
            }
        }*/
        
     int newestYear = dvdList.stream()
             .mapToInt(s -> (s.getReleaseDate().getYear()))
             .max().getAsInt();
     
     Collection<Dvd> dvdCollection = dvdList.stream()
             .filter(s ->(s.getReleaseDate().getYear())==newestYear)
             .collect(Collectors.toList());   
                
          
                
       return dvdCollection;
   }

    
    public Collection<Dvd> getOldestDVD() {
        int oldestYear = dvdList.stream()
             .mapToInt(s -> (s.getReleaseDate().getYear()))
             .min().getAsInt();
      return  dvdList.stream()
                .filter(dvd ->(dvd.getReleaseDate().getYear())==oldestYear)
                .collect(Collectors.toList());
        
    }

    
    public double getAverageNoOfNotesAssociate() {
        Collection<Dvd> numOfDVDs = dvdList.stream()
                .collect(Collectors.toList());
        
        Collection<Dvd> numOfNotes = dvdList.stream()
                .filter(n -> n.getNote() != null)
                .collect(Collectors.toList());
        
        double total = numOfDVDs.size();
        double totalNotes = numOfNotes.size();
        return (totalNotes / total);
    }
    
}
