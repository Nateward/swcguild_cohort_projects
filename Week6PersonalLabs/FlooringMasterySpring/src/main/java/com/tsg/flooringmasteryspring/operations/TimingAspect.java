/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.operations;

import org.aspectj.lang.ProceedingJoinPoint;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class TimingAspect {
    
    
 public Object timeMethod(ProceedingJoinPoint jp) {
        Object timeLapse = null;

        try {
            long start = System.currentTimeMillis();
            timeLapse = jp.proceed();

            long end = System.currentTimeMillis();

            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");
            System.out.println(jp.getSignature().getName() + " took: " + (end - start) + " ms.");
            System.out.println("++++++++++++++++++++++++++++++++++++++++++++++");

        } catch (Throwable ex) {
            System.out.println("Exception in TimingAspect.timeMethod()");
        }
        return timeLapse;

    }    
}
