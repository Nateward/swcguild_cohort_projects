/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.jsoup.dto;

/**
 *
* @author Nathan Ward <nateward.nw@gmail.com>
*/
public class City {
    private String cityName;
    private String cityTag;

    /**
     * @return the cityName
     */
    public String getCityName() {
        return cityName;
    }

    /**
     * @param cityName the cityName to set
     */
    public void setCityName(String cityName) {
        this.cityName = cityName;
    }

    /**
     * @return the cityTag
     */
    public String getCityTag() {
        return cityTag;
    }

    /**
     * @param cityTag the cityTag to set
     */
    public void setCityTag(String cityTag) {
        this.cityTag = cityTag;
    }
    
}
