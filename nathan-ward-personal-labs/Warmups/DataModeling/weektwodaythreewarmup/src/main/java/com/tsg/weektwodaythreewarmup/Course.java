/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.weektwodaythreewarmup;

/**
 *
 * @author apprentice
 */
public class Course {

    private String courseName;
    private String courseNumber;
    private Student[] students;
    private int index;
    

    public Course(String courseName, String courseNumber) {
        students = new Student[10];
        this.courseName = courseName;
        this.courseNumber = courseNumber;
        
    }

    public Course(){
        students = new Student[10];
        this.courseName = "";
        this.courseNumber = "";
        this.index = 0;
    }
    /**
     * @return the courseName
     */
    public String getCourseName() {
        return courseName;
    }

    /**
     * @param courseName the courseName to set
     */
    public void setCourseName(String courseName) {
        this.courseName = courseName;
    }

    /**
     * @return the classNumber
     */
    public String getClassNumber() {
        return courseNumber;
    }

    /**
     * @param classNumber the classNumber to set
     */
    public void setClassNumber(String classNumber) {
        this.courseNumber = classNumber;
    }

    public void addStudent(Student student){
        students[index] = student;
        index++;
    }
   
    public void printCourseReport(){
        System.out.println("Course name: " + courseName);
        System.out.println("Course number: " + courseNumber);
        
        for( int i = 0; i < 10; i++){
        System.out.println("Student: " + students[i].getStudentName());
            System.out.println("Email: " + students[i].getEmail());}
    }
}