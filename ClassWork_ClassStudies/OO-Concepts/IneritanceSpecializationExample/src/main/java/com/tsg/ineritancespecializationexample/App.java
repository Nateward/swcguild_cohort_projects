package com.tsg.ineritancespecializationexample;

import com.tsg.ineritancespecializationexample.employees.Manager;
import com.tsg.ineritancespecializationexample.employees.Employee;
import com.tsg.ineritancespecializationexample.employees.SampleAbstract;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilyagotfryd
 */
public class App {
    
    public static void main(String[] args) {
        
        Employee someGuy = new Employee("Jeff Dunham", "123-45-6789");
        
        someGuy.doWork();
        someGuy.createObjectives();
        someGuy.collectBonus();
        
        System.out.println("=========== running boss as an employee here ==========");
        Employee boss = new Manager("Pointy haired boss","321-54-0987");
        
        boss.doWork();
        boss.createObjectives();
        boss.collectBonus();
        boss.setName("Pointy haired boss");
        
        //cast first ask questions later
        ((Manager)boss).hire();
        ((Manager)boss).fire();
        ((Manager)boss).givePerformanceReview();
        
        
        
    }
    
}
