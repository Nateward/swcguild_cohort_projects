/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Manager {
    private String firstName;
    private String lastName;
    private int age;
    private boolean onDuty;

    public Manager(String firstName, String lastName, int age, boolean onDuty) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
        this.onDuty = onDuty;
    }

    /**
     * @return the firstName
     */
    public String getFirstName() {
        return firstName;
    }

    /**
     * @param firstName the firstName to set
     */
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * @return the lastName
     */
    public String getLastName() {
        return lastName;
    }

    /**
     * @param lastName the lastName to set
     */
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * @return the age
     */
    public int getAge() {
        return age;
    }

    /**
     * @param age the age to set
     */
    public void setAge(int age) {
        this.age = age;
    }

    /**
     * @return the onDuty
     */
    public boolean isOnDuty() {
        return onDuty;
    }

    /**
     * @param onDuty the onDuty to set
     */
    public void setOnDuty(boolean onDuty) {
        this.onDuty = onDuty;
    }
    
    // Behavoirs
    /////////////
    
    
    public void walking(){
       
    }
    
    
    public void inspects(){
        
    }
    
    
    
}
