/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.olympianexample;

/**
 *
 * @author ilyagotfryd
 */
public class SkiJumper {
    
    private Event event;
    
    public SkiJumper()
    {
        event = new SkiJumpEvent();
    }
    
    public String competeInEevent()
    {
        return event.compete();
    }
    
}
