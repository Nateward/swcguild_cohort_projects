<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Index Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- My Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>

    <body>
        <div class="container">
            <br/>
            <br/>
            <br/>
            <h1 class="col-md-offset-4 col-md-8">Please enter your zip code or city name.</h1>
             <div class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="add-city" class="col-md-4 control-label">
                        City:
                    </label>

                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-city"
                               placeholder="City Name"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit"
                                id="city-button"
                                class="btn btn-default">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
            <div class="form-horizontal" role="form">
                <div class="form-group">
                    <label for="add-zip" class="col-md-4 control-label">
                        Zip:
                    </label>

                    <div class="col-md-8">
                        <input type="text"
                               class="form-control"
                               id="add-zip"
                               placeholder="Zip Code"/>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit"
                                id="zip-button"
                                class="btn btn-default">
                            Submit
                        </button>
                    </div>
                </div>
            </div>
            <h4 id="content-rows">
                <p>Temperature: <span id="weather-temp"></span></p>
                <p>Conditions: <span id="weather-description"></span><img id="weather-status-img"></p>
            </h4>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/openWeather.js"></script>
        <script>
//            $(document).ready(function () {
//                alert(weatherNow.coord.lon);
//            })
        </script>

    </body>
</html>

