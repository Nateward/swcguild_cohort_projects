/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrills;

import com.tsg.junitdrills.SameFirstLast;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class SameFirstLastJUnitTest {
    
    public SameFirstLastJUnitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    
     @Test
    public void justTheFirst()
    {
    SameFirstLast test = new SameFirstLast();
    int[] array = {1, 2, 3};
    assertFalse(test.SameFirstLast(array));
    }
    
    
    @Test
    public void firstAndLastBig()
    {
    SameFirstLast test = new SameFirstLast();
    int[] array = {1, 2, 3, 1};
    assertTrue(test.SameFirstLast(array));
    }
    
    @Test
    public void firstAndLastSmall()
    {
    SameFirstLast test = new SameFirstLast();
    int[] array = {1, 2, 1};
    assertTrue(test.SameFirstLast(array));
    }
}
