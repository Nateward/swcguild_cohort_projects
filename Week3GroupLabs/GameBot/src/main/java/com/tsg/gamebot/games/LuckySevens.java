/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.gamebot.games;

import java.util.Random;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class LuckySevens implements Game{
    
    private final String gameName = "LuckySevens";
    
    @Override
    public String getGameName(){
        return this.gameName;
    }
    @Override
    public void playGame() {
        Scanner keyboard = new Scanner(System.in);
        Random generator = new Random();

        int die1, die2, // two dice
                sum, // sum of die1 and die2
                startingBet, // starting bet (user input)
                rolls, // counts dice rolls
                maxMoney, // most money won
                rollsAtMax, // number of rolls when user had the most money
                pot;              // user's winnings and losses

        System.out.print("How many dollars do you have? "); //
        startingBet = keyboard.nextInt(); //

        pot = startingBet; //
        maxMoney = pot; //
        rollsAtMax = 0; //
        rolls = 0; //

        while (pot > 0) { //
            rolls++; //

            die1 = generator.nextInt(6) + 1; //
            die2 = generator.nextInt(6) + 1; //
            sum = die1 + die2; //

            if (sum == 7) { //
                pot += 4; //
            } else {
                pot -= 1; //
            }

            if (pot > maxMoney) { //
                maxMoney = pot; //
                rollsAtMax = rolls; //
            }
        }

        System.out.println("You are broke after " + rolls + " rolls.");
        System.out.println("You should have quit after " + rollsAtMax + " rolls when you were up $" + maxMoney + ".");

    }
}
