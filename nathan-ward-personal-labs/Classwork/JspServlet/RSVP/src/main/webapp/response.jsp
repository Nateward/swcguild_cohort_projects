<%-- 
    Document   : response
    Created on : 24-Mar-2016, 1:55:08 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Response</title>
    </head>
    <body>
        <h1>Response</h1>
        <a href="RSVPSerlet">Home</a><br/>
        Your answer was: ${param.myAnswer}<br/>
        ${message}<br/>
    </body>
</html>
