/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab3consolecalculator;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class MathJUnitTest {
    
    public MathJUnitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    //
    // @Test
    // public void hello() {}

@Test
public void threePlusFive(){
    
    MathLogic test = new MathLogic();
    assertEquals(8,test.add(3, 5),0);
}

@Test
public void fifteenMinusFive(){
    
    MathLogic test = new MathLogic();
    assertEquals(10,test.substract(15, 5),0);
}

@Test
public void threeTimesFive(){
    
    MathLogic test = new MathLogic();
    assertEquals(15,test.multiply(3, 5),0);
}

@Test
public void fifteenDividedByFive(){
    
    MathLogic test = new MathLogic();
    assertEquals(3,test.divide(15, 5),0);
}
}


