/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bolleanexpressions;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class DoWhileAndWhileLoops {
    public static void main(String[] args) {
        System.out.println("User may get it right with do while loops.");
        
        Scanner sc = new Scanner(System.in);
        
        int userValue = 0;
        
        do{
            System.out.println("Please enter value between 1 and 5 inclusive");
            
            String userInput = sc.nextLine();
            userValue = Integer.parseInt(userInput);
            
        }while(userValue < 1 || userValue > 5);
        
        System.out.println("Excelent, thank you for being withinn range!");
        
        System.out.println("User may get it right with do while loops.");
        
        int otherValue = 0;
        
        System.out.println("Please enter another value from 3 to 10: ");
        
        String otherInput = sc.nextLine();
        
        otherValue = Integer.parseInt(otherInput);
        
        while(otherValue < 3 || otherValue > 10)
        {
            System.out.println("Please pay attention to the range! Range is between"
                    + "3 - 10");
            otherInput = sc.nextLine();
            otherValue = Integer.parseInt(otherInput);
            
        }
    }
    
}
