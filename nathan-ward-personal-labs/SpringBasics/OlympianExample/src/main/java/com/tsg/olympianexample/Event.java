/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.olympianexample;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface Event {
    
    public String compete();
}
