/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd.dao;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public enum SearchTerm {
    TITLE, RELEASEDATE, MPAARATING, DIRECTORSNAME, STUDIO, USERNOTE
}
