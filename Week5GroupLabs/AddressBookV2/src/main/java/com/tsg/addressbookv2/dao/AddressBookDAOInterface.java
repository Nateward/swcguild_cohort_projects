/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbookv2.dao;

import com.tsg.addressbookv2.dto.Address;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *
 * @author apprentice
 */
public interface AddressBookDAOInterface {
    
    public void addAddress(Address address);
    
    public Address removeAddress(Integer index);
    
    public ArrayList<Address> findAddressByLastName(String lastName);
    
    public ArrayList<Address> findAddressByCity(String city);
    
    public Map<String, List<Address>> findAddressByState(String state);
    
    public ArrayList<Address> findAddressByZip(Integer zip);
    
    public ArrayList<Address> findAllAddresses();
    
    public Integer addressBookSize();
    
}
