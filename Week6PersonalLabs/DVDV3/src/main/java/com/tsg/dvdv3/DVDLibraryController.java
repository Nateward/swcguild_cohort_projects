/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdv3;

import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import com.swcguild.dvdlibrary.dto.Dvd;
import com.tsg.dvdv3.ui.ConsoleIO;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author apprentice
 */
public class DVDLibraryController {

    ConsoleIO fido = new ConsoleIO();
    DvdLibraryDao dvdList;

    public DVDLibraryController(DvdLibraryDao dao) {
        this.dvdList = dao;
    }
    
    

    void run() throws InterruptedException {
        try {
//            dvdList.loadList();
            boolean runMenu = true;
            while (runMenu) {
                printMenu();
                Integer userInput = fido.readInt("Please enter a number for the menu above:", 1, 7);
                switch (userInput) {
                    case 1:
                        addDVD();
                        fido.print("Adding DVD to Collection.......");
                        Thread.sleep(600);
                        fido.print("DVD added to collection.......");
                        break;
                    case 2:
                        removeDVD();
                        fido.print("Deleting DVD f Collection.......");
                        Thread.sleep(600);
                        fido.print("DVD removed from collection");
                        break;
                    case 3:
                        fido.print("Listing All DVD from collection.....");
                        Thread.sleep(600);
                        showAllDVD();
                        break;
                    case 4:
                        fido.print("Displaying infromation of given DVD....");
                        Thread.sleep(600);
                        searchDVDByTitle();

                        break;
                    case 5:
                        editDVD();
                        Thread.sleep(600);
                        fido.print("Editing DVD by title");

                        break;
                    case 6:
                        fido.print("Quitting......");
                        Thread.sleep(600);
                        runMenu = false;
                        break;
                }
            }
//            dvdList.writeLibrary();
//        } catch (IOException e) {
//            System.out.println("File not found ... ");
        } catch (InterruptedException e) {
            System.out.println("Interrupted exception");
        }
    }

    private void printMenu() {

        fido.print("1) Add a DVD to collection");
        fido.print("2) Remove a DVD from collection");
        fido.print("3) List all the DVDs from the collection");
        fido.print("4) Search a DVD by title");
        fido.print("5) Edit a DVD by title");
        fido.print("6) Exit");
    }

    private void addDVD() {
        fido.print("Enter information of your DVD you want to add ");
        String title = fido.readString("Title:");
        String releaseDate = fido.readString("Release Date:");
        String ratings = fido.readString("MPAA Ratings:");
        String directorName = fido.readString("Director:");
        String Studio = fido.readString("Studio:");
        String userNote = fido.readString("Notes:");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate date = LocalDate.parse(releaseDate, formatter);

        Dvd dvd = new Dvd();
        dvd.setTitle(title);
        dvd.setReleaseDate(date);
        dvd.setMpaaRating(ratings);
        dvd.setDirector(directorName);
        dvd.setStudio(Studio);
        dvd.setNote(userNote);

        dvdList.add(dvd);
    }

    private void removeDVD() {
        String titleRemove = fido.readString("Please enter the title you want to delete.");
        List<Dvd> dvdReturnList = dvdList.getByTitle(titleRemove);

        Dvd dvd;
        if (dvdReturnList.isEmpty()) {
            fido.print("No DVD with title " + titleRemove + " exist.");

        } else {
            //iterating through the arraylist to get the DVD object
            if (dvdReturnList.size() > 1) {
                //if more than one title exist loop through and let user see all the options
                dVDInfo(dvdReturnList);

                Integer userSelect = fido.readInt("There are " + dvdReturnList.size() + " records of " + titleRemove
                        + " which one do you want to select.", 1, dvdReturnList.size());
                userSelect = userSelect - 1; //index is from 1 to no. of DVD inside the arraylist. So userSelect has to match the 0 index of arraylist
                //select the DVD object user wants to select
                dvd = dvdReturnList.get(userSelect);
            } else {
                dvd = dvdReturnList.get(0);
            }
            //As user if this is the title you want to delete
            String userConfirmation = fido.readString("Are you sure you want to delete " + dvd.getTitle() + " which was released in " + dvd.getReleaseDate());
            if (userConfirmation.equalsIgnoreCase("yes")) {
                Integer index = dvd.getId();
                dvdList.remove(index);
            } else {
                fido.print("Returning to main menu.......");
            }

        }

    }

    private void searchDVDByTitle() {

        String titleSearch = fido.readString("Please enter the title you want to Search.");
        List<Dvd> dvdReturnedList = dvdList.getByTitle(titleSearch);

        Dvd dvd;
        if (dvdReturnedList.isEmpty()) {
            fido.print("No DVD with title " + titleSearch + " exist.");

        } else {
            if (dvdReturnedList.size() == 1) {
                fido.print("We found 1 record with title " + titleSearch);
            } else {
                fido.print("We found " + dvdReturnedList.size() + " record with title " + titleSearch);
            }

            dVDInfo(dvdReturnedList);

        }
    }

    private void showAllDVD() {

        if (dvdList.listAll().isEmpty()) {
            fido.print("Your DVD library is empty!");
        } else {
            List<Dvd> dvdCollection = dvdList.listAll();
            ArrayList<Dvd> dvdArrayList = new ArrayList<>();
            for (Dvd dvd : dvdCollection) {
                dvdArrayList.add(dvd);
            }
            dVDInfo(dvdArrayList);

        }
    }

    private void editDVD() {
        String titleRemove = fido.readString("Please enter the title you want to edit.");
        List<Dvd> dvdReturnedList = dvdList.getByTitle(titleRemove);

        Dvd dvd;
        if (dvdReturnedList.isEmpty()) {
            fido.print("No DVD with title " + titleRemove + " exist.");
            //return;
        } else {
            //iterating through the list to get the DVD object
            if (dvdReturnedList.size() > 1) {
                //if more than one title exist loop through and let user see all the options

                dVDInfo(dvdReturnedList);
                Integer userSelect = fido.readInt("There are " + dvdReturnedList.size() + " records of " + titleRemove
                        + " which one do you want to select.", 1, dvdReturnedList.size());
                userSelect = userSelect - 1; //index is from 1 to no. of DVD inside the arraylist. So userSelect has to match the 0 index of arraylist
                //select the DVD object user wants to select
                dvd = dvdReturnedList.get(userSelect);
            } else {
                dvd = dvdReturnedList.get(0);
            }
            //As user if this is the title you want to edit
            String userConfirmation = fido.readString("Are you sure you want to Edit " + dvd.getTitle() + " which was released in " + dvd.getReleaseDate());
            if (userConfirmation.equalsIgnoreCase("yes")) {
                String title = fido.readString("Title:");
                String releaseDate = fido.readString("Release Date:");
                String ratings = fido.readString("MPAA Ratings:");
                String directorName = fido.readString("Director:");
                String Studio = fido.readString("Studio:");
                String userNote = fido.readString("Notes:");

                DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
                LocalDate date = LocalDate.parse(releaseDate, formatter);

                dvd.setTitle(title);
                dvd.setReleaseDate(date);
                dvd.setMpaaRating(ratings);
                dvd.setDirector(directorName);
                dvd.setStudio(Studio);
                dvd.setNote(userNote);
                dvdList.add(dvd);
            } else {
                fido.print("Returning to main menu.......");
            }

        }
    }

    private void dVDInfo(List<Dvd> dvdReturnedList) {

        for (int i = 0; i < dvdReturnedList.size(); i++) {

            fido.print("\n#" + (i + 1) + "");
            fido.print("Title       : " + dvdReturnedList.get(i).getTitle());
            fido.print("Release Date: " + dvdReturnedList.get(i).getReleaseDate());
            fido.print("MPAA Ratings: " + dvdReturnedList.get(i).getMpaaRating());
            fido.print("Studio      : " + dvdReturnedList.get(i).getStudio());
            fido.print("Director    : " + dvdReturnedList.get(i).getDirector());
            fido.print("Note        : " + dvdReturnedList.get(i).getNote() + "\n");
        }
    }

}
