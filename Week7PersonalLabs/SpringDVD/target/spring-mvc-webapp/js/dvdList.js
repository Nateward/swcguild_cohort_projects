/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function (event) {
    loadDvd();
    $('#add-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/SpringDVD/dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                mpaaRating: $('#add-mpaaRating').val(),
                directorsName: $('#add-director').val(),
                studio: $('#add-studio').val(),
                userNote: $('#add-user-notes').val(),
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-mpaaRating').val('');
            $('#add-director').val('');
            $('#add-studio').val('');
            $('#add-user-notes').val('');
            loadDvd();
        });
    });
    $('#edit-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-id').val(),
            data: JSON.stringify({
                title: $('#edit-title').val(),
                releaseDate: $('#edit-releaseDate').val(),
                mpaaRating: $('#edit-rating').val(),
                directorsName: $('#edit-director').val(),
                studio: $('#edit-studio').val(),
                userNote: $('textarea#edit-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadDvd();
        });
    });
});


function loadDvd() {
    clearDvdTable();
    var dTable = $("#contentRows");
    $.ajax({
        type: 'GET',
        url: "Dvds"
    }).success(function (data, status) {
        $.each(data, function (index, dvd) {
            dTable.append($('<tr>')
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-dvd-id': dvd.id,
                                'data-toggle': 'modal',
                                'data-target': '#detailsModal'
                            })
                            .text(dvd.title)))
                    .append($('<td>').text(dvd.directorsName))
                    .append($('<td>').append($('<a>')
                            .attr({
                                'data-dvd-id': dvd.id,
                                'data-toggle': 'modal',
                                'data-target': '#editModal'
                            })
                            .text("Edit")))
                    .append($('<td>')
                            .append($('<a>').attr({
                                'onclick': 'deleteDvd(' + dvd.id + ')'}).text('Delete')))
                    );
        });

    });
}

function deleteDvd(id) {
    var answer = confirm("Do you really want to delete this Dvd?");

    if (answer === true) {

        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDvd();
        })

    }
}

function clearDvdTable() {
    $("#contentRows").empty();
}

$('#detailsModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#dvd-id').text(dvd.idNumber);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-releaseDate').text(dvd.releaseDate);
        modal.find('#dvd-rating').text(dvd.mpaaRating);
        modal.find('#dvd-director').text(dvd.directorsName);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-notes').text(dvd.userNote);
    });
});

$('#editModal').on('show.bs.modal', function (event) {
    var element = $(event.relatedTarget);

    console.log("I got here");

    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        modal.find('#edit-id').val(dvd.id);
        modal.find('#edit-title').val(dvd.title);
        modal.find('#edit-releaseDate').val(dvd.releaseDate);
        modal.find('#edit-rating').val(dvd.mpaaRating);
        modal.find('#edit-director').val(dvd.directorsName);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-user-notes').val(dvd.userNote);
    });

});