/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.dvdlibraryv2.dao.DVDLibraryImpl;
import com.tsg.dvdlibraryv2.dto.DVD;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class DVDLibraryIMPLTest {

    public DVDLibraryIMPLTest() {
    }
    DVDLibraryImpl dao;

    DVD dvd1;
    DVD dvd2;
    DVD dvd3;
    DVD dvd4;

    @Before
    public void setUp() {
        dao = new DVDLibraryImpl();
        dvd1 = new DVD();
        dvd1.setTitle("Harry Porter");
        dvd1.setReleaseDate("2001");
        dvd1.setMpaaRating("8");
        dvd1.setDirectorsName("Chris Cholumbus");
        dvd1.setStudio("Sony");
        

        dvd2 = new DVD();
        dvd2.setTitle("Zootopia");
        dvd2.setReleaseDate("2016");
        dvd2.setMpaaRating("10");
        dvd2.setDirectorsName("Byron Howard");
        dvd2.setStudio("Sony");
        dvd2.setUserNote("Need to watch, looks awesome");

        dvd3 = new DVD();
        dvd3.setTitle("Harry Porter");
        dvd3.setReleaseDate("2002");
        dvd3.setMpaaRating("8");
        dvd3.setDirectorsName("Chris Cholumbus");
        dvd3.setStudio("Sony");
        dvd3.setUserNote("two of 8");

        dvd4 = new DVD();
        dvd4.setTitle("Harry Porter");
        dvd4.setReleaseDate("2003");
        dvd4.setMpaaRating("8.5");
        dvd4.setDirectorsName("Chris Cholumbus");
        dvd4.setStudio("Sony");
        dvd4.setUserNote("three of 8");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addAndRemoveDVDTest() {
        //Arrange

        //Act
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);

        DVD removedDVD = dao.removeDVD(dvd1.getIndex());

        //assert
        Assert.assertEquals(dvd1, removedDVD);
    }

    @Test
    public void getDVDByTitleTest() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        //Act
        Collection<DVD> dvdList = dao.getDVDByTitle(dvd1.getTitle());
        for (DVD dvd : dvdList) {
            Assert.assertEquals(dvd1.getTitle(), dvd.getTitle());
        }
        //Assert
        Assert.assertEquals(2, dvdList.size());
        Assert.assertTrue(dvdList.contains(dvd1));
        Assert.assertTrue(dvdList.contains(dvd3));
       Assert.assertFalse(dvdList.contains(dvd2));
    }

    @Test
    public void getDVDByMPAATest() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        //Act
        Collection<DVD> dvdList = dao.getDVDByMPAA(dvd1.getMpaaRating());
        for (DVD dvd : dvdList) {
            Assert.assertEquals(dvd1.getMpaaRating(), dvd.getMpaaRating());
        }
        //Assert
        Assert.assertEquals(2, dvdList.size());
    }

    @Test
    public void getDVDByDirectorSortedByMPAARatingsTest() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        dao.addDVD(dvd4);

        //Act
        Map<String, List<DVD>> dvdMap = dao.getDVDByDirector(dvd1.getDirectorsName());
        List<DVD> listWithMPAARatings8 = dvdMap.get(dvd1.getMpaaRating());

        Assert.assertEquals(2, dvdMap.size());
        Assert.assertTrue(listWithMPAARatings8.contains(dvd1));
        Assert.assertFalse(listWithMPAARatings8.contains(dvd4));
        Assert.assertFalse(dvdMap.keySet().contains(dvd2));

    }

    @Test
    public void getNewestDVD() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        dao.addDVD(dvd4);
        //Act
        Collection<DVD> newestDVDCollection = dao.getNewestDVD();
        //Assert
        Assert.assertEquals(1, newestDVDCollection.size());
        Assert.assertFalse(newestDVDCollection.contains(dvd1));
        Assert.assertTrue(newestDVDCollection.contains(dvd2));
        
    }
    @Test
    public void getOldestDVD() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        dao.addDVD(dvd4);
        //Act
        Collection<DVD> oldestDVDCollection = dao.getOldestDVD();
        //Assert
        Assert.assertEquals(1, oldestDVDCollection.size());
        Assert.assertFalse(oldestDVDCollection.contains(dvd2));
        Assert.assertTrue(oldestDVDCollection.contains(dvd1));
        
    }
    
    @Test  //Testing for the average number of notes
    public void getAverageNumOfNotes() {
        //Arrange
        dao.addDVD(dvd1);
        dao.addDVD(dvd2);
        dao.addDVD(dvd3);
        dao.addDVD(dvd4);
        //Act
       double average = dao.getAverageNoOfNotesAssociate();
       double testAverage = 3.0/4.0;
        //Assert
        Assert.assertEquals(testAverage, average, 0);
        
    }
    
}
