/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Circle {
    private int length;
    private float circumference;
     private float area;
    private int diameter;  

    public Circle(int length, float circumference, float area, int diameter) {
        this.length = length;
        this.circumference = circumference;
        this.area = area;
        this.diameter = diameter;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public float getCircumference() {
        return circumference;
    }

    public void setCircumference(float circumference) {
        this.circumference = circumference;
    }

    public float getArea() {
        return area;
    }

    public void setArea(float area) {
        this.area = area;
    }

    public int getDiameter() {
        return diameter;
    }

    public void setDiameter(int diameter) {
        this.diameter = diameter;
    }
   
    //Behaviors
    
    /**
     * these are behaviors of a circle
     */
    
    public void similarity()
    {
        System.out.println("All circumference and radius of circle are proportional");
    }
}
