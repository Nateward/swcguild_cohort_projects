/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.mapexamples;

import java.util.Collection;
import java.util.HashMap;
import java.util.Set;
import javafx.print.Collation;

/**
 *
 * @author apprentice
 */
public class MapExamples {

    public static void main(String[] args) {
        HashMap<String, Integer> population = new HashMap<>();

//        population.put("USA", 313000);
        population.put("USA", 200000000);

        population.put("Canada", 340000);

        population.put("UK", 630000);

        population.put("Japan", 127000000);

        population.put("USA", 313000);

        System.out.println("Map size is: " + population.size());
        
        Integer japanPopulation = population.get("Japan");
        
        System.out.println("The population of japan is: " + japanPopulation);
        
        
        System.out.println("Yet again the population of japan is: " + population.get("Japan"));
        
        System.out.println("==========Printing keys==========");
        
        Set<String> keys = population.keySet();
        
        
        
        for(String k : keys){
            System.out.println(k);
        }
        
        System.out.println("------------Printing all the values-----------");
        
        for(String k: keys){
            System.out.println("The population of " + k + " is " + population.get(k));
        }
        
        System.out.println("+++++++++Printing out pure population list without keys+++++++++=");
        
        Collection<Integer> popValues = population.values();
        
            for (Integer p: popValues) {
                System.out.println(p);
            }

    }

}
