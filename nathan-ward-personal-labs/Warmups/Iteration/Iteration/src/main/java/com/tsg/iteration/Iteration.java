/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.iteration;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 *
 * @author apprentice
 */
public class Iteration {
    public static void main(String[] args) {
        ArrayList<String> teams = new ArrayList<>();
        
        teams.add("Vikings");
        teams.add("Packers");
        teams.add("Lions");
        teams.add("Bears");
        teams.add("Browns");
        teams.add("Bengals");
        teams.add("Steelers");
        teams.add("Ravens");
        
        for (String team : teams) {
            System.out.println("Team: " + team);
        }
        System.out.println("");
        for (int i = 0; i < teams.size(); i++) {
            System.out.println("Team: " + teams.get(i));
            
        }
        System.out.println("");
        ListIterator iterateTeams = teams.listIterator();
        while (iterateTeams.hasNext()) {            
            System.out.println("Team: " + iterateTeams.next());
        }
    }
    
}
