/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.iteration2;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class Iteration2 {

    public static void main(String[] args) {
        Map<String, String> nflCities = new HashMap<String, String>();

        nflCities.put("Cleveland", "Browns");
        nflCities.put("Pittsburg", "Steelers");
        nflCities.put("Cincinnati", "Bengals");
        nflCities.put("Minneapolis", "Vikings");
        nflCities.put("Kansas City", "Chiefs");

        Set keys = nflCities.keySet();

        for (Object key : keys) {
            System.out.println(key + ": " + nflCities.get(key));
        }

        System.out.println("");

        Iterator<String> iter = keys.iterator();
        
        while (iter.hasNext()) {
            String k = iter.next();
            System.out.println(k + ": " + nflCities.get(k));
        }
        
        System.out.println("");
        

        for (Map.Entry<String, String> entry : nflCities.entrySet()) {
            System.out.println(entry.getKey() + ": " + entry.getValue());
        }
        
        
    }
}
