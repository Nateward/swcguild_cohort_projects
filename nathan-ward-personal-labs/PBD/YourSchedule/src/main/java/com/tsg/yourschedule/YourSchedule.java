/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.yourschedule;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class YourSchedule {
    public static void main(String[] args) {
        String class1 = "English III | ";
        String class2 = "Precalculus | ";
        String class3 = "Music Theory | ";
        String class4 = "Biotechnology | ";
        String class5 = "Principles of Technology I | ";
        String class6 = "Latin II | ";
        String class7 = "AP US History | ";
        String class8 = "Business Computer Information Systems | ";
        
        String teacher1 = "      Ms. Lapan";
        String teacher2 = "    Mrs. Gideon";
        String teacher3 = "      Mr. Davis";
        String teacher4 = "     Ms. Palmer";
        String teacher5 = "     Ms. Garcia";
        String teacher6 = "   Mrs. Barnett";
        String teacher7 = "Ms. Johannessen";
        String teacher8 = "      Mr. James";
        
        System.out.println("+-----------------------"
                + "------------------------------------+"); 
        System.out.println("|1|                           " + class1 + teacher1 + " |");
        System.out.println("|2|                           " + class2 + teacher2 + " |");
        System.out.println("|3|                          " + class3 + teacher3 + " |");
        System.out.println("|4|                         " + class4 + teacher4 + " |");
        System.out.println("|5|            " + class5 + teacher5 + " |");
        System.out.println("|6|                              " + class6 + teacher6 + " |");
        System.out.println("|7|                         " + class7 + teacher7 + " |");
        System.out.println("|8| " + class8 + teacher8 + " |");

        System.out.println("+-----------------------"
                + "------------------------------------+");       
    }

}
