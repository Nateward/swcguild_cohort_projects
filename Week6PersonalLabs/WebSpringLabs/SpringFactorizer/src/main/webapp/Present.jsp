<%-- 
    Document   : present
    Created on : 25-Mar-2016, 8:06:15 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css"/>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Factorizer Numbers</title>
    </head>
    <body>
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-4"></div>
                <div class="col-lg-4 well">
                    <h1>Here Are the factors of ${quantity}!</h1>
                    <p>

                        <c:forEach var="list" items="${theFactorz}">
                            ${list}<br/>
                        </c:forEach>
                        <c:if test="${isItPrime}">
                            ${quantity} is a prime number.<br/>
                        </c:if>
                        <c:if test="${!isItPrime}">
                            ${quantity} is not a prime number.<br/>
                        </c:if>

                        <c:if test="${isItPerfect}">
                            ${quantity} is a perfect number.<br/>
                        </c:if>
                        <c:if test="${!isItPerfect}">
                            ${quantity} is not a perfect number.<br/>
                        </c:if>
                    </p>
                </div>
                <div class="col-lg-4"></div>
            </div>
            <div class="row">

                <div class="col-lg-5"></div>
                <div class="col-lg-7">
                    <a class="btn btn-success" href="SpringFactorizerServlet">Play Again</a>
                </div>
            </div>
        </div>        

    </body>
</html>
