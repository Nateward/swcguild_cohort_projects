<%-- 
    Document   : NewContactFormNoAjax
    Created on : 29-Mar-2016, 10:24:07 AM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/phone-book.png" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
        </div>
        <div class="container">
            <h1>New Address Form</h1>
            <a href="displayAddressBook">View Address Book</a>
            <hr/>
            <sf:form class="form-horizontal" modelAttribute="address" role="form" action="addNewAddress" method="POST">
                <div class="form-group">
                    <label for="add-first-name" class="col-md-4 control-label">First Name:</label>
                    <div class="col-md-8">
                        <sf:input type="text" class="form-control text-right" id="add-first-name" path="firstName" name="firstName" placeholder="First Name"/>
                        <sf:errors path="firstName" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-last-name" class="col-md-4 control-label">Last Name:</label>
                        <div class="col-md-8">
                        <sf:input type="text" class="form-control text-right" id="add-last-name" path="lastName" name="lastName" placeholder="Last Name"/>
                        <sf:errors path="lastName" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-street" class="col-md-4 control-label">Street Address:</label>
                        <div class="col-md-8">
                        <sf:input path="streetAddress" type="text" class="form-control text-right" id="add-street" name="street" placeholder="Street Address"/>
                        <sf:errors path="streetAddress" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-city" class="col-md-4 control-label">City:</label>
                        <div class="col-md-8">
                        <sf:input type="text" path="city" class="form-control text-right" id="add-city" name="city" placeholder="City"/>
                        <sf:errors path="city" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-state" class="col-md-4 control-label">State:</label>
                        <div class="col-md-8">
                        <sf:input type="text" path="state" class="form-control text-right" id="add-state" name="state" placeholder="State"/>
                        <sf:errors path="state" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="add-zip" class="col-md-4 control-label">Zip:</label>
                        <div class="col-md-8">
                        <sf:input type="text" path="zip" class="form-control text-right" id="add-zip" name="zip" placeholder="Zip Code"/>
                        <sf:errors path="zip" cssClass="text-danger"></sf:errors>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="col-md-offset-4 col-md-8">
                            <button type="submit" id="add-button" class="btn btn-info">Add New Address</button>
                        </div>
                    </div>
            </sf:form>
        </div>
    </body>
</html>
