/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Triangle {
    private String typeOfTriangle; //ie, according to the sides, angles
    private String name;
    private int height;
    private int base;
    private float perimeter;

    public Triangle(String typeOfTriangle, String name, int height, int base, float perimeter) {
        this.typeOfTriangle = typeOfTriangle;
        this.name = name;
        this.height = height;
        this.base = base;
        this.perimeter = perimeter;
    }

    public String getTypeOfTriangle() {
        return typeOfTriangle;
    }

    public void setTypeOfTriangle(String typeOfTriangle) {
        this.typeOfTriangle = typeOfTriangle;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getBase() {
        return base;
    }

    public void setBase(int base) {
        this.base = base;
    }

    public float getPerimeter() {
        return perimeter;
    }

    public void setPerimeter(float perimeter) {
        this.perimeter = perimeter;
    }
    
    //Behaviors
    
    /**
     * These are behaviors of a triangle.
     */
    
    public void angles()
    {
        System.out.println("The trianle has three angles");
    }
}
