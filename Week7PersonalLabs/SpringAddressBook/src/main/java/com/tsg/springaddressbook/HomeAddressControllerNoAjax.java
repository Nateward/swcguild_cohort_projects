/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook;

import com.tsg.springaddressbook.dao.AddressBookDAO;
import com.tsg.springaddressbook.dto.Address;
import java.util.List;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@Controller
public class HomeAddressControllerNoAjax {

    private final AddressBookDAO dao;

    @Inject
    public HomeAddressControllerNoAjax(AddressBookDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/displayAddressBook", method = RequestMethod.GET)
    public String displayContactListNoAjax(Model model) {

        List<Address> aList = dao.GetAll();
        model.addAttribute("addressList", aList);
        return "displayAddressBook";
    }

    @RequestMapping(value = "displayNewAddressForm", method = RequestMethod.GET)
    public String displayNewAddressForm(Model model) {
        
        Address address = new Address();
        model.addAttribute("address", address);
        return "newAddressForm";
    }

    @RequestMapping(value = "/addNewAddress", method = RequestMethod.POST)
    public String addNewAddress(@Valid @ModelAttribute("address") Address address, BindingResult result) {
        
        
//        String firstName = req.getParameter("firstName");
//        String lastName = req.getParameter("lastName");
//        String streetAddress = req.getParameter("street");
//        String city = req.getParameter("city");
//        String state = req.getParameter("state");
//        Integer zip = Integer.parseInt(req.getParameter("zip"));
//
//        Address address = new Address();
//        address.setFirstName(firstName);
//        address.setLastName(lastName);
//        address.setStreetAddress(streetAddress);
//        address.setCity(city);
//        address.setState(state);
//        address.setZip(zip);

        if (result.hasErrors()) {
            return "newAddressForm";
        }

        dao.Create(address);

        return "redirect:displayAddressBook";

    }

    @RequestMapping(value = "/deleteAddress", method = RequestMethod.GET)
    public String deleteAddress(HttpServletRequest req) {
        int addressId = Integer.parseInt(req.getParameter("addressId"));

        dao.Delete(addressId);

        return "redirect:displayAddressBook";
    }
    
    @RequestMapping(value = "/diplayEditAddressFrom", method = RequestMethod.GET)
    public String displayEditContactFormNoAjax(HttpServletRequest req, Model model){
        
        int addressId = Integer.parseInt(req.getParameter("addressId"));
        
        Address address = dao.GetById(addressId);
        
        model.addAttribute("address", address);
        
        return "editAddressForm";
    }
    
    @RequestMapping(value = "/editAddress", method = RequestMethod.POST)
    public String editContactNoAjax(@Valid @ModelAttribute("address") Address address, BindingResult result){
        
        if (result.hasErrors()) {
            return "editAddressForm";
        }
        
        dao.Update(address.getIdNumber(), address);
        
        return "redirect:displayAddressBook";
    }
}
