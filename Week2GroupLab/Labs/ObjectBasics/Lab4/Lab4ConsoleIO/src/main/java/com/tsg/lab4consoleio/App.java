/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab4consoleio;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) throws Exception {
       String word;
       float float1;
        int int1;
        double double1;
        
//        MathLogic math = new MathLogic();
//       
//
//        math.calculatorLogic();
        
        ConsoleIO test = new ConsoleIO();
        int1 = test.method1("please enter a value: ");
        int1 = test.method2("please enter a value between 10 - 20: ", 10, 20);
        
        word = test.method3("Please enter anything: ");
        
        float1 = test.method4("please enter a value: ");
        float1 = test.method5("please enter a value between 10 - 20: ", 10.5f, 20.5f);
        
        double1 = test.method6("please enter a value: ");
        double1 = test.method7("please enter a value between 10 - 20: ", 10.5, 20.5);
    }
}
