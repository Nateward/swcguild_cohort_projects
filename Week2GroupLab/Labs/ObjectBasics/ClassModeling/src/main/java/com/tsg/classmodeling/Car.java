/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Car {
    private String make;
    private String model;
    private String color;
    private int year;
    private int length;
    private int width;

    public Car(String make, String model, String color, int year, int length, int width) {
        this.make = make;
        this.model = model;
        this.color = color;
        this.year = year;
        this.length = length;
        this.width = width;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }
    

    
    //Behaviors
    
    /**
     * These are the behaviors of 
     */
    
    public void size()
    {
        System.out.println("There are full, mideum and small size cars");
    }
    
    public void power()
    {
        System.out.println("Cars can be powered by Gas, Electricity and Solar power");
    }
}
