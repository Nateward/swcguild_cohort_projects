/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.interfaces;

import com.tsg.flooringmasteryspring.dto.Order;
import java.util.ArrayList;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface OrdersInterface {
    
    public void createOrder(Order order);
    
    public ArrayList<Order> getOrders();
    
    public Order getOrderByOrderNumber(Integer orderNumber);
    
    public void editOrder(Integer orderNumber, Order updatedOrder);
    
    public void deleteOrder(Integer orderId);
    
    public void load(String saveLocation);
    
    public void save(String saveLocation);
    
    public String getORDER_FILE();
    
    public void setORDER_FILE(String ORDER_FILE);
    
    public String getORDER_DELIMETER();
    
    public String getDate();
    
    public void setDate(String date);
    
}
