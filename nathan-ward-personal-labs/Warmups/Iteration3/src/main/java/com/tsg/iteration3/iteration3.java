/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.iteration3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

/**
 *
 * @author apprentice
 */
public class iteration3 {
    public static void main(String[] args) {
        HashMap<String, ArrayList<String>> cities = new HashMap<String, ArrayList<String>>();
        
        ArrayList<String> teams = new ArrayList<>();
        teams.add("Indians");
        teams.add("Browns");
        cities.put("Cleveland", teams);
        
        teams = new ArrayList<>();
        
        teams.add("Steelers");
        teams.add("Pirates");        
        cities.put("Pittsburg", teams);
         
        teams = new ArrayList<>();
        
        teams.add("Bengals");
        teams.add("Reds");        
        cities.put("Cincinnati", teams);
        
         
        teams = new ArrayList<>();
        
        teams.add("Vikings");
        teams.add("Twins");        
        cities.put("Minneapolis", teams);
        
         
        teams = new ArrayList<>();
        
        teams.add("Chiefs");
        teams.add("Royals");        
        cities.put("Kanasa City", teams);
        
        Set<String> keys = cities.keySet();
        
        for (String key : keys) {
            ArrayList<String> team = cities.get(key);
                        System.out.println(key + ":");

            
            for (String s : team) {
                System.out.println(s);
            }
        }
        
    }
}
