/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.javabasicskillcheck;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WhatIsYourNumber {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        
        int yourNum = 0;
        
        System.out.println("Please pick a number: ");
        yourNum = keyboard.nextInt();
        
        System.out.println(yourNum + "\n");
        
        for (int i = 1; i < (yourNum + 1); i++) {
            System.out.println(i);
        }
        System.out.println("\n");
        
        System.out.println("Thank you for playing");
    }
}
