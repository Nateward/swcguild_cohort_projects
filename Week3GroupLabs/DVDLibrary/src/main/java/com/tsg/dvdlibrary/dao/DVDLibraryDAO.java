/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dvdlibrary.dao;

import com.tsg.dvdlibrary.dto.DVD;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class DVDLibraryDAO {

    private HashMap<Integer, DVD> dvdLibrary = new HashMap<>();

    private Integer idIndex;
    private final static String LIBRARY_FILE = "dvdLibrary.txt";
    private final static String DELIMITER = "::";

    public void addDvd(DVD dvd) {
        dvd.setIndexNumber(this.idIndex);
        dvdLibrary.put(this.idIndex, dvd);
        this.idIndex++;

    }

    public void deleteDvdById(Integer dvdId) {
        dvdLibrary.remove(dvdId);
    }

    public ArrayList<DVD> getDvdByName(String dvdName) {
        Collection<DVD> dvds = dvdLibrary.values();

        ArrayList<DVD> dvdResultList = new ArrayList<>();
        for (DVD dvd : dvds) {
            String dvdResultName = dvd.getTitle().toLowerCase();
            if (dvdResultName.length() >= dvdName.length()) {
//                String subString = dvdResultName.substring(0, dvdName.length());
                if (dvdResultName.contains(dvdName.toLowerCase())) {
                    dvdResultList.add(dvd);
                }
            }
        }

        return dvdResultList;
    }

    public DVD getDvdById(Integer dvdId) {
        return dvdLibrary.get(dvdId);
    }

    public ArrayList<DVD> getAllDvds() {
        Collection<DVD> dvds = dvdLibrary.values();

        ArrayList<DVD> dvdResultList = new ArrayList<>();

        for (DVD dvd : dvds) {
            dvdResultList.add(dvd);
        }
        return dvdResultList;
    }

    public Integer dvdLibrarySize() {
        return this.dvdLibrary.size();
    }

    public void loadDvdLibrary() throws FileNotFoundException {
        this.idIndex = 1;
        Scanner sc = new Scanner(new BufferedReader(new FileReader(LIBRARY_FILE)));

        while (sc.hasNextLine()) {
            String currentLine = sc.nextLine();
            String[] tokens = currentLine.split(DELIMITER);

            DVD dvd = new DVD();

            Integer dvdCurrentIndex = Integer.parseInt(tokens[0]);
            dvd.setIndexNumber(dvdCurrentIndex);
            dvd.setTitle(tokens[1]);
            dvd.setDirector(tokens[2]);
            dvd.setReleaseDate(tokens[3]);
            dvd.setStudio(tokens[4]);
            dvd.setMpaaRating(tokens[5]);
            dvd.setReviewerRating(Integer.parseInt(tokens[6]));
            dvd.setNotes(tokens[7]);
            
            if (dvdCurrentIndex >= this.idIndex) {
                this.idIndex = dvdCurrentIndex + 1;
            }
            
            this.dvdLibrary.put(dvdCurrentIndex, dvd);
        }
    }

    public void writeDvdLibrary() throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(LIBRARY_FILE));

        Collection<DVD> dvds = dvdLibrary.values();
        
        for (DVD dvd : dvds) {
            out.println(dvd.getIndexNumber() + DELIMITER + dvd.getTitle() + 
                    DELIMITER + dvd.getDirector() + DELIMITER + dvd.getReleaseDate() + 
                    DELIMITER + dvd.getStudio() + DELIMITER + dvd.getMpaaRating() + 
                    DELIMITER + dvd.getReviewerRating() + DELIMITER + dvd.getNotes());
                        
        }
        out.flush();
        out.close();

    }

}
