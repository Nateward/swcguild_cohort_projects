/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.interfaces;

import java.util.List;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface TaxesInterface {

    public Double getTaxRate(String state);

    public void loadTaxRateList();

    public List<String> getStateList();
}
