/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Student {
    private String firstName;
    private String lastName;
    private String major;
    private float gpa;
    private int age;

    public Student(String firstName, String lastName, String major, float gpa, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.major = major;
        this.gpa = gpa;
        this.age = age;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getMajor() {
        return major;
    }

    public void setMajor(String major) {
        this.major = major;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }
    
    
    
    
    
    public void happy()
    {
        System.out.println(firstName + " " + lastName + " jumps up and down because he got an A+!!");
    }
    
    
    
    public void pissed()
    {
        System.out.println(firstName + " " + lastName + " Throws his pencil because he got an F-!!");
    }
    
    
    
    public void confused()
    {
        System.out.println(firstName + " " + lastName + " scratches head at Singleton and JUnit testing.");
    }
    
    
}
