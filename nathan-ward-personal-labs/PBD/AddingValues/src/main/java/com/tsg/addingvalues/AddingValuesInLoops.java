/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addingvalues;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AddingValuesInLoops {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int number = 1;
        int totalnum = 0;
        System.out.println("I will add up the numbers you give me.");

        while (number != 0) {
            System.out.print("Number: ");
            number = sc.nextInt();
            totalnum = totalnum + number;
            if (number != 0) {
                System.out.println("The total so far is: " + (totalnum));

            }
        }
        System.out.println("\nThe total is " + totalnum);
    }
}
