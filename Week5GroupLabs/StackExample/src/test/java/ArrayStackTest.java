/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.stackexample.ArrayStack;
import java.util.Iterator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class ArrayStackTest {

    public ArrayStackTest() {
    }
    ArrayStack stack;

    @Before
    public void setUp() {
        stack = new ArrayStack();
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void pushPopNormalTest() {
        //Arrange

        String a = "a";
        String b = "b";
        String c = "c";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);

        Object popedString1 = stack.pop();//1st pop c is taken out of stack
        Object popedString2 = stack.pop();//2nd pop, b is taken out of stack
        assertEquals(c, popedString1);
        assertEquals(b.toString(), popedString2.toString());
    }

    @Test
    public void pushItemEqualsToArrayLenght() {
        //Arrange

        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);
        stack.push(d);
        stack.push(e);
        int size1 = stack.size();
        stack.pop();
        int size2 = stack.size();

        //Assert
        assertEquals(5, size1);
        assertEquals(4, size2);
    }

    @Test
    public void popTestForNull() {
        //Arrange

        //Act
        Object returnPopObject = stack.pop();
        assertTrue(returnPopObject == null);
    }

    @Test
    public void isEmptyTest() {
        //Arrange

        String a = "a";
        String b = "b";
        String c = "c";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);

        stack.pop();
        stack.pop();
        stack.pop();

        //Assert
        assertTrue(stack.isEmpty());
        stack.push(c);
        assertFalse(stack.isEmpty());
    }

    @Test
    public void iteratorTest() {
        //Arrange
        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        //Act
        stack.push(a);
        stack.push(b);
        stack.push(c);
        stack.push(d);
        stack.push(e);
        Iterator itr = stack.iterator();
        Object firstOut = itr.next();
        Object secondOut = itr.next();
        Object thirdOut = itr.next();
        Object fourthOut = itr.next();
        boolean hasNext1 = itr.hasNext();
        Object lastOut = itr.next();
        boolean hasNext2 = itr.hasNext();

        //assert
        assertEquals(e, firstOut);
        assertTrue(hasNext1);
        assertEquals(a, lastOut);
        assertFalse(hasNext2);

    }
}
