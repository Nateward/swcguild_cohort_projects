/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.shapes;

/**
 *
 * @author apprentice
 */
public class Circle extends Shape {

    @Override// pi * r^2
    public void area() { 
        Double x = Math.PI * 2 * 2;
        System.out.println(x);
    }

    @Override // 2 * pi * r
    public void perimeter() {
        Double x = 2 * Math.PI * 2;
        System.out.println(x);
    }
}
