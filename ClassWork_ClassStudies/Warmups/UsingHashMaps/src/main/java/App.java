
import java.util.HashMap;
import java.util.Set;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author ilyagotfryd
 */
public class App {
    public static void main(String[] args) {
        HashMap<String,Integer> scores = new HashMap<>();
        
        scores.put("Smith", 23);
        scores.put("Jones", 12);
        scores.put("Jordan", 45);
        scores.put("Pippen", 32);
        scores.put("Kerr", 15);
        
        Set<String> keys = scores.keySet();
        Double total = 0.0;
        for(String k : keys)
        {
            System.out.println(k + ": " + scores.get(k));
            System.out.println(" ");
            total = scores.get(k) + total;
        }
        
        Double average = total/scores.size();
        
        System.out.println("Total:" + total);
        System.out.println("Average: " + average);
    }
    
}
