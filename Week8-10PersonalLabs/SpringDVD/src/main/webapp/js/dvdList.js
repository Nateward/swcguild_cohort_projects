/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$(document).ready(function (event) {
    loadDvd();
    $('#add-button').click(function (event) {
        $('#validationErrors').text('');
        $('#validationEditErrors').text('');
        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/SpringDVD/dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-release-date').val(),
                mpaaRating: $('#add-mpaaRating').val(),
                directorsName: $('#add-director').val(),
                studio: $('#add-studio').val(),
                userNote: $('#add-user-notes').val(),
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-title').val('');
            $('#add-release-date').val('');
            $('#add-mpaaRating').val('');
            $('#add-director').val('');
            $('#add-studio').val('');
            $('#add-user-notes').val('');
            loadDvd();
        }).error(function (data, status) {

            $.each(data.responseJSON.fieldErrors, function (index, validationError) {
                var errorDiv = $('#validationErrors');
                errorDiv.append(validationError.message).append($('<br>'));
            });
        });
    });
    $('#edit-button').click(function (event) {
        $('#validationErrors').text('');
        $('#validationEditErrors').text('');
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-id').val(),
            data: JSON.stringify({
                title: $('#edit-title').val(),
                releaseDate: $('#edit-releaseDate').val(),
                mpaaRating: $('#edit-rating').val(),
                directorsName: $('#edit-director').val(),
                studio: $('#edit-studio').val(),
                userNote: $('textarea#edit-user-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            loadDvd();
        }).error(function (data, status) {

            $.each(data.responseJSON.fieldErrors, function (index, validationEditError) {
                var errorDiv = $('#validationEditErrors');
                errorDiv.append(validationEditError.message).append($('<br>'));
            });
        });
    });
});

function fillDvdTable(dvdList, status) {
    clearDvdTable();
    var dTable = $('#contentRows');

    $.each(dvdList, function (index, dvd) {
        dTable.append($('<tr>')
                .append($('<td>').append($('<a>')
                        .attr({
                            'data-dvd-id': dvd.id,
                            'data-toggle': 'modal',
                            'data-target': '#detailsModal'
                        })
                        .text(dvd.title)))
                .append($('<td>').text(dvd.directorsName))
                .append($('<td>').append($('<a>')
                        .attr({
                            'data-dvd-id': dvd.id,
                            'data-toggle': 'modal',
                            'data-target': '#editModal'
                        })
                        .text("Edit")))
                .append($('<td>')
                        .append($('<a>').attr({
                            'onclick': 'deleteDvd(' + dvd.id + ')'})
                                .text('Delete')))
                );
    });

}


function loadDvd() {

    $.ajax({
        url: "Dvds"
    }).success(function (data, status) {
        fillDvdTable(data, status);
    });
}

function deleteDvd(id) {
    $('#validationErrors').text('');
    $('#validationEditErrors').text('');
    var answer = confirm("Do you really want to delete this Dvd?");

    if (answer === true) {

        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDvd();
        })

    }
}

function clearDvdTable() {
    $("#contentRows").empty();
}

$('#detailsModal').on('show.bs.modal', function (event) {
    $('#validationErrors').text('');
    $('#validationEditErrors').text('');
    var element = $(event.relatedTarget);

    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        $('#edit-dvd-id').val(dvd.id);
        modal.find('#dvd-id').text(dvd.idNumber);
        modal.find('#dvd-title').text(dvd.title);
        modal.find('#dvd-releaseDate').text(dvd.releaseDate);
        modal.find('#dvd-rating').text(dvd.mpaaRating);
        modal.find('#dvd-director').text(dvd.directorsName);
        modal.find('#dvd-studio').text(dvd.studio);
        modal.find('#dvd-notes').text(dvd.userNote);
    });
});

$('#editModal').on('show.bs.modal', function (event) {
    $('#validationErrors').text('');
    $('#validationEditErrors').text('');
    var element = $(event.relatedTarget);

    console.log("I got here");

    var dvdId = element.data('dvd-id');

    var modal = $(this);

    $.ajax({
        url: 'dvd/' + dvdId
    }).success(function (dvd) {
        $('#edit-dvd-id').val(dvd.id);
        modal.find('#edit-id').val(dvd.id);
        modal.find('#edit-title').val(dvd.title);
        modal.find('#edit-releaseDate').val(dvd.releaseDate);
        modal.find('#edit-rating').val(dvd.mpaaRating);
        modal.find('#edit-director').val(dvd.directorsName);
        modal.find('#edit-studio').val(dvd.studio);
        modal.find('#edit-user-notes').val(dvd.userNote);
    });

});

$('#search-button').click(function (event) {
    // we don’t want the button to actually submit
    // we'll handle data submission via ajax
    event.preventDefault();
    $.ajax({
        type: 'POST',
        url: 'search/Dvds',
        data: JSON.stringify({
            title: $('#search-title').val(),
            releaseDate: $('#search-release-date').val(),
            mpaaRating: $('#search-mpaaRating').val(),
            directorsName: $('#search-director').val(),
            studio: $('#search-studio').val(),
            userNote: $('#search-user-notes').val()
        }),
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json'
        },
        'dataType': 'json'
    }).success(function (data, status) {
//        $('#search-title').val('');
//        $('#search-releaseDate').val('');
//        $('#search-rating').val('');
//        $('#search-phone').val('');
//        $('#search-email').val('');
        fillDvdTable(data, status);
    });
});