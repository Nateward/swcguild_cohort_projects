/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Airplane {
    private int heigth;
    private int width;
    private int length;
    private int wingspan;
    private int numOfPassengers;
    private boolean enginesPoweredOn;
    private boolean parachuteOnBoard;
    private String type;
    private String pilotName;
    
    
    public Airplane(int height, int width, int length){
        this.heigth = height;
        this.width = width;
        this.length = length;
    }

    /**
     * @return the heigth
     */
    public int getHeigth() {
        return heigth;
    }

    /**
     * @param heigth the heigth to set
     */
    public void setHeigth(int heigth) {
        this.heigth = heigth;
    }

    /**
     * @return the width
     */
    public int getWidth() {
        return width;
    }

    /**
     * @param width the width to set
     */
    public void setWidth(int width) {
        this.width = width;
    }

    /**
     * @return the length
     */
    public int getLength() {
        return length;
    }

    /**
     * @param length the length to set
     */
    public void setLength(int length) {
        this.length = length;
    }

    /**
     * @return the wingspan
     */
    public int getWingspan() {
        return wingspan;
    }

    /**
     * @param wingspan the wingspan to set
     */
    public void setWingspan(int wingspan) {
        this.wingspan = wingspan;
    }

    /**
     * @return the numOfPassengers
     */
    public int getNumOfPassengers() {
        return numOfPassengers;
    }

    /**
     * @param numOfPassengers the numOfPassengers to set
     */
    public void setNumOfPassengers(int numOfPassengers) {
        this.numOfPassengers = numOfPassengers;
    }

    /**
     * @return the enginesPoweredOn
     */
    public boolean isEnginesPoweredOn() {
        return enginesPoweredOn;
    }

    /**
     * @param enginesPoweredOn the enginesPoweredOn to set
     */
    public void setEnginesPoweredOn(boolean enginesPoweredOn) {
        this.enginesPoweredOn = enginesPoweredOn;
    }

    /**
     * @return the parachuteOnBoard
     */
    public boolean isParachuteOnBoard() {
        return parachuteOnBoard;
    }

    /**
     * @param parachuteOnBoard the parachuteOnBoard to set
     */
    public void setParachuteOnBoard(boolean parachuteOnBoard) {
        this.parachuteOnBoard = parachuteOnBoard;
    }

    /**
     * @return the type
     */
    public String getType() {
        return type;
    }

    /**
     * @param type the type to set
     */
    public void setType(String type) {
        this.type = type;
    }

    /**
     * @return the pilotName
     */
    public String getPilotName() {
        return pilotName;
    }

    /**
     * @param pilotName the pilotName to set
     */
    public void setPilotName(String pilotName) {
        this.pilotName = pilotName;
    }
    
    // Behavior
    ///////////
    
    
    public void turbulancce(){
        
    }
    
    
    public void engineFire(){
        
    }
}
