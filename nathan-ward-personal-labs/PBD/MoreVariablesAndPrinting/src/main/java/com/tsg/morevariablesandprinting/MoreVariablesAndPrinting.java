/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.morevariablesandprinting;

/**
 *
 * @author apprentice
 */
public class MoreVariablesAndPrinting {
    public static void main(String[] args) {
        String Name, Eyes, Teeth, Hair;
        int Age, Height, Weight;
        
        Name = "Nathaniel Ward";
        Age = 29; //This is my real age
        Height = 71;  //in inches
        Weight = 175; //in lbs.
        Eyes = "Blue";
        Teeth = "White";
        Hair = "Brown";
        double convertHeight = Math.round(Height / 2.54);
        double convertWeight = Math.round(Weight / 2.20);        
        
        System.out.println("Let's talk about " + Name + ".");
        System.out.println("He's " + Height + " inches (or " + (convertHeight) + " cm) tall.");
        System.out.println("He's " + Weight + " pounds (or " + (convertWeight) + " kg) heavy.");
        System.out.println("Actually, that's not to heavy.");
        System.out.println("He's got " + Eyes + " eyes and " + Hair + " hair.");
        System.out.println("His teeth are usually " + Teeth + " depending on the coffee.");
        
        
        System.out.println("If I add " + Age + ", " + Height + ", and" + 
                Weight + " I get" + (Age + Height + Weight) + ".");
    }
    
}
