/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook.dao;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public enum SearchTerm {
    FIRSTNAME, LASTNAME, STREETADDRESS, CITY, STATE, ZIP
}
