/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.tentimes;

/**
 *
 * @author apprentice
 */
public class TenTimes {
    public static void main(String[] args) {
        String phrase = "Mr. Mitchell is cool.";
        
        for (int i = 0; i < 10; i++) {
            System.out.println((i + 1) + ".) Mr. Mitchell is cool.");
        }
    }
}
