<%-- 
    Document   : response
    Created on : Mar 24, 2016, 1:54:50 PM
    Author     : ilyagotfryd
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Response</title>
    </head>
    <body>
        <h1>Response</h1>
        <a href="RSVPServlet">Home</a><br/>
        Your answer was: ${param.myAnswer}<br/>
        ${message}<br/>
    </body>
</html>
