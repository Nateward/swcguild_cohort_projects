/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lucky7s.dto;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class Stats {
    private int totalRolls, maxDollars, maxDollarsRolls;

    
     public int getTotalRolls() {
        return this.totalRolls;
    }
    
    public int getMaxDollars() {
        return this.maxDollars;
    }
    public int getMaxDollarsRolls() {
        return this.maxDollarsRolls;
    }

    /**
     * @param totalRolls the totalRolls to set
     */
    public void setTotalRolls(int totalRolls) {
        this.totalRolls = totalRolls + 1;
    }

    /**
     * @param maxDollars the maxDollars to set
     */
    public void setMaxDollars(int maxDollars) {
        this.maxDollars = maxDollars;
    }

    /**
     * @param maxDollarsRolls the maxDollarsRolls to set
     */
    public void setMaxDollarsRolls(int maxDollarsRolls) {
        this.maxDollarsRolls = maxDollarsRolls;
    }
}
