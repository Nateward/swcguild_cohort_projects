/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.countingwhile;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class CountingWhile {

    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);

        System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        String message = keyboard.nextLine();

        int n = 0;
        while (n < 100) {
            n += 10;//if this is removed it loops till stack blows

            System.out.println((n) + ". " + message);
        }
        n = 0;
        
         System.out.println("Type in a message, and I'll display it five times.");
        System.out.print("Message: ");
        message = keyboard.nextLine();
        
        System.out.print("How many times ");
        
        n = keyboard.nextInt();
        while (n < 100) {
            n += 10;//if this is removed it loops till stack blows

            System.out.println((n) + ". " + message);
        }
    }
}
