/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

var testContactData = [
    
    {
        contactId: 1,
        firstName: "Susan",
        lastName: "William",
        company: "IBM",
        email: "swilliams@ibm.com",
        phone: "555-1212"
    },
    {
        contactId: 2,
        firstName: "Willmah",
        lastName: "Buttfit",
        company: "EMC",
        email: "willmahBtft@emc.com",
        phone: "555-1234"
    },
    {
        contactId: 3,
        firstName: "Chuck",
        lastName: "Steak",
        company: "3M",
        email: "chuck.steak@3M.com",
        phone: "555-5656"
    }
    
];

var dummyDetailsContact = 
        {
         contactId: 42,
        firstName: "Dowwee-Cheteem",
        lastName: "Enhow",
        company: "Apple",
        email: "DWCTAH@apple.com",
        phone: "555-4783"   
};

var dummyEditContact = 
        {
         contactId: 52,
        firstName: "Jacque",
        lastName: "Strappe",
        company: "Sun",
        email: "jStrappe@sun.com",
        phone: "777-4783"   
};