/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bankaccountmanager.dto;

/**
 *
 * @author apprentice
 */
public class Checking extends Account {

    private final Integer OVERDRAFT_PENALTY = 10;

    @Override
    public void withdraw(Double withdrawAmount) {
        if (withdrawAmount > 0) {
            if (withdrawAmount <= availableBalance) {
                availableBalance = availableBalance - withdrawAmount;
                totalBalance = totalBalance - withdrawAmount;
            } else if ((withdrawAmount + OVERDRAFT_PENALTY) <= (availableBalance + 100)) {
                availableBalance = availableBalance - (withdrawAmount + OVERDRAFT_PENALTY);
                totalBalance = totalBalance - (withdrawAmount + OVERDRAFT_PENALTY);
            } else {
                System.out.println("Not enough funds.");
            }
        } else {
            System.out.println("You must enter a positive number.");
        }

        System.out.println("Your available balance is $" + this.availableBalance);
        System.out.println("Your total bank balance is $" + this.totalBalance + "\n");
    }
}
