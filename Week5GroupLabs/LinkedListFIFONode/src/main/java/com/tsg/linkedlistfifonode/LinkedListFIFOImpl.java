/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistfifonode;

import java.util.Iterator;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class LinkedListFIFOImpl implements LinkedListFIFONode {

    private Node head;
    private Node tail;
    private int numItems = 0;

    @Override
    public void enqueue(Object o) {
        Node prevLast = tail;
        tail = new Node();
        tail.item = o;

        if (isEmpty()) {
            head = tail;
        } else {
            prevLast.next = tail;
        }
        numItems++;
    }

    @Override
    public Object deQueue() {
        Object item = null;

        item = head.item;
        head = head.next;
        if (numItems == 1) {
            tail = null
        }
// Can't figure out what to do from here!!!!!!!
    }
//
//    @Override
//    public Object get(int index) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }
//
//    @Override
//    public void insert(int index, Object item) {
//        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public int size() {
        return numItems;
    }

    @Override
    public boolean isEmpty() {
        return numItems == 0;
    }

    @Override
    public Iterator iterator() {
        return new LinkedListQueueIterator();
    }

    private class LinkedListQueueIterator implements Iterator {

    
            Node current = head;

            @Override
            public boolean hasNext() {
            return current != null;
            }

            @Override
            public Object next() {
            
            Object item = current.item;
                current = current.next;

                return item;
            }

        }

    private class Node {

        Object item;
        Node next;
    }
}
