/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.inheritancespecialization;

import com.tsg.inheritancespecialization.employee.Manager;
import com.tsg.inheritancespecialization.employee.Employee;

/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        Employee someguy = new Employee();
        System.out.println(someguy.getName());
        
        new Employee("Jeff Dunham", "123-45-6789" );

        someguy.doWork();
        someguy.createObjectives();
        someguy.collectBonus();
        System.out.println(someguy.getName());
        Employee boss = new Manager();
        System.out.println("=================Manager Mode===========================");
        
        System.out.println(boss.getName());
        
        boss.doWork();
        boss.collectBonus();
        boss.createObjectives();
        boss.setName("pointy haird boss");

        //undercover boss rigt here
        ((Manager) boss).hire();
        ((Manager) boss).fire();
        ((Manager) boss).givePerformanceReview();

    }

}
