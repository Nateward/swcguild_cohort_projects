/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.dao;

import com.tsg.flooringmasteryspring.dto.Product;
import com.tsg.flooringmasteryspring.interfaces.ProductInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class ProductDAO implements ProductInterface{

    HashMap<String, Product> productList = new HashMap<>();
    private final String PRODUCT_DELIMETER = ",";
    private final String PRODUCT_FILE = "products.txt";
    
    public ProductDAO(){
        this.loadProductList();
    }

    @Override
    public Product getProduct(String productType) {
        return productList.get(productType);
    }

    @Override
    public List<String> getProductList() {
        return new ArrayList(productList.keySet());
    }

    @Override
    public void loadProductList() {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(PRODUCT_FILE)));
            
            while (sc.hasNext()) {
                String currentLine = sc.next();
                String[] tokens = currentLine.split(PRODUCT_DELIMETER);
                
                Product product = new Product();
                
                product.setProductType(tokens[0]);
                product.setCostPerSqFt(Double.parseDouble(tokens[1]));
                product.setLaborCostPerSqFt(Double.parseDouble(tokens[2]));
                
                productList.put(product.getProductType(), product);
                
            }
        } catch (FileNotFoundException ex) {
            System.out.println("Sorry could not load 'Product List'.");
            Logger.getLogger(ProductDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
