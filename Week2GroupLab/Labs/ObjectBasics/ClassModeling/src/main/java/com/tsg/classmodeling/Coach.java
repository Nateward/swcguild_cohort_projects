/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Coach {
    
    private String coachType;
    private String coachMaterial;
    private int width;
    private int height;
    private int length;

    public Coach(String coachType, String coachMaterial, int width, int height, int length) {
        this.coachType = coachType;
        this.coachMaterial = coachMaterial;
        this.width = width;
        this.height = height;
        this.length = length;
    }

    public String getCoachType() {
        return coachType;
    }

    public void setCoachType(String coachType) {
        this.coachType = coachType;
    }

    public String getCoachMaterial() {
        return coachMaterial;
    }

    public void setCoachMaterial(String coachMaterial) {
        this.coachMaterial = coachMaterial;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }
     //Behaviour
    
    
    /**
     * this behavior lets you know how my coach works.
     */
    
    public void recliners()
    {
        System.out.println("This coach reclines forward and backwards");
    }
}
