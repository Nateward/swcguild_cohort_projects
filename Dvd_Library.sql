-- phpMyAdmin SQL Dump
-- version 4.0.10deb1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: May 06, 2016 at 04:18 PM
-- Server version: 5.5.47-0ubuntu0.14.04.1
-- PHP Version: 5.5.9-1ubuntu4.14

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `Dvd_Library`
--

-- --------------------------------------------------------

--
-- Table structure for table `Dvd`
--

CREATE TABLE IF NOT EXISTS `Dvd` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(60) NOT NULL,
  `releaseDate` varchar(4) NOT NULL,
  `mpaaRating` varchar(5) NOT NULL,
  `directorsName` varchar(50) NOT NULL,
  `studio` varchar(100) NOT NULL,
  `userNote` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `Dvd`
--

INSERT INTO `Dvd` (`id`, `title`, `releaseDate`, `mpaaRating`, `directorsName`, `studio`, `userNote`) VALUES
(1, 'Harry Potter', '2004', 'PG', 'someone', 'WB', 'dgfhikkhfdsdbj'),
(2, 'Star Wars', '2004', 'R', 'Lucas', 'Lucas Studios', 'blah blah blah'),
(5, 'Star Trek', '2014', 'PG-13', 'J.J. Abrahams', 'WB', 'Loved this!'),
(6, 'aaa', '2344', 'r', 'someone', 'WB', '');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
