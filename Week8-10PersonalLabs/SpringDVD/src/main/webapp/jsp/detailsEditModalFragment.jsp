<%-- 
    Document   : detailsEditModalFragment
    Created on : 7-Apr-2016, 2:09:16 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="detailsModalLabel">Dvd Details</h4>
            </div>
            <div class="modal-body">
                <h3 id="dvd-id"></h3>
                <table class="table table-bordered">
                    <tr>
                        <th>Title:</th>
                        <td id="dvd-title"></td>
                    </tr>
                    <tr>
                        <th>Release:</th>
                        <td id="dvd-releaseDate"></td>
                    </tr>
                    <tr>
                        <th>MPAA Rating:</th>
                        <td id="dvd-rating"></td>
                    </tr>
                    <tr>
                        <th>Director:</th>
                        <td id="dvd-director"></td>
                    </tr>
                    <tr>
                        <th>Studio:</th>
                        <td id="dvd-studio"></td>
                    </tr>
                    <tr>
                        <th>Notes:</th>
                        <td id="dvd-notes"></td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-laballedby="editModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">&times;</span>
                    <span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title" id="editModalLabel">Edit Dvd</h4>
            </div>
            <div class="modal-body">
                <input type="hidden" value="0" id="edit-id"/>
                <form class="form-horizontal" role="form">
                    <div class="form-group">
                        <label for="edit-title" class="col-md-4 control-label" placeholder="Title">Title:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-title" placeholder="Casino Royale"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-releaseDate" class="col-md-4 control-label" placeholder="Release Date:">Release Date:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-releaseDate" placeholder="Release Date"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-rating" class="col-md-4 control-label" placeholder="MPAA Rating:">MPAA Rating:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-rating" placeholder="PG-13"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-director" class="col-md-4 control-label">Director:</label>
                        <div class="col-md-8">
                            <input type="text" class="form-control" id="edit-director" placeholder="Director"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-studio" class="col-md-4 control-label">Studio:</label>
                        <div class="col-md-8">
                            <input type="tel" class="form-control" id="edit-studio" placeholder="MGM"/>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="edit-user-notes" class="col-md-4 control-label">Notes:</label>
                        <div class="col-md-8">
                            <textarea class="form-control" id="edit-user-notes" form="usrform" placeholder="User Notes"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <div class="pull-right">
                            <button type="submit" id="edit-button" class="btn btn-primary" data-dismiss="modal">Edit Dvd</button>
                            <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">

            </div>
        </div>
    </div>
</div>