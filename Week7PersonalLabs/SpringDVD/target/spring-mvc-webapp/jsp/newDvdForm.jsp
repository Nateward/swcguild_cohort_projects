<%-- 
    Document   : NewContactFormNoAjax
    Created on : 29-Mar-2016, 10:24:07 AM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/dvdvideo.jpg" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
        </div>
        <div class="container">
            <h1>New Dvd Form</h1>
            <a href="displayDvdsList">View Dvd Library</a>
            <hr/>
            <sf:form modelAttribute="dvd" class="form-horizontal" id="usrform" role="form" action="addNewDvd" method="POST">
                <div class="form-group">
                    <label for="add-title" class="col-md-4 control-label">Title:</label>
                    <div class="col-md-8">
                        <sf:input type="text" path="title" class="form-control text-right" id="add-title" name="title" placeholder="Title"/>
                        <sf:errors path="title" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-release-date" class="col-md-4 control-label">Release Date:</label>
                    <div class="col-md-8">
                        <sf:input type="text" path="releaseDate" class="form-control text-right" id="add-release-date" name="releaseDate" placeholder="Release Date"/>
                        <sf:errors path="releaseDate" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-mpaaRating" class="col-md-4 control-label">MPAA Rating:</label>
                    <div class="col-md-8">
                        <sf:input type="text" path="mpaaRating" class="form-control text-right" id="add-mpaaRating" name="rating" placeholder="MPAA Rating"/>
                        <sf:errors path="mpaaRating" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-director" class="col-md-4 control-label">Director:</label>
                    <div class="col-md-8">
                        <sf:input type="text" path="directorsName" class="form-control text-right" id="add-director" name="director" placeholder="Director"/>
                        <sf:errors path="directorsName" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-studio" class="col-md-4 control-label">Studio:</label>
                    <div class="col-md-8">
                        <sf:input type="text" path="studio" class="form-control text-right" id="add-studio" name="studio" placeholder="Studio"/>
                        <sf:errors path="studio" cssClass="text-danger"></sf:errors>
                    </div>
                </div>
                <div class="form-group">
                    <label for="add-user-notes" class="col-md-4 control-label">Notes:</label>
                    <div class="col-md-8">
                        <sf:textarea type="text" path="userNote" class="form-control text-right" id="add-user-notes" form="usrform" name="notes" placeholder="User Notes"></sf:textarea>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-md-offset-4 col-md-8">
                        <button type="submit" id="add-button" class="btn btn-info">Add New Dvd</button>
                    </div>
                </div>
            </sf:form>
        </div>
    </body>
</html>
