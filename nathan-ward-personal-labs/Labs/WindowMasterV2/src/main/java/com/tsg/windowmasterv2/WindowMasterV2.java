/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.windowmasterv2;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class WindowMasterV2 {
    public static void main(String[] args) {
        float height = 0;
        float width = 0;
        
        String stringHeight = "";
        String stringWidth = "";
        
        final float maxHeight = 25.5f;
        final float minHeight = 1.0f;
        final float maxWidth = 25.5f;
        final float minWidth = 1.0f;
        final float glass;
        final float trim;
        
        float areaOfWindow = 0;
        float cost = 0;
        float perimeterOfWindow = 0;
        
        Scanner sc = new Scanner(System.in);
        
        System.out.println("Please enter window height: ");
        stringHeight = sc.nextLine();
        
        System.out.println("Please enter window width: ");
        stringWidth = sc.nextLine();
        
         System.out.println("Please enter the cost of the glass: ");
        glass = sc.nextInt();
        
        System.out.println("Please enter the cost of the trim: ");
        trim = sc.nextInt();
        
        height = Float.parseFloat(stringHeight);
        width = Float.parseFloat(stringWidth);
        
        if (height > maxHeight || height < minHeight){
            System.out.println("Height is out of range. Height should be between " + 
                    minHeight + " - " + maxHeight);
        }
        else {
            if(width > maxWidth || width < minWidth){
                System.out.println("Width is out of range. Width should be between " + 
                    minWidth + " - " + maxWidth);
            }
            else{
        
        
        areaOfWindow = height * width;
        perimeterOfWindow = 2 * (height + width);
        
        cost = ((glass * areaOfWindow) + (trim * perimeterOfWindow));
        
        System.out.println("Glass cost is = $" + glass);
        System.out.println("Trim cost is = $" + trim);
        System.out.println("Window height = " + stringHeight);
        System.out.println("Window width = " + stringWidth);
        System.out.println("Window area = " + areaOfWindow);
        System.out.println("Window preimeter = " + perimeterOfWindow);
        System.out.println("Total cost = " + cost);
        
            }
        }
        
    }
    
}
