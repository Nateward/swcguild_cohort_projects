/*
* To change this license header, choose License Headers in Project Properties.
* To change this template file, choose Tools | Templates
* and open the template in the editor.
*/
package com.tsg.warmupsweek2;

import java.util.HashMap;
import java.util.Set;

/**
*
* @author apprentice
*/
public class HashMapWarmUp{
public static void main(String[] args) {

HashMap<String, Double> playerList = new HashMap<>();

playerList.put("Smith " , 23.0 );
playerList.put("Jones", 12.0 );
playerList.put("Jordan" , 45.0 );
playerList.put("Pippen" , 32.0 );
playerList.put("Kerr" , 15.0 );

Set<String> keySet = playerList.keySet();
        
        double x = 0;
        for (String key : keySet) {
            System.out.println(key +" - " + playerList.get(key) + " points" );
            x += playerList.get(key);
 }
        x = x / playerList.size();
        System.out.println("");
        System.out.println("Team point average : " + x);
    }
    
}

