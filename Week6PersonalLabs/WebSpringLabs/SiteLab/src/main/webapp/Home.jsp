<%-- 
    Document   : Home
    Created on : 4-Apr-2016, 1:31:59 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html lang="en">
<head>
	<link rel="stylesheet" type="text/css" href="css/styles.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap-theme.css">
	<meta charset="UTF-8">
	<title>WebUI Lab1</title>
</head>
<body>

	<header>
		<div class="container-fluid">
			<nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
				<div class="container-fluid">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="navbar-brand" href="#">Company</a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						<ul class="nav navbar-nav">
							<li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
							<li><a href="#">About</a></li>
							<li><a href="#">Contacts</a></li>
						</ul>     
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
	</header>
	

	<br>
	<br>
	<br>
	<br>

	<!-- Start of div container containing 3 images. -->
	<div class="container-fluid">
		<div class="row">
			<div class="container-fluid col-lg-3">
				<img src="Image.gif" class="img-responsive" alt="Responsive image">
			</div>

			<div class="container-fluid col-lg-3">
				<img src="Image.gif" class="img-responsive" alt="Responsive image">
			</div>

			<div class="container-fluid col-lg-3">
				<img src="Image.gif" class="img-responsive" alt="Responsive image">
			</div>
			<div class="container-fluid col-lg-3"></div>

		</div>
	</div>
	<br>
	<div class="container-fluid ">
		<div class="row">
			<div class=" col-lg-pull-12">
				<!-- Nav tabs -->
				<ul class="nav nav-tabs" role="tablist">
					<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Home</a></li>
					<li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Resources</a></li>
					<li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Account</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Arrange</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Empower</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Innovate</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Create</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Design</a></li>
					<li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Implement</a></li>
				</ul>

			</div>
		</div>
		<div class="row">
			<div class="container-fluid col-lg-12">
				<br/>
				<div class="row">

					<div class="container-fluid col-lg-9">
						<img src="Picture.gif" class="img-responsive" alt="Responsive image">
						<hr/>
					</div>

					<div class="container-fluid col-lg-3">
						<img src="News.gif" class="img-responsive" alt="Responsive image">
					</div>
				</div>

			</div>
		</div>
		<div class="row">
			<div class="container-fluid col-lg-4">
				<img src="SmallPic.gif" class="img-responsive" alt="Responsive image">
			</div>

			<div class="container-fluid col-lg-4">
				<img src="SmallPic.gif" class="img-responsive" alt="Responsive image">
			</div>

			<div class="container-fluid col-lg-4">
				<img src="SmallPic.gif" class="img-responsive" alt="Responsive image">
			</div>
		</div>
	</div>

	

	<script	src="js/jquery-2.2.2.min.js"></script>
	<script	src="js/bootstrap.min.js"></script>
	<script type="text/javascript" src="scripts/myJS.js"></script>
</body>
<footer>
	<div class="container-fluid">
	<hr/>
		
		<div class="footer">
		<span class="glyphicon glyphicon-copyright-mark" aria-hidden="true"> Company 2012</span>
			
		</div>
	</div>
</footer>
</html>