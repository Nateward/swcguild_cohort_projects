/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.factorizoroo;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class FactorizorApp {

    public static void main(String[] args) {

        Scanner sc = new Scanner(System.in);

        int x;
        String number;
        boolean primeFromOtherClass, perfectionFromOtherClass;

        System.out.println("Enter a number: ");
        number = sc.nextLine();
        x = Integer.parseInt(number);

        System.out.println("The factors of " + x + " are");

        FactorizorOO classThatIsOO = new FactorizorOO();
        primeFromOtherClass = classThatIsOO.factorBro(x);
        perfectionFromOtherClass = classThatIsOO.perfectBro(x);

        if (perfectionFromOtherClass == true) {
            System.out.println(x + " is a perfect number!");
        } else {
            System.out.println(x + " is not a perfect number.");
        }

        if (primeFromOtherClass == false) {
            System.out.println(x + " is not a prime number.");
        } else {
            System.out.println(x + " is a prime number.");
        }

    }
}
