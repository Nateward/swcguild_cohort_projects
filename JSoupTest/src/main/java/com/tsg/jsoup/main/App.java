/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.jsoup.main;

import java.io.File;  
import java.io.IOException;  
import org.jsoup.Jsoup;  
import org.jsoup.nodes.Document;  
import org.jsoup.nodes.Element;  
import org.jsoup.select.Elements;  

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class App {

    public static void main(String[] args) throws IOException {
        Document doc = Jsoup.connect("www.reddit.com").get();
        Elements contents = doc.getElementsByTag("p");
        Elements innercontents = contents.
        Elements links = doc.getElementsByTag("a");

        for (Element link : links) {
            String key = link.attr("href");
            String value = link.text();
            System.out.println("Param name: " + key + " \nParam value: " + value);
        }
    }
}
