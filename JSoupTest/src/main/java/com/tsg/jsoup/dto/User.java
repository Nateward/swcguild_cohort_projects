/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.jsoup.dto;

import java.util.ArrayList;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class User {
    private String userName;
    private String userTag;
    private String reason;
    private String numOfPosts;
    private Integer id;
    private ArrayList<String> cityName;
    private String cityTag;

    /**
     * @return the userName
     */
    public String getUserName() {
        return userName;
    }

    /**
     * @param userName the userName to set
     */
    public void setUserName(String userName) {
        this.userName = userName;
    }

    /**
     * @return the userTag
     */
    public String getUserTag() {
        return userTag;
    }

    /**
     * @param userTag the userTag to set
     */
    public void setUserTag(String userTag) {
        this.userTag = userTag;
    }

    /**
     * @return the reason
     */
    public String getReason() {
        return reason;
    }

    /**
     * @param reason the reason to set
     */
    public void setReason(String reason) {
        this.reason = reason;
    }

    /**
     * @return the numOfPosts
     */
    public String getNumOfPosts() {
        return numOfPosts;
    }

    /**
     * @param numOfPosts the numOfPosts to set
     */
    public void setNumOfPosts(String numOfPosts) {
        this.numOfPosts = numOfPosts;
    }

    /**
     * @return the id
     */
    public Integer getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * @return the cityTag
     */
    public String getCityTag() {
        return cityTag;
    }

    /**
     * @param cityTag the cityTag to set
     */
    public void setCityTag(String cityTag) {
        this.cityTag = cityTag;
    }

    /**
     * @return the cityName
     */
    public ArrayList<String> getCityName() {
        return cityName;
    }

    /**
     * @param cityName the cityName to set
     */
    public void setCityName(ArrayList<String> cityName) {
        this.cityName = cityName;
    }
}
