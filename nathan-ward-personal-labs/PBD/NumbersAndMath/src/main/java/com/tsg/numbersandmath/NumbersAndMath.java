/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.numbersandmath;

/**
 *
 * @author apprentice
 */
public class NumbersAndMath {
    public static void main(String[] args) {
//        Prints to the console window stuff about chickens
        System.out.println("I will now count my chickens:");
        
        //Prints Hens and the equations end result = 30
        System.out.println("Hens " + (25.0f + 30 / 6) );
//        Prints roosters and the equations end result: 25*3=75 75%4=3 100-3=97 97 is the answer
        System.out.println("Roosters " + (100.0f - 25 * 3 % 4) );
        
//        Prints to the console window now I will count the eggs
        System.out.println("Now I will count the eggs:");
        
       
//       prints  the answer to this equation
        System.out.println(3.0f + 2 + 1 - 5 + 4 % 2 - 1.0f / 4 + 6);
        
//        prints whats in the println()
        System.out.println("Is it true that 3 + 2 < 5 - 7?");
        
//        prints the answer false
        System.out.println(3 + 2 < 5 - 7);
        
        System.out.println("What is 3 + 2? " + (3.0f + 2) );//prints the answer 5
        System.out.println("What is 5 - 7? " + (5.0f - 7) );// prints the answer -2
        
//        Prints to the console window oh, that's why it's false.
        System.out.println("Oh, that's why it's false.");
        
//        Prints to the console window How about some more.
        System.out.println("How about some more.");
        
        System.out.println("Is it greater? " + (5 > -2) );
        System.out.println("Is it greater or equal? " + (5 >= -2));
        System.out.println("Is it less or equal? " + (5 <= -2));
    
    }
    
}
