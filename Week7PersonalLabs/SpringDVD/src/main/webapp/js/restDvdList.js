/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */


$(document).ready(function () {

    loadDvds();

    $('#add-button').click(function (event) {

        event.preventDefault();
        $.ajax({
            type: 'POST',
            url: 'http://localhost:8080/SpringDVD/dvd',
            data: JSON.stringify({
                title: $('#add-title').val(),
                releaseDate: $('#add-releaseDate').val(),
                mpaaRating: $('#add-rating').val(),
                directorsName: $('#add-director').val(),
                studio: $('#add-studio').val(),
                userNote: $('#add-notes').val(),
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            $('#add-title').val('');
            $('#add-releaseDate').val('');
            $('#add-rating').val('');
            $('#add-director').val('');
            $('#add-studio').val('');
            $('#add-notes').val('');
            loadDvds();
        });
    });

    $('#edit-button').click(function (event) {
        event.preventDefault();
        $.ajax({
            type: 'PUT',
            url: 'dvd/' + $('#edit-dvd-id').val(),
            data: JSON.stringify({
                title: $('#edit-title').val(),
                releaseDate: $('#edit-releaseDate').val(),
                mpaaRating: $('#edit-rating').val(),
                directorsName: $('#edit-director').val(),
                studio: $('#edit-studio').val(),
                userNote: $('#edit-notes').val()
            }),
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
            },
            'dataType': 'json'
        }).success(function (data, status) {
            hideEditForm();
            loadDvds();
        });
    });
});

function loadDvds() {
    clearDvdTable();

    var contentRows = $('#contentRows');

    $.ajax({
        type: 'GET',
        url: "http://localhost:8080/SpringDVD/Dvds"
    }).success(function (data, status) {
        $.each(data, function (index, dvd) {
            var title = dvd.title;
            var director = dvd.directorsName;
            var id = dvd.id;
            var row = '<tr>';
            row += '<<td>' + title + '</td>';
            row += '<<td>' + director + '</td>';
            row += '<<td><a onclick="editDvd(' + id + ')">Edit</a></td>';
            row += '<<td><a onclick="deleteDvd(' + id + ')">Close</a></td>';
            row += '</tr>';

            contentRows.append(row);
        });
    });
}

function clearDvdTable() {
    $('#contentRows').empty();
}

function deleteDvd(id) {
    var answer = confirm("Do you really want to delete this Dvd?");

    if (answer === true) {

        $.ajax({
            type: 'DELETE',
            url: 'dvd/' + id
        }).success(function () {
            loadDvds();
        })

    }
}

function editDvd(id) {

    $.ajax({
        type: 'GET',
        url: 'dvd/' + id
    }).success(function (dvd, status) {
        $('#edit-dvd-id').val(dvd.id);
        $('#edit-title').val(dvd.title);
        $('#edit-releaseDate').val(dvd.releaseDate);
        $('#edit-rating').val(dvd.mpaaRating);
        $('#edit-director').val(dvd.directorsName);
        $('#edit-studio').val(dvd.studio);
        $('#edit-notes').val(dvd.userNote);
        $('#editFormDiv').show();
        $('#addFormDiv').hide();
    });
}

function hideEditForm() {
    $('#edit-title').val('');
    $('#edit-release-date').val('');
    $('#edit-mpaaRating').val('');
    $('#edit-director').val('');
    $('#edit-studio').val('');
    $('#edit-user-notes').val('');
    $('#addFormDiv').show();
    $('#editFormDiv').hide();
}