/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.queueui;

import com.tsg.queueui.generics.ArrayQueueImpl_IlyasQueue;
import java.util.Iterator;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class QueueDriverApp {

    public static void main(String[] args) {
//        ArrayQueueInterface theQueue = new ArrayQueueImpl();

        ArrayQueueImpl_IlyasQueue<String> theQueue = new ArrayQueueImpl_IlyasQueue<>();

        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        String f = "f";

        System.out.println("Enqueueing " + a);
        theQueue.enqueue(a);
        
        System.out.println("Enqueueing " + b);
        theQueue.enqueue(b);
        
        System.out.println("Enqueueing " + c);
        theQueue.enqueue(c);
        
        System.out.println("Enqueueing " + d);
        theQueue.enqueue(d);
        
        Iterator itr = theQueue.iterator();
        System.out.println("Printing values of queue...");
        while (itr.hasNext()) {
            System.out.println(itr.next());
        }
        
        System.out.println("Printing stack values using enhanced for loop...");
        for (Object o : theQueue) {
            System.out.println(o);
        }
        
        System.out.println("\nDequeueing... ");
        System.out.println("value : " + theQueue.dequeue());
        
         System.out.println("Enqueueing " + e);
        theQueue.enqueue(e);
        
        System.out.println("Enqueueing " + f);
        theQueue.enqueue(f);
        
        System.out.println("\nDequeueing... ");
        System.out.println("value : " + theQueue.dequeue());
        
        System.out.println("\nDequeueing... ");
        System.out.println("value : " + theQueue.dequeue());
        
       
        
        
    }
}
