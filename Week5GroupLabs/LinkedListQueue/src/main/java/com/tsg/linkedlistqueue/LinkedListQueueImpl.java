/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistqueue;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class LinkedListQueueImpl implements LinkedListQueueInterface{
    private Node first;
    private Node last;
    private int numItems=0;

    @Override
    public void enqueue(Object item) {
        Node prevLast = last;
        last = new Node();
        last.item = item;

        if (isEmpty()) {
            first = last;
        } else {
            prevLast.next = last;
        }
        numItems++;
    }

    @Override
    public Object dequeue() {
        Object objectToDequeue= null;
        if (numItems==0) {
            return null;
        }
        if(numItems==1){
            objectToDequeue = first.item;
            first=null;
            last = first;
        }else{
            Node secondNode = first.next;
            objectToDequeue = first.item;
            first = secondNode; 
        }
        numItems--;
        return objectToDequeue;
    }
    
    
    @Override
    public int size() {
        return numItems;
    }

    @Override
    public boolean isEmpty() {
        return numItems==0;
    }

    @Override
    public Iterator iterator() {
        return new StackIterator();
    }
    
    private class StackIterator implements Iterator{
        Node current = first;
        @Override
        public boolean hasNext() {
            return current != null;
        }

        @Override
        public Object next() {
            if (current == null) {
                return null;
            }
            Object item = current.item;
            current = current.next;
            
            return item;
        }
        
    }
    
    private class Node{
        Object item;
        Node next;
    }
    
}
