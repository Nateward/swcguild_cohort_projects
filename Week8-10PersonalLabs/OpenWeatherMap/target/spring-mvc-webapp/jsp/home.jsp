<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>Nate's Page</title>
        <!-- Bootstrap core CSS -->
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">

        <!-- My Icon -->
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
        <style>
            div div.jumbotron{
                background-image: url(${pageContext.request.contextPath}/img/openWeatherIcons/WeatherWallpaper.jpg);
                background-size: cover;
                padding: 44px;

            }
            h4#weather-content{
                color: white;
            }
            #weather-temp{
                color: white;
            }
            #weather-description{
                color: white;
            }
            #city-name-status{
                color: white;
            }
            .well{
                background: none;
                background-color: rgba(999, 999, 999, 0.3);
                text-decoration: none;
                border-color: black;
                border-radius: 10px;
                margin-bottom: 0px;
                margin-right: 16px;
                padding-top: 10px;
                padding-bottom: 9px;
                /*max-height: 20px;*/
            }
        </style>
    </head>
    <header>
        <div class="container-fluid">
            <nav class="navbar navbar-default navbar-fixed-top navbar-inverse">
                <div class="container-fluid">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"><span class="glyphicon glyphicon-leaf"></span></a>
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home <span class="sr-only">(current)</span></a></li>
                            <li role="presentation" class="dropdown">
                                <a class="dropdown-toggle" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                                    Projects<span class="caret"></span>
                                </a>
                                <ul class="dropdown-menu label-default">
                                    <li><a href="#">SWC Guild</a></li>
                                    <li><a href="#">Personal</a></li>
                                    <li role="separator" class="divider"></li>
                                    <li class="list-group-item disabled" style="margin: 20px 0px"><a href="#">Upcoming Projects</a></li>
                                </ul>
                            </li>
                            <li><a href="#">About Me</a></li>
                            <li><a href="#">Contact Me</a></li>
                        </ul>     
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>
        </div>
    </header>
    <body>
        <div class="container">
            <br/>
            <br/>
            <br/>
            <div class="row">
                <div class="col-lg-8 col-lg-offset-2">
                    <div class="jumbotron">
                        <div class="row pull-right"><h2 id="city-name-status">Find Your Weather</h2></div>
                        <div class="row"><br/><br/><br/><br/></div>
                        <div class="row"><br/><br/><br/><br/><br/></div>
                        <div class="row" id="weather-spacing"><br/><br/><br/><br/><br/></div>
                        <div class="row">
                            <h4 class="pull-right well" id="weather-content" style="display: none">
                                <div>Temperature: <span id="weather-temp"></span></div>
                                <div>Conditions: <span id="weather-description"></span><img id="weather-status-img"></div>
                            </h4>
                        </div>
                        <div class="row">
                            <div class="form-inline pull-right" role="form">
                                <div class="form-group pull-left">
                                    <div class="col-md-6">
                                        <input type="text" class="form-control" id="add-city" placeholder="City Name"/>
                                    </div>
                                </div>
                                <div class="form-group pull-right">
                                    <div class="col-md-3">
                                        <button type="submit" id="city-button" class="btn btn-sm btn-default">
                                            Submit
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row" id="5-Day-Forecast" style="visibility: visible">
                <div class="col-md-offset-1 col-md-10">
                    <table>
                    <thead>
                    <h3>5 Day Forecast</h3>
                    </thead>
                    <tbody>
                        <tr id="forecast-Days-Content">
                        
                    </tr>
                    </tbody>
                </table>
                </div>
            </div>
        </div>
        <!-- Placed at the end of the document so the pages load faster -->
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/openWeather.js"></script>

    </body>
</html>

