/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.tsg.exceptionsexamples.CheckedExceptionExample;
import java.util.logging.Level;
import java.util.logging.Logger;
import junit.framework.Assert;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class CheckExceptionUnitTest {
    
    public CheckExceptionUnitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void CanEchoValueLessThan42(){
        CheckedExceptionExample example = new CheckedExceptionExample();
        try{
        int result = example.SillyCheckedException(23);
        
        Assert.assertEquals(23, result);
        }catch(Exception e)
        {
            Assert.fail();
        }
    }
    
    @Test
    public void WillThrowExceptionValueMoreThan42()
    {
        try {
            CheckedExceptionExample example = new CheckedExceptionExample();
            
            int result = example.SillyCheckedException(53);
        } catch (Exception ex) {
            Assert.assertEquals("You have gone beyond 42.", ex.getMessage());
            Assert.assertTrue(true);
        }
        
    }
}
