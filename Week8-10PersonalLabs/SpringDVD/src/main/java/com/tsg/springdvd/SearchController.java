/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd;

import com.tsg.springdvd.dao.DvdDAO;
import com.tsg.springdvd.dao.SearchTerm;
import com.tsg.springdvd.dto.Dvd;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.inject.Inject;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@Controller
public class SearchController {
    
    private final DvdDAO dao;
    
    @Inject
    public SearchController(DvdDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = "/search", method = RequestMethod.GET)
    public String displaySearch() {
        return "search";
    }
    
    @RequestMapping(value = "search/Dvds", method = RequestMethod.POST)
    @ResponseBody
    public List<Dvd> searchDvds(@RequestBody Map<String, String> searchMap){
        
        Map<SearchTerm, String> criteriaMap = new HashMap<>();
        
        String currentTerm = searchMap.get("title");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.TITLE, currentTerm);
        }
        currentTerm = searchMap.get("releaseDate");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.RELEASEDATE, currentTerm);
        }
        currentTerm = searchMap.get("mpaaRating");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.MPAARATING, currentTerm);
        }
        currentTerm = searchMap.get("directorsName");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.DIRECTORSNAME, currentTerm);
        }
        currentTerm = searchMap.get("studio");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.STUDIO, currentTerm);
        }
        currentTerm = searchMap.get("userNote");
        if (!currentTerm.isEmpty()) {
            criteriaMap.put(SearchTerm.USERNOTE, currentTerm);
        }
        return dao.searchDvds(criteriaMap);
    }
}
