/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.alittlequiz;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class aLittleQuiz {

    public static void main(String[] args) {

        Scanner keyboard = new Scanner(System.in);

        String answer = "";
        int answerNumber, correct = 0;

        System.out.println("Are you ready for a quiz(yes or no)? ");
        answer = keyboard.nextLine();
        
        
        if (answer.equalsIgnoreCase("yes")) {
            System.out.println("Okay here it comes!\n");

            System.out.println("Q1) What is the capitol of Alaska? ");
            System.out.println("\t 1) Melbourne");
            System.out.println("\t 2) Anchorage");
            System.out.println("\t 3) Juneau");

            answer = keyboard.nextLine();
            answerNumber = Integer.parseInt(answer);

            if (answerNumber == 3) {

                correct++;
                System.out.println("That's right!\n");
                
            } else {
                System.out.println("That's wrong.\n");
            }

            System.out.println("Q2) Can you store the value \"cat\" in a variable of type int? ");
            System.out.println("\t 1) yes");
            System.out.println("\t 2) no");

            answer = keyboard.nextLine();
            answerNumber = Integer.parseInt(answer);

            if (answerNumber == 2) {

                correct++;
                System.out.println("That's right!\n");
            } else {
                System.out.println("Sorry, \"cat\" is a string. Int's can only store numbers.\n");
            }

            System.out.println("Q3) What is the result of 9+6/3?");
            System.out.println("1) 5");
            System.out.println("2) 11");
            System.out.println("3) 15/3");
            
            answer = keyboard.nextLine();
            answerNumber = Integer.parseInt(answer);

            if (answerNumber == 2) {

                correct++;
                System.out.println("That's correct!\n");
            } else {
                System.out.println("Sorry, wrong!\n");
            }

            if (correct == 0) {
                System.out.println("Overall, you got 0 out of 3 correct.");
            } else if (correct == 1) {
                System.out.println("Overall, you got 1 out of 3 correct.");
            } else if (correct == 2) {
                System.out.println("Overall, you got 2 out of 3 correct.");
            } else {
                System.out.println("You got all of right!!!");
            }

        } else {
            System.out.println("Oh that's too bad! Good-bye.");
        }

        System.out.println("Thanks for playing!");
    }

}
