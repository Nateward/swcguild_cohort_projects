<%-- 
    Document   : home
    Created on : 28-Mar-2016, 1:16:10 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/phone-book.png" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation" >
                        <a href="${pageContext.request.contextPath}/displayAddressBook">Address Book</a>
                    </li>
                </ul>
            </div>
            <div class="row">
                <div class="col-md-7">
                    <h2>My Addresses</h2>
                    <table id="addressTable" class="table table-hover">
                        <tr>
                            <th width="30%">Name</th>
                            <th width="10%">City</th>
                            <th width="20%">State</th>
                            <th width="20%"></th>
                            <th width="20%"></th>
                        </tr>
                        <tbody id="contentRows"></tbody>
                    </table>
                </div>
                <!-- start of add form -->
                <div class="col-md-4">
                    <h2 class="col-md-offset-5 h3">Add New Address</h2>
                    <form class="form-horizontal" role="form">
                        <div class="form-group">
                            <label for="add-first-name" class="col-md-4 control-label" placeholder="First Name">First Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-first-name" placeholder="First Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-last-name" class="col-md-4 control-label" placeholder="Last Name">Last Name:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-last-name" placeholder="Last Name"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-address" class="col-md-4 control-label" placeholder="Street Address">Street Address:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-address" placeholder="Street Address"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-city" class="col-md-4 control-label">City:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-city" placeholder="City"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-state" class="col-md-4 control-label">State:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-state" placeholder="State"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="add-zip" class="col-md-4 control-label">Zip:</label>
                            <div class="col-md-8">
                                <input type="text" class="form-control" id="add-zip" placeholder="Zip"/>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-lg-3 pull-right">
                                <button type="submit" id="add-button" class="btn btn-primary ">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>

            <div class="modal fade" id="detailsModal" tabindex="-1" role="dialog" aria-labelledby="detailsModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="detailsModalLabel">Address Details</h4>
                        </div>
                        <div class="modal-body">
                            <h3 id="address-id"></h3>
                            <table class="table table-bordered">
                                <tr>
                                    <th>Name:</th>
                                    <td id="address-name"></td>
                                </tr>
                                <tr>
                                    <th>Street Address:</th>
                                    <td id="address-street"></td>
                                </tr>
                                <tr>
                                    <th>City:</th>
                                    <td id="address-city"></td>
                                </tr>
                                <tr>
                                    <th>State:</th>
                                    <td id="address-state"></td>
                                </tr>
                                <tr>
                                    <th>Zip:</th>
                                    <td id="address-zip"></td>
                                </tr>
                            </table>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-laballedby="editModalLabel" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal">
                                <span aria-hidden="true">&times;</span>
                                <span class="sr-only">Close</span>
                            </button>
                            <h4 class="modal-title" id="editModalLabel">Edit Address</h4>
                        </div>
                        <div class="modal-body">
                            <h3 id="address-id"></h3>
                            <form class="form-horizontal" role="form">
                                <div class="form-group">
                                    <label for="edit-first-name" class="col-md-4 control-label" placeholder="First Name">First Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-firstName" placeholder="First Name"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-last-name" class="col-md-4 control-label" placeholder="Last Name">Last Name:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-lastName" placeholder="Last Name"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-street" class="col-md-4 control-label" placeholder="Street Address">Street Address:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-street" placeholder="Street Address"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-city" class="col-md-4 control-label">City:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-city" placeholder="City"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-state" class="col-md-4 control-label">State:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-state" placeholder="State"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="edit-zip" class="col-md-4 control-label">Zip:</label>
                                    <div class="col-md-8">
                                        <input type="text" class="form-control" id="edit-zip" placeholder="Zip"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="pull-right">
                                        <button type="submit" id="edit-button" class="btn btn-primary" data-dismiss="modal">Edit Address</button>
                                        <button type="button" class="btn btn-warning" data-dismiss="modal">Cancel</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div class="modal-footer">

                        </div>
                    </div>
                </div>
            </div>
            <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
            <script src="${pageContext.request.contextPath}/js/mockData.js"></script>
            <script src="${pageContext.request.contextPath}/js/addressList.js"></script>
    </body>
</html>
