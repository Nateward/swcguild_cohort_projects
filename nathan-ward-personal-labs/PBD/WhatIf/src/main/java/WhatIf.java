/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author apprentice
 */
public class WhatIf {
//Stuff within the curly brackets is printed to the console if the "if Statement" is true
    public static void main(String[] args) {
//        Changed the variable for people so that the if statements for 
//cat's is equal and no print about cats is made
        int people = 30;
        int cats = 30;
        int dogs = 15;
        
//        if people is less than cats show in the console what is within the ""'s
        if (people < cats) {
            System.out.println("Too many cats!  The world is doomed!");
        }
//        if people is greater than cats show in the console what is within the ""'s
        if (people > cats) {
            System.out.println("Not many cats!  The world is saved!");
        }
//        if people is less than dogs show in the console what is within the ""'s
        if (people < dogs) {
            System.out.println("The world is drooled on!");
        }
//        if people is greater than dogs show in the console what is within the ""'s
        if (people > dogs) {
            System.out.println("The world is dry!");
        }

        dogs += 5;
//        if people is greater than or equal to dogs show in the console what is within the ""'s
        if (people >= dogs) {
            System.out.println("People are greater than or equal to dogs.");
        }
//        if people is less than or equal to dogs show in the console what is within the ""'s
        if (people <= dogs) {
            System.out.println("People are less than or equal to dogs.");
        }
//        if people absolutely equal show in the console what is within the ""'s
        if (people == dogs) {
            System.out.println("People are dogs.");
        }
    }
}
