/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springvendingmachinemvc;

import com.tsg.springvendingmachinemvc.dao.VendingMachineDAO;
import com.tsg.springvendingmachinemvc.dto.Beer;
import com.tsg.springvendingmachinemvc.dto.Change;
import java.text.NumberFormat;
import java.util.ArrayList;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
@Controller
public class VendingMachineHomeController {

    NumberFormat defaultFormat = NumberFormat.getCurrencyInstance();
    private final VendingMachineDAO dao;

    private Change change = new Change();
//    private final String ADMIN_PW = "beermejesus";

    /*
    *Variable message provides error messages and info messages.
     */
    private String message;

    @Inject
    public VendingMachineHomeController(VendingMachineDAO dao) {
        this.dao = dao;
    }

    @RequestMapping(value = {"/", "vendingMachine"}, method = RequestMethod.GET)
    public String displayVendingMachineHomePage(Model model) {
        ArrayList<Beer> beersList = dao.getAllBeers();

        String total = defaultFormat.format(change.getTotal());
        model.addAttribute("beerList", beersList);
        model.addAttribute("change", change);
        model.addAttribute("total", total);
        model.addAttribute("message", message);

        return "vendingMachineHome";
    }

    @RequestMapping(value = {"add5Dollar"}, method = RequestMethod.GET)
    public String addDollar() {
        change.addToTotal(5.0);

        change.restUserChange();

        message = null;

        return "redirect:vendingMachine";
    }

    @RequestMapping(value = {"addDollar"}, method = RequestMethod.GET)
    public String add5Dollar() {
        change.addToTotal(1.0);

        change.restUserChange();

        message = null;

        return "redirect:vendingMachine";
    }

    @RequestMapping(value = {"addQuarter"}, method = RequestMethod.GET)
    public String addQuater() {
        change.addToTotal(0.25);

        change.restUserChange();

        message = null;

        return "redirect:vendingMachine";
    }

    @RequestMapping(value = {"addDime"}, method = RequestMethod.GET)
    public String addDime() {
        change.addToTotal(0.1);

        change.restUserChange();

        message = null;

        return "redirect:vendingMachine";
    }

    @RequestMapping(value = {"addNickel"}, method = RequestMethod.GET)
    public String addNickel() {
        change.addToTotal(0.05);

        change.restUserChange();

        message = null;

        return "redirect:vendingMachine";
    }

    @RequestMapping(value = {"getBeerToVend"}, method = RequestMethod.POST)
    public String vendBeer(HttpServletRequest req) {

        String view = "redirect:vendingMachine";
        String userPosition = req.getParameter("position");

        Beer userBeer = dao.getBeerByItemPosition(userPosition);

        if (userBeer.getQuantity() > 0) {
            if (change.getTotal() >= userBeer.getCost()) {
                dao.sellBeer(userBeer.getItemPosition());
                dispenseChange(userBeer.getCost());
                change.setUserHasChange(change.getTotalInPennies());
                change.getDollarsTotal();
                change.getQuarters();
                change.getDimes();
                change.getNickels();
                change.getPennies();

                

                change.setTotalInPennies(0);

                message = "Here is your change:";

                return "redirect:vendingMachine";

            } else {
                message = "Sorry, you don't have enough money in the machine.";
            }
        } else {
            message = "Sorry, that is temporarily out of stock.";
        }
        return view;
    }

//    @RequestMapping(value = {"vendingMachineChange"}, method = RequestMethod.GET)
//    public String vendAndChange(HttpServletRequest req, Model model) {
//
//        change.getDollarsTotal();
//        change.getQuarters();
//        change.getDimes();
//        change.getNickels();
//        change.getPennies();
//
//        change.setUserHasChange(change.getTotalInPennies());
//
//        change.setTotalInPennies(0);
//
//        message = "Here is your change:";
//
//        return "redirect:vendingMachine";
//
//    }

    private void dispenseChange(double beerCost) {
        change.removeFromTotal(beerCost);
    }
}
