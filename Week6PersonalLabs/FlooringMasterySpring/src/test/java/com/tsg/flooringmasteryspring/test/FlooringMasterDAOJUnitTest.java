/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.test;

import com.tsg.flooringmasteryspring.dao.OrdersDAO;
import com.tsg.flooringmasteryspring.dto.Order;
import com.tsg.flooringmasteryspring.operations.FlooringMasterController;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class FlooringMasterDAOJUnitTest {

    public FlooringMasterDAOJUnitTest() {
    }

    OrdersDAO dao;

    Order o1;
    Order o2;
    Order o3;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationContext.xml");
        dao = ctx.getBean("OrdersDaoImpl", OrdersDAO.class);

        dao.setDate("01012010");
        
        o1 = new Order();
        o1.setOrderDate("01012010");
        o1.setOrderNumber(5);
        o1.setCustomerName("Jack");
        o1.setState("IN");
        o1.setTaxRate(6.5);
        o1.setProductType("Carpet");
        o1.setArea(100.0);
        o1.setLaborCostPerSqFt(5.0);
        o1.setMaterialCostPerSqFt(2.0);
        o1.setTotalCostPerSqFt(10.0);
        o1.setTotalLaborCost(100.0);
        o1.setTotalMaterialCost(200.0);
        o1.setTax(78.0);
        o1.setTotal(2000.0);

        o2 = new Order();
        o2.setOrderDate("01012010");
        o2.setOrderNumber(5);
        o2.setCustomerName("Bill");
        o2.setState("IN");
        o2.setTaxRate(6.5);
        o2.setProductType("Carpet");
        o2.setArea(100.0);
        o2.setLaborCostPerSqFt(5.0);
        o2.setMaterialCostPerSqFt(2.0);
        o2.setTotalCostPerSqFt(10.0);
        o2.setTotalLaborCost(100.0);
        o2.setTotalMaterialCost(200.0);
        o2.setTax(78.0);
        o2.setTotal(2000.0);

        o3 = new Order();
        o3.setOrderDate("01012010");
        o3.setOrderNumber(5);
        o3.setCustomerName("Ralph");
        o3.setState("IN");
        o3.setTaxRate(6.5);
        o3.setProductType("Carpet");
        o3.setArea(100.0);
        o3.setLaborCostPerSqFt(5.0);
        o3.setMaterialCostPerSqFt(2.0);
        o3.setTotalCostPerSqFt(10.0);
        o3.setTotalLaborCost(100.0);
        o3.setTotalMaterialCost(200.0);
        o3.setTax(78.0);
        o3.setTotal(2000.0);
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test //Testing to see if the order number for an order is correct
    public void createGetOrderTest() {

        //arrange
        dao.createOrder(o1);

        //act
        Order result = dao.getOrderByOrderNumber(o1.getOrderNumber());

        //assert
        Assert.assertEquals(o1, result);
    }

    @Test //Testing to see if the order number for an order is correct
    public void createGetOrderTest2() {

        //arrange
        dao.createOrder(o2);

        //act
        Order result = dao.getOrderByOrderNumber(o2.getOrderNumber());

        //assert
        Assert.assertEquals(o2, result);
    }

    @Test //Getting an arrayList of orders
    public void createGetOrdersTest() {

        //arrange
        dao.createOrder(o1);
//        dao.createOrder(o2);
//        dao.createOrder(o3);

        //act
        Order result = dao.getOrderByOrderNumber(o1.getOrderNumber());

        //assert
        Assert.assertEquals(o1, result);
    }

    @Test //Editing an order
    public void createEditOrdersTest() {

        //arrange
        dao.createOrder(o1);
        dao.createOrder(o2);

        //act
        String name = "Nate";
        o1.setCustomerName(name);

        //assert
        Assert.assertEquals(o1.getCustomerName(), name);
    }

    
    @Test //Delete an order
    public void createDeleteOrdersTest() {

        //arrange
        dao.createOrder(o1);

        //act
        dao.deleteOrder(o1.getOrderNumber());

        //assert
        Assert.assertEquals(dao.getOrderByOrderNumber(o1.getOrderNumber()), null);
    }

    
    @Test //Saving and loading an orders
    public void saveLoadOrdersTest() {

        //arrange
        dao.createOrder(o1);
        dao.createOrder(o2);
        dao.createOrder(o3);
        
        OrdersDAO readDAO = new OrdersDAO();
        readDAO.setDate("01012010");
        
        dao.save("junit_hates_us.txt");
        
        readDAO.load("junit_hates_us.txt");
     
        //assert
        Order readOrder1 = readDAO.getOrderByOrderNumber(o1.getOrderNumber());
        Order readOrder2 = readDAO.getOrderByOrderNumber(o2.getOrderNumber());
        Order readOrder3 = readDAO.getOrderByOrderNumber(o3.getOrderNumber());
        
        Assert.assertEquals(o1, readOrder1);
        Assert.assertEquals(o2, readOrder2);
        Assert.assertEquals(o3, readOrder3);
    }

}
