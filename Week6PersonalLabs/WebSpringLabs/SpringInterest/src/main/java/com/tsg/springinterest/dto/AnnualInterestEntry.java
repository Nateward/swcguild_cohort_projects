/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springinterest.dto;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AnnualInterestEntry {

    private int year = 0;
    private float interestEarned = 0.0f;
    private float money = 0.0f;


    /**
     * @return the interestEarned
     */
    public float getInterestEarned() {
        return interestEarned;
    }

    /**
     * @param interestEarned the interestEarned to set
     */
    public void setInterestEarned(float interestEarned) {
        this.interestEarned = interestEarned;
    }

    /**
     * @return the money
     */
    public float getMoney() {
        return money;
    }

    /**
     * @param money the money to set
     */
    public void setMoney(float money) {
        this.money = money;
    }


    /**
     * @return the years
     */
    public int getYear() {
        return year;
    }

    /**
     * @param years the years to set
     */
    public void setYear(int year) {
        this.year = year;
    }

    
    
}
