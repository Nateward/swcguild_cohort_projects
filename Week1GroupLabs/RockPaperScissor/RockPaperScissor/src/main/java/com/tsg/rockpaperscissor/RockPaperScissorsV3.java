package com.tsg.rockpaperscissor;


import java.util.Random;
import java.util.Scanner;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author apprentice
 */
public class RockPaperScissorsV3 {

    public static void main(String[] args) {
        int rock, paper, scissor, computerChoice, playerChoice, rounds, numOfRound, win, loss, tie, outcome;

        rock = 1;
        paper = 2;
        scissor = 3;
        computerChoice = 0;
        playerChoice = 0;
        numOfRound = 0;
        rounds = 0;
        win = 0;
        loss = 0;
        tie = 0;

        String result = "";

        Scanner keyboard = new Scanner(System.in);

        Random randomGenerator = new Random();

        System.out.println("How many rounds would you like to play (choose between 1--10)? ");
        rounds = keyboard.nextInt();

        if (rounds > 1 && rounds < 10) {
            while (numOfRound < rounds) {
                outcome = gameLogic();

                switch (outcome) {
                    case 1:
                        win++;
                        break;
                    case 2:
                        tie++;
                        break;
                    case 3:
                        loss++;
                        break;
                }

                numOfRound++;

                if (numOfRound == rounds) {
                    System.out.println("\n You played " + numOfRound + " rounds. \n "
                            + "You won " + win + " times. \n You lost " + loss + " times."
                            + " \n We tied " + tie + " times.");

                }

            }
        } else if (rounds == 0) {
            System.out.println("Ok, good-bye.");
        } else {
            System.out.println("Wrong answer! Good-bye.");
        }
    }

    public static int gameLogic() {
        Scanner keyboard = new Scanner(System.in);
        Random randomGenerator = new Random();
        int goofs, usersChoice, computersChoice, outcome;
        goofs = 0;
        outcome = 0;

        System.out.println("Player choose your item: ");
        System.out.println("\t 1) Rock");
        System.out.println("\t 2) Paper");
        System.out.println("\t 3) Scissor\n");

        usersChoice = keyboard.nextInt();

        computersChoice = randomGenerator.nextInt(3) + 1;

        if (usersChoice == 1) {
            switch (computersChoice) {
                case 1:
                    System.out.println("I picked a rock also. That's a tie. ");
                    outcome = 2;
                    break;
                case 2:
                    System.out.println("I picked paper. Get dunked on. Another loss for you.");
                    outcome = 3;
                    break;
                default:
                    System.out.println("I picked scissors. Nerf rock. What a meaningless victory for you.");
                    outcome = 1;
                    break;
            }
        } else if (usersChoice == 2) {
            switch (computersChoice) {
                case 1:
                    System.out.println("I picked a rock. Nerf paper. Enjoy your empty win.");
                    outcome = 1;
                    break;
                case 2:
                    System.out.println("I picked paper too. Real original. I 'guess' it's a tie.");
                    outcome = 2;
                    break;
                default:
                    System.out.println("I picked scissors. Get dunked on. I win again.");
                    outcome = 3;
                    break;
            }
        } else if (usersChoice == 3) {
            switch (computersChoice) {
                case 1:
                    System.out.println("I picked a rock. All skill baby. Way to lose");
                    outcome = 3;
                    break;
                case 2:
                    System.out.println("I picked paper. Way to pick an overpowered class. Enjoy your easy win.");
                    outcome = 1;
                    break;
                default:
                    System.out.println("I picked scissors also. Guess that's a tie.");
                    outcome = 2;
                    break;
            }
        } else {
            System.out.println("You dun goofed");
            goofs++;
        }
        if (goofs > 0) {
            System.out.println("There were " + goofs + "errors on our fault. \n Please contact the louse that gave this to you so they can fix it.");
        }

        return outcome;
    }
}
