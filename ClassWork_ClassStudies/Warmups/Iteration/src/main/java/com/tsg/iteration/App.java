/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.iteration;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.ListIterator;

/**
 *
 * @author ilyagotfryd
 */
public class App {
    public static void main(String[] args) {
        ArrayList<String> football = new ArrayList<>();
        
        String[] listOfTeams = {"Vikings","Packers","Lions","Bears","Browns", "Bengals", "Steelers", "Ravens"};
        
        football.addAll(Arrays.asList(listOfTeams));
        
        for(String n:football)
        {
            System.out.println(n);
        }
        
        ListIterator teams = football.listIterator();
        while(teams.hasNext())
        {
            System.out.println(teams.next());
        }
        
        for(int i=0;i<football.size();i++)
        {
            System.out.println(football.get(i));
        }
        
    }
    
}
