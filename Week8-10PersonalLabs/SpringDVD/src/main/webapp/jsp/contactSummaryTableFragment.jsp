<%-- 
    Document   : contactSummaryTableFragment
    Created on : 7-Apr-2016, 2:08:42 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>

<table id="dvdTable" class="table table-hover">
    <tr>
        <th width="40%">Dvd Name</th>
        <th width="30%">Director</th>
        <th width="15%"></th>
        <th width="15%"></th>
    </tr>
    <tbody id="contentRows"></tbody>
</table>