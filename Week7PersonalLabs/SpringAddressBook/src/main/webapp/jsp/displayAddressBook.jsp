<%-- 
    Document   : home
    Created on : 28-Mar-2016, 1:16:10 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/phone-book.png" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Address Book</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/stats">Stats</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/displayAddressBook">Address Book</a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="container">
            <h1>The Addresses: </h1>
            <a href="displayNewAddressForm">Add Address</a>
            <hr/>

            <c:forEach var="address" items="${addressList}">
                <s:url value="deleteAddress" var="deleteAddress_url">
                    <s:param name="addressId" value="${address.idNumber}"/>
                </s:url>
                <s:url value="diplayEditAddressFrom" var="editAddress_url">
                    <s:param name="addressId" value="${address.idNumber}"/>
                </s:url>
                Name: ${address.firstName} ${address.lastName} |
                <a href="${deleteAddress_url}">Delete</a> |
                <a href="${editAddress_url}">Edit</a><br/>
                Address ${address.streetAddress}<br/>
                ${address.city} ${address.state} ${address.zip}<br/>
                <hr/>
            </c:forEach>
        </div>


        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
