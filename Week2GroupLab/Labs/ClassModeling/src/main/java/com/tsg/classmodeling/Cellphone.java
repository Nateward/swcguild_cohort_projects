/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Cellphone {

    private String color;
    private String type;
    private String size;
    private int costOfManufacturing;

    public Cellphone(String color, String type) {
        this.color = color;
        this.type = type;
      
    }
    
    public String getColor() {
        return this.color;
    }
    
    public void setColor(String color) {
        this.color = color;
    }
    
    public void setType(String type) {
        
        if(type != "i60")
            this.type = type;
        
    }
    
    public String getType() {
        
        return this.type;
    }
    
//    public void makeACall() {
//        
//        
//    }
    
    public int sendAnSMS() {
        
        Random r = new Random();
        int y = r.nextInt(6) + 1;
        int x;
        if(y == 3) {
            System.out.println("SMS sent");
            x = 10;
        }else
        {
            System.out.println("SMS not sent");
            x = 5;
        }
        
        
        return x;
        
    }
    
}
