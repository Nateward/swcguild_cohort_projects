/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springdvd.dao;

import com.tsg.springdvd.dto.Dvd;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class DvdDaoTests {

    private DvdDAO dao;
    private Dvd d1;
    private Dvd d2;
    private Dvd d3;
    private Dvd d4;

    public DvdDaoTests() {
    }

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("dvdDAO", DvdDAO.class);

        JdbcTemplate cleaner = (JdbcTemplate) ctx.getBean("jdbcTemplate");
        cleaner.execute("delete from Dvd");
        
        d1 = new Dvd();
        d1.setTitle("Harry Potter");
        d1.setReleaseDate("2001");
        d1.setMpaaRating("8");
        d1.setDirectorsName("Chris Columbus");
        d1.setStudio("Sony");

        d2 = new Dvd();
        d2.setTitle("Zootopia");
        d2.setReleaseDate("2016");
        d2.setMpaaRating("10");
        d2.setDirectorsName("Byron Howard");
        d2.setStudio("WB");
        d2.setUserNote("Need to watch, looks awesome");

        d3 = new Dvd();
        d3.setTitle("Harry Potter");
        d3.setReleaseDate("2002");
        d3.setMpaaRating("8");
        d3.setDirectorsName("Chris Cholumbus");
        d3.setStudio("MGM");
        d3.setUserNote("two of 8");

        d4 = new Dvd();
        d4.setTitle("Harry Potter");
        d4.setReleaseDate("2003");
        d4.setMpaaRating("8.5");
        d4.setDirectorsName("Chris Cholumbus");
        d4.setStudio("Sony");
        d4.setUserNote("three of 8");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addAndRemoveDVDTest() {
        //Arrange
        dao.Create(d1);
        dao.Create(d2);
        dao.Create(d3);
        //Act

        dao.Delete(d1.getId());
        //assert
        Assert.assertFalse(dao.GetAll().contains(d1));
    }

    @Test
    public void addUpdateDvd() {
        dao.Create(d1);

        d1.setStudio("WB");
        dao.Update(d1.getId(), d1);

        Dvd fromDB = dao.GetById(d1.getId());
        Assert.assertEquals(d1, fromDB);

    }

    @Test
    public void getAllDvds() {
        dao.Create(d1);
        dao.Create(d2);

        List<Dvd> dList = dao.GetAll();
        Assert.assertEquals(2, dList.size());
    }

    @Test
    public void searchContacts() {
        dao.Create(d1);
        dao.Create(d2);
        dao.Create(d3);
        dao.Create(d4);

        Map<SearchTerm, String> criteria = new HashMap<>();

        criteria.put(SearchTerm.TITLE, "Harry Potter");
        List<Dvd> dList = dao.searchDvds(criteria);
        
        Assert.assertEquals(3, dList.size());
        Assert.assertNotEquals(d2, dList.get(1));
        
    }
}
