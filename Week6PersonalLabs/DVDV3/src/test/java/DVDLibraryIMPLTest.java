/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.swcguild.dvdlibrary.dao.DvdLibraryDao;
import com.swcguild.dvdlibrary.dto.Dvd;
import com.tsg.dvdv3.dao.DVDLibraryImpl;
import java.time.LocalDate;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author apprentice
 */
public class DVDLibraryIMPLTest {
    
    DvdLibraryDao dao;

    Dvd dvd1;
    Dvd dvd2;
    Dvd dvd3;
    Dvd dvd4;

    @Before
    public void setUp() {
        ApplicationContext ctx = new ClassPathXmlApplicationContext("applicationcontext.xml");
        dao = ctx.getBean("DvdLibraryDAO", DvdLibraryDao.class);
//        dao = new DVDLibraryImpl();
        dvd1 = new Dvd();
        dvd1.setTitle("Harry Porter");
        dvd1.setReleaseDate(LocalDate.of(2001, 1, 1));
        dvd1.setMpaaRating("8");
        dvd1.setDirector("Chris Cholumbus");
        dvd1.setStudio("Sony");

        dvd2 = new Dvd();
        dvd2.setTitle("Zootopia");
        dvd2.setReleaseDate(LocalDate.of(2016, 1, 1));
        dvd2.setMpaaRating("10");
        dvd2.setDirector("Byron Howard");
        dvd2.setStudio("Sony");
        dvd2.setNote("Need to watch, looks awesome");

        dvd3 = new Dvd();
        dvd3.setTitle("Harry Porter");
        dvd3.setReleaseDate(LocalDate.of(2002, 1, 1));
        dvd3.setMpaaRating("8");
        dvd3.setDirector("Chris Cholumbus");
        dvd3.setStudio("Sony");
        dvd3.setNote("two of 8");

        dvd4 = new Dvd();
        dvd4.setTitle("Harry Porter");
        dvd4.setReleaseDate(LocalDate.of(2003, 1, 1));
        dvd4.setMpaaRating("8.5");
        dvd4.setDirector("Chris Cholumbus");
        dvd4.setStudio("Sony");
        dvd4.setNote("three of 8");
    }

    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    @Test
    public void addAndRemoveDVDTest() {
        //Arrange
        dao.add(dvd1);
        dao.add(dvd2);
        dao.add(dvd3);
        //Act

        dao.remove(dvd1.getId());
        //assert
        Assert.assertFalse(dao.listAll().contains(dvd1));
    }

    @Test
    public void getDVDByTitleTest() {
        //Arrange
        dao.add(dvd1);
        dao.add(dvd2);
        dao.add(dvd3);
        //Act
        Collection<Dvd> dvdList = dao.getByTitle(dvd1.getTitle());
        for (Dvd dvd : dvdList) {
            Assert.assertEquals(dvd1.getTitle(), dvd.getTitle());
        }
        //Assert
        Assert.assertEquals(2, dvdList.size());
        Assert.assertTrue(dvdList.contains(dvd1));
        Assert.assertTrue(dvdList.contains(dvd3));
        Assert.assertFalse(dvdList.contains(dvd2));
    }

    @Test
    public void getDVDByMPAATest() {
        //Arrange
        dao.add(dvd1);
        dao.add(dvd2);
        dao.add(dvd3);
        //Act
        List<Dvd> dvdList = dao.getByRating(dvd1.getMpaaRating());
        for (Dvd dvd : dvdList) {
            Assert.assertEquals(dvd1.getMpaaRating(), dvd.getMpaaRating());
        }
        //Assert
        Assert.assertEquals(2, dvdList.size());
    }
//
//    @Test
//    public void getDVDByDirectorSortedByMPAARatingsTest() {
//        //Arrange
//        dao.add(dvd1);
//        dao.add(dvd2);
//        dao.add(dvd3);
//        dao.add(dvd4);
//
//        //Act
//        Map<String, List<Dvd>> dvdMap = dao.(dvd1.getDirector());
//        List<Dvd> listWithMPAARatings8 = dvdMap.get(dvd1.getMpaaRating());
//
//        Assert.assertEquals(2, dvdMap.size());
//        Assert.assertTrue(listWithMPAARatings8.contains(dvd1));
//        Assert.assertFalse(listWithMPAARatings8.contains(dvd4));
//        Assert.assertFalse(dvdMap.keySet().contains(dvd2));
//
//    }
//
//    @Test
//    public void getNewestDVD() {
//        //Arrange
//        dao.add(dvd1);
//        dao.add(dvd2);
//        dao.add(dvd3);
//        dao.add(dvd4);
//        //Act
//        Collection<Dvd> newestDVDCollection = dao.getNewestDVD();
//        //Assert
//        Assert.assertEquals(1, newestDVDCollection.size());
//        Assert.assertFalse(newestDVDCollection.contains(dvd1));
//        Assert.assertTrue(newestDVDCollection.contains(dvd2));
//
//    }

//    @Test
//    public void getOldestDVD() {
//        //Arrange
//        dao.add(dvd1);
//        dao.add(dvd2);
//        dao.add(dvd3);
//        dao.add(dvd4);
//        //Act
//        Collection<Dvd> oldestDVDCollection = dao.getOldestDVD();
//        //Assert
//        Assert.assertEquals(1, oldestDVDCollection.size());
//        Assert.assertFalse(oldestDVDCollection.contains(dvd2));
//        Assert.assertTrue(oldestDVDCollection.contains(dvd1));
//
//    }
//
//    @Test  //Testing for the average number of notes
//    public void getAverageNumOfNotes() {
//        //Arrange
//        dao.add(dvd1);
//        dao.add(dvd2);
//        dao.add(dvd3);
//        dao.add(dvd4);
//        //Act
//        double average = dao.getAverageNoOfNotesAssociate();
//        double testAverage = 3.0 / 4.0;
//        //Assert
//        Assert.assertEquals(testAverage, average, 0);
//
//    }

}
