package com.tsg.randomness;

import java.util.Random;

public class Randomness
{
	public static void main ( String[] args )
	{
		Random r = new Random();
//                Setting the seed to a given # only allows the random generator to pick a number from 0 - to number x
		int x = r.nextInt(10);

		System.out.println( "My random number is " + x );

                /*
                If i delete the 1 + in front of the r.nextInt(5)
                    the random numbers generated are from 0-4.
                
                If i add the 3 + in front of the r.nextInt(5)
                    the random numbers generated are from 2-6.
                
                */
                
		System.out.println( "Here are some numbers from 1 to 5!" );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.print(1 + r.nextInt(5) + " " );
		System.out.println();

		System.out.println( "Here are some numbers from 1 to 100!" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.print( r.nextInt(100) + "\t" );
		System.out.println();

		int num1 = r.nextInt(10);
		int num2 = r.nextInt(10);

		if ( num1 == num2 )
		{
			System.out.println( "The random numbers were the same! Weird." );
		}
		if ( num1 != num2 )
		{
			System.out.println( "The random numbers were different! Not too surprising, actually." );
		}
	}
}