/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedliststack;

import java.util.Iterator;

/**
 *
 * @author apprentice
 */
public class LinkedListStackImpl implements LinkedListStackInterface{
    private Node first;
    private Node last;
    private int numItems=0;

    @Override
    public void push(Object item) {
        Node previousLast = last;
        last = new Node();
        last.item = item;
        if (isEmpty()) {
            first = last;
        }else{
            previousLast.next = last;
        }
        numItems++;
    }

    @Override
    public Object pop() {
        //condition if no of item is 0 and 1 is yet to be writter.
        Object objectToPop;
        if (numItems==0) {
            return null;
        }
        if(numItems==1){
            objectToPop = first.item;
            last = first;
        }else{
        Node secondLastNode = getSecondLastNode();
        objectToPop = secondLastNode.next.item;
        last = secondLastNode;
        }
        numItems--;
        return objectToPop;
    }
    
    private Node getSecondLastNode(){
        Node node = first;
       
        for (int i = 0; i < numItems-2; i++) {//secondlast node needs numItems -2 iteration 
            node = node.next;
        }
        return node;
    }

    @Override
    public int size() {
        return numItems;
    }

    @Override
    public boolean isEmpty() {
        return numItems==0;
    }

    @Override
    public Iterator iterator() {
        return new ReverseLinkedlistIterator();
    }
    
    private class Node{
        Object item;
        Node next;
    }
    
    private class ReverseLinkedlistIterator implements Iterator{
        private int i = numItems;
         
            
        @Override
        public boolean hasNext() {
            return i>0;
        }

        @Override
        public Object next() {
           Node current = first;
            Object obj = null;
            for (int j = 0; j < i-1; j++) {
                current = current.next;
            }
            i--;
            return current.item;
        }
        
    }
    
}
