/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.dice;

import java.util.Random;

/**
 *
 * @author apprentice
 */
public class Dice {

    public static void main(String[] args) {

        Random randomGenerator = new Random();

        int die1 = randomGenerator.nextInt(6) + 1;
        int die2 = randomGenerator.nextInt(6) + 1;
        int dieTotal = (die1 + die2);
        
        System.out.println("Here comes the Dice!");
        
        System.out.println("Roll #1: " + die1);
        System.out.println("Roll #2: " + die2);
        System.out.println("The total is " + dieTotal);

    }

}
