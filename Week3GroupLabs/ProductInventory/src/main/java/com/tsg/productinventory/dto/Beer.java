/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.productinventory.dto;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class Beer extends Product {

    private double abv;

    /**
     * @return the abv
     */
    public double getAbv() {
        return abv;
    }

    /**
     * @param abv the abv to set
     */
    public void setAbv(double abv) {
        this.abv = abv;
    }
}
