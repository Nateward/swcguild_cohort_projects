/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bankaccountmanager.dto;

/**
 *
 * @author apprentice
 */
public class Account {

    protected Double availableBalance = 0d;
    protected Double totalBalance = 0d;
    protected Integer pin = 9876;

    //junit -> create, deposit 500
    // withdraw 450, total 50
    // withdraw 550, total 500
    public void withdraw(Double withdrawAmount) {
        if (withdrawAmount > 0) {
            if (withdrawAmount <= availableBalance) {
                availableBalance = availableBalance - withdrawAmount;
                totalBalance = totalBalance - withdrawAmount;

            } else {
                System.out.println("Not enough funds.");
            }
        } else {
            System.out.println("You must enter a positive number.");
        }

        System.out.println("You have " + this.availableBalance + " available.");
        System.out.println("You have " + this.totalBalance + " total.");
    }

    //JUnit test-> create account
    // deposit 500, getbalance = 500, gettotal = 500
    // deposit -500, getbalance = 0, gettotal = 0
    // deposit 20000, getbalance = 0, totalbalance = 20000
    public void deposit(Double depositAmount) {
        if (depositAmount > 0) {
            if (depositAmount <= 10000) {
                availableBalance = availableBalance + depositAmount;
                totalBalance = totalBalance + depositAmount;
                System.out.println("Your deposit has been received.");
            } else {
                totalBalance = totalBalance + depositAmount;
                System.out.println("This deposit will be available after manager review.");
            }
        } else {
            System.out.println("You must enter a positive number.");
        }

        System.out.println("Your available balance is $" + this.availableBalance);
        System.out.println("Your total bank balance is $" + this.totalBalance + "\n");
    }

    /**
     * @return the pin
     */
    public Integer getPin() {
        return pin;
    }

    /**
     * @param pin the pin to set
     */
    public void setPin(Integer pin) {
        this.pin = pin;
    }

    /**
     * @return the availableBalance
     */
    public Double getAvailableBalance() {
        return availableBalance;
    }

    /**
     * @return the totalBalance
     */
    public Double getTotalBalance() {
        return totalBalance;
    }

}
