/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook.dao;

import com.tsg.springaddressbook.dto.Address;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AddressBookTests {
    
    public AddressBookTests() {
    }
    
    AddressBookDAO dao;
    Address A1;
    Address A2;
    Address A3;
    Address A4;
    
    @Before
    public void setUp() {
        
        ApplicationContext ctx = new ClassPathXmlApplicationContext("test-applicationContext.xml");
        dao = ctx.getBean("addressDAO", AddressBookDAO.class);
        
        A1 = new Address();
        A1.setFirstName("Nate");
        A1.setLastName("Ward");
        A1.setStreetAddress("110 Greenwich Rd.");
        A1.setCity("Medina");
        A1.setState("Ohio");
        A1.setZip(String.valueOf(44273));

        A2 = new Address();
        A2.setFirstName("John");
        A2.setLastName("Ward");
        A2.setStreetAddress("115 Greenwich Rd.");
        A2.setCity("Seville");
        A2.setState("Ohio");
        A2.setZip(String.valueOf(44254));

        A3 = new Address();
        A3.setFirstName("Jane");
        A3.setLastName("Smith");
        A3.setStreetAddress("105 Greenwich Rd.");
        A3.setCity("Seville");
        A3.setState("Ohio");
        A3.setZip(String.valueOf(44273));

        A4 = new Address();
        A4.setFirstName("Bill");
        A4.setLastName("Smithfield");
        A4.setStreetAddress("105 Greenwich Rd.");
        A4.setCity("Seville");
        A4.setState("Maine");
        A4.setZip(String.valueOf(44256));
        
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void addAndRemoveAddressTest() {
        //Arrange
        dao.Create(A1);
        dao.Create(A2);
        dao.Create(A3);
        //Act

        dao.Delete(A1.getIdNumber());
        //assert
        Assert.assertFalse(dao.GetAll().contains(A1));
    }

    @Test
    public void addUpdateAddress() {
        dao.Create(A1);

        A1.setFirstName("Bill");
        dao.Update(A1.getIdNumber(), A1);

        Address fromDB = dao.GetById(A1.getIdNumber());
        Assert.assertEquals(A1, fromDB);

    }

    @Test
    public void getAllAddresses() {
        dao.Create(A1);
        dao.Create(A2);

        List<Address> aList = dao.GetAll();
        Assert.assertEquals(2, aList.size());
    }

    @Test
    public void searchContacts() {
        dao.Create(A1);
        dao.Create(A2);
        dao.Create(A3);
        dao.Create(A4);

        Map<SearchTerm, String> criteria = new HashMap<>();

        criteria.put(SearchTerm.LASTNAME, "Ward");
        List<Address> aList = dao.searchContacts(criteria);
        
        Assert.assertEquals(2, aList.size());
        Assert.assertEquals(A2, aList.get(1));
        
    }
}
