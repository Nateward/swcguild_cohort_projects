/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.junitdrills;

import com.tsg.junitdrills.FirstLast6;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class FirstLast6JunitTest {
    
    public FirstLast6JunitTest() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
//    
    
    
//    FirstLast6({1, 2, 6}) -> true
    
    @Test
    public void lastIndexIs6()
    {
    FirstLast6 test = new FirstLast6();
    int[] array = {1, 2, 6};
    assertTrue(test.firstLastSix(array));
    }
//FirstLast6({6, 1, 2, 3}) -> true
    
    
    @Test
    public void firstIndexIs6()
    {
    FirstLast6 test = new FirstLast6();
    int[] array = {6, 1, 2, 3};
    assertTrue(test.firstLastSix(array));
    }
//FirstLast6({13, 6, 1, 2, 3}) -> false
    
    @Test
    public void middleIndexIs6()
    {
    FirstLast6 test = new FirstLast6();
    int[] array = {13, 6, 1, 2, 3};
    assertFalse(test.firstLastSix(array));
    }
}
