/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.bankaccountmanager;

import com.tsg.bankaccountmanager.dto.Account;
import com.tsg.bankaccountmanager.dto.Checking;
import com.tsg.bankaccountmanager.dto.Savings;
import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class AccountManagerController {

    Account aAccount = new Account();
    Checking checking = new Checking();
    Savings savings = new Savings();
    Scanner sc = new Scanner(System.in);
    Double userAmount;
    Integer userInput = 0;
    Integer numberOfTries = 0;
    Integer timesTried = 3;

    public void logic() {
        while (!userInput.equals(aAccount.getPin()) && numberOfTries < 3) {
            System.out.println("Please enter your PIN # to view your accounts: ");
            userInput = sc.nextInt();
            if (userInput.equals(aAccount.getPin())) {
                System.out.println("\nLoading account info...\n\n");
            } else {
                System.out.println("\nWrong PIN #.\n");
                numberOfTries++;
                timesTried--;
                System.out.println("Number of tries left: " + timesTried);
            }
        }

        if (userInput.equals(aAccount.getPin())) {

            while (userInput != 3) {
                printMenu();
                userInput = sc.nextInt();
                switch (userInput) {
                    case 1:
                        saving();
                        break;
                    case 2:
                        checking();
                        break;
                    default:
                        break;
                }
            }
        }
    }

    public void printMenu() {
        System.out.println("Please choose an account: ");
        System.out.println("1.) Saving");
        System.out.println("2.) Checking");
        System.out.println("3.) Exit\n");
    }

    public void checking() {

        checking.getAvailableBalance();
        checking.getTotalBalance();
        System.out.println("");
        printAccountOptions();
        userInput = sc.nextInt();

        switch (userInput) {
            case 1:
                System.out.println("\nThere is a $10 penalty charge for overdrafts from this account type.");
                System.out.println("Withdrawal Amount: ");
                checking.withdraw(sc.nextDouble());
                break;
            case 2:
                System.out.println("Deposit Amount: ");
                checking.deposit(sc.nextDouble());
                break;
            default:
                break;
        }
    }

    public void printAccountOptions() {
        System.out.println("Please choose from the following options: ");
        System.out.println("1.) Withdraw from account.");
        System.out.println("2.) Deposit money into acccount.");
        System.out.println("3.) Exit.\n");
    }

    private void saving() {
        savings.getAvailableBalance();
        savings.getTotalBalance();
        System.out.println("");
        printAccountOptions();
        userInput = sc.nextInt();

        switch (userInput) {
            case 1:
                System.out.println("\nThere is a $50 penalty charge for withdrawals from this account type.");
                System.out.println("Withdrawal Amount: ");
                savings.withdraw(sc.nextDouble());
                break;
            case 2:
                System.out.println("Deposit Amount: ");
                savings.deposit(sc.nextDouble());
                break;
            default:
                break;
        }
    }
}
