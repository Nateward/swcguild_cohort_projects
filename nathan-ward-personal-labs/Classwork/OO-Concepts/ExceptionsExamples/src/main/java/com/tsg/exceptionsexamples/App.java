/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.exceptionsexamples;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author apprentice
 */
public class App {

    public static void main(String[] args) {
        CheckedExceptionExample example = new CheckedExceptionExample();

        try {
            int value = example.SillyCheckedException(32);
            System.out.println("We expect to execute this line just fine.");
            System.out.println("As well as this line");
            System.out.println("we were echoing " + value);
        } catch (Exception ex) {
            System.out.println("This code should not execute.");
            Logger.getLogger(App.class.getName()).log(Level.SEVERE, null, ex);
        }
        try {
            int value = example.SillyCheckedException(66);
            System.out.println("This code should not execute.");
            System.out.println("As well as this line");
        } catch (Exception ex) {
            System.out.println("Exceptions has been recieved: " + ex.getMessage());
        }
        
        System.out.println("All is well that ends well.");
    }

}
