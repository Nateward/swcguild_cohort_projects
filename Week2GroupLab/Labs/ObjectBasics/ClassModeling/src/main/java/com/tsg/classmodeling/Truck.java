/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Truck {
    
    private String truckType;
    private String make;
    private String model;
    private int length;
    private int width;
    private int height;

    public Truck(String truckType, String make, String model, int length, int width, int height) {
        this.truckType = truckType;
        this.make = make;
        this.model = model;
        this.length = length;
        this.width = width;
        this.height = height;
    }

    public String getTruckType() {
        return truckType;
    }

    public void setTruckType(String truckType) {
        this.truckType = truckType;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }
          
    //Behaviors
    
    /**
     * these behaviors lets you know how bigger my truck is
     */
    
    public void size()
    {
        System.out.println("The full size truck carries heavier loads");
    }
}
