/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.linkedlistqueue.test;

import com.tsg.linkedlistqueue.LinkedListQueueImpl;
import java.util.Iterator;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author apprentice
 */
public class LinkedListQueueImplTest {
    
    public LinkedListQueueImplTest() {
    }
    LinkedListQueueImpl queue;
    @Before
    public void setUp() {
        queue= new LinkedListQueueImpl();
    }
    
    @After
    public void tearDown() {
    }

    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
    
    @Test
    public void enqueSimpleExample(){
        //Arrange
        String a = "a";
        //Act
        queue.enqueue(a);
        int size = queue.size();
        
        //Assert
        assertEquals(1, size);
        
    }
    
    //test from ilya's files
    @Test
    public void enqueueDequeueOneElement()
    {
        queue.enqueue(20);
        Integer expected = 20;
        //Assert.assertEquals(expected, queue.peek());
        assertEquals(1, queue.size());
        Object result = queue.dequeue();
        assertEquals(expected, result);
        assertEquals(0, queue.size());
    }
    
    @Test
    public void fifoIsFunctional()
    {
        queue.enqueue(10);
        queue.enqueue(20);
        queue.enqueue(30);
        queue.enqueue(42);
        assertEquals((Integer)10, queue.dequeue());
        assertEquals((Integer)20, queue.dequeue());
        assertEquals(2, queue.size());
        assertEquals((Integer)30, queue.dequeue());
        assertEquals((Integer)42, queue.dequeue());
    }
    
    @Test
    public void fifoCanEmptyAndRefill()
    {
        queue.enqueue(11);
        queue.enqueue(12);
        queue.enqueue(13);
        queue.enqueue(14);
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        queue.dequeue();
        
        queue.enqueue(10);
        queue.enqueue(20);
        queue.enqueue(30);
        queue.enqueue(42);
        assertEquals((Integer)10, queue.dequeue());
        Assert.assertEquals((Integer)20, queue.dequeue());
        Assert.assertEquals(2, queue.size());
        Assert.assertEquals((Integer)30, queue.dequeue());
        Assert.assertEquals((Integer)42, queue.dequeue());
    }
    @Test
    public void fifoCanPartiallyEmptyAndRefill()
    {
        queue.enqueue(11);
        queue.enqueue(12);
        queue.enqueue(10);
        queue.enqueue(20);
        queue.dequeue();
        queue.dequeue();
 
        queue.enqueue(30);
        queue.enqueue(42);
        
        Assert.assertEquals((Integer)10, queue.dequeue());
        Assert.assertEquals((Integer)20, queue.dequeue());
        Assert.assertEquals(2, queue.size());
        Assert.assertEquals((Integer)30, queue.dequeue());
        Assert.assertEquals((Integer)42, queue.dequeue());
    }
    
    @Test
    public void iteratorTest() {
        //Arrange
        String a = "a";
        String b = "b";
        String c = "c";
        String d = "d";
        String e = "e";
        //Act
        queue.enqueue(a);
        queue.enqueue(b);
        queue.enqueue(c);
        queue.enqueue(d);
        queue.enqueue(e);
        
        Iterator itr = queue.iterator();
        Object firstOut = itr.next();
        Object secondOut = itr.next();
        Object thirdOut = itr.next();
        Object fourthOut = itr.next();
        boolean hasNext1 = itr.hasNext();
        Object lastOut = itr.next();
        boolean hasNext2 = itr.hasNext();

        //assert
        assertEquals(a, firstOut);
        assertEquals(b, secondOut);
        assertTrue(hasNext1);
        assertEquals(d, fourthOut);
        assertEquals(e, lastOut);
        assertFalse(hasNext2);

    }
    
}
