/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook.dao;

import com.tsg.springaddressbook.dto.Address;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public interface AddressBookDAO {

    //GetAll
    public List<Address> GetAll();

    //GetById
    public Address GetById(int id);

    //Create
    public Address Create(Address dto);

    //Update
    public void Update(int id, Address dto);

    //Delete
    public void Delete(int id);

    //Search
    public List<Address> searchContacts(Map<SearchTerm, String> criteria);

}
