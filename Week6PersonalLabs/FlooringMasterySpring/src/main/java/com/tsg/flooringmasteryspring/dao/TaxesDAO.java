/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.flooringmasteryspring.dao;

import com.tsg.flooringmasteryspring.dto.Taxes;
import com.tsg.flooringmasteryspring.interfaces.TaxesInterface;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Adam Coate <adamcoate1@gmail.com>
 */
public class TaxesDAO implements TaxesInterface{
    
    HashMap<String, Double> taxRateList = new HashMap<>();
    private final String TAXES_DELIMETER = ",";
    private final String TAXES_FILE = "stateTax.txt";
    
    public TaxesDAO(){
        this.loadTaxRateList();
    }
    
    @Override
    public Double getTaxRate(String state) {
        return taxRateList.get(state);
    }
    
    @Override
    public void loadTaxRateList() {
        try {
            Scanner sc = new Scanner(new BufferedReader(new FileReader(TAXES_FILE)));
            
            while (sc.hasNext()) {
                String currentLine = sc.next();
                String[] tokens = currentLine.split(TAXES_DELIMETER);
                
                Taxes taxes = new Taxes();
                
                taxes.setStateName(tokens[0]);
                taxes.setTaxRate(Double.parseDouble(tokens[1]));
                
                taxRateList.put(taxes.getStateName(), taxes.getTaxRate());
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(TaxesDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    
    @Override
    public List<String> getStateList() {
        return new ArrayList(taxRateList.keySet());
    }
    
}
