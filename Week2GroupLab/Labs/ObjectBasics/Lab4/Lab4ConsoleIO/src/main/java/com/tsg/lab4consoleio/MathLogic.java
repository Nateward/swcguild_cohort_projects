/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.lab4consoleio;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MathLogic {
    
    public void calculatorLogic(){
        Scanner keyboard = new Scanner(System.in);
        float firstNumber = 0.0f;
        float secondNumber = 0.0f;
        int userInput = 0;
        float result;
        

        System.out.println("Welcome to console calculator.");

        while (userInput != 5) {

            System.out.println("\n Please choose what operation you would like to do:");
            System.out.println("\t 1.) Add\n\t 2.) Substract\n\t 3.) Multiply\n\t 4.) Divide\n\t 5.) Exit ");
            userInput = keyboard.nextInt();

            switch (userInput) {
                case 1:
                    System.out.println("\n Whats the first number you would like to use in the calculator?");
                    firstNumber = keyboard.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = keyboard.nextFloat();
                    result = add(firstNumber, secondNumber);
                    break;
                case 2:
                    System.out.println("\n Whats the first number you would like to use?");
                    firstNumber = keyboard.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = keyboard.nextFloat();
                    result = substract(firstNumber, secondNumber);
                    break;
                case 3:
                    System.out.println("\n Whats the first number you would like to use?");
                    firstNumber = keyboard.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = keyboard.nextFloat();
                    result = multiply(firstNumber, secondNumber);
                    break;

                case 4:
                    System.out.println("\n Whats the first number you would like to use?");
                    firstNumber = keyboard.nextFloat();

                    System.out.println(" Please choose another number: ");
                    secondNumber = keyboard.nextFloat();
                    result = divide(firstNumber, secondNumber);
                    break;
                case 5:
                    System.out.println("Thank you!");
                    break;
                default:
                    System.out.println("You picked something not in the list");
                    break;

            }
        }
    }

    public float add(float firstNumber, float secondNumber) {
        System.out.printf(firstNumber + " + " + secondNumber + " = %s", (firstNumber + secondNumber));
        return firstNumber + secondNumber;
    }

    public float substract(float firstNumber, float secondNumber) {
        System.out.printf(firstNumber + " - " + secondNumber + " = %s", (firstNumber - secondNumber));
        return firstNumber - secondNumber;
    }

    public float multiply(float firstNumber, float secondNumber) {
        System.out.printf(firstNumber + " * " + secondNumber + " = %s", (firstNumber * secondNumber));
        return firstNumber * secondNumber;
    }

    public float divide(float firstNumber, float secondNumber) {
        System.out.printf(firstNumber + " / " + secondNumber + " = %s", (firstNumber / secondNumber));
        return firstNumber / secondNumber;
    }
    
    
}
