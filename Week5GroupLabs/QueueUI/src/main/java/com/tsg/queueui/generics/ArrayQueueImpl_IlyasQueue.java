/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.queueui.generics;

import com.thesoftwareguild.queue.Queue;
import java.util.Iterator;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class ArrayQueueImpl_IlyasQueue<Item> implements Queue<Item> {

    private static final int DEFAULT_INITIAL_SIZE = 4;
    private Item[] items;
    private int numItems;
    private int head = 0;
    private int tail = 0;

    public ArrayQueueImpl_IlyasQueue() {
        this(DEFAULT_INITIAL_SIZE);
    }

    public ArrayQueueImpl_IlyasQueue(int size) {
        items = (Item[]) new Object[size];
    }

    @Override
    public void enqueue(Item o) {
        items[tail] = o;

        numItems++;

        if (tail <= (items.length - 1)) {
            tail++;
            if (tail == items.length) {
                reSize(items.length * 2);
                //position of head and tail now?
            }
        } else {
            tail = 0;
        }
    }

    @Override
    public Item dequeue() {
        if (numItems == 0) {
            return null;
        }

        numItems--;
        Item obj = items[head];

        items[head] = null;

        if (head < (items.length - 1)) {
            head++;
        } else {
            head = 0;
        }
        if (numItems > 0 && ((numItems + 1) <= items.length / 2)) {
            reSize(items.length / 2);
        }

        return obj;
    }

    @Override
    public int size() {
        return numItems;
    }

    @Override
    public Boolean isEmpty() {
        return numItems == 0;
    }

    private void reSize(int newSize) {
        Item[] temp = (Item[]) new Object[newSize];
        for (int i = 0; i < numItems; i++) {

            if (i == 0) {
                temp[i] = items[head];
            } else if ((head + i) < items.length) {
                temp[i] = items[head + i];
            } else {
                temp[i] = temp[0 + (i - 1)];
            }
//            if (items[i] != null) {
//                temp[i] = items[i];
//            } else {
//                temp[i] = items[head];
//                head++;
//                if (temp.length == (head - 1)) {
//                    tail = head + 1;
//                }
//            }

        }
        head = 0;
        tail = numItems;
        items = temp;

    }

    @Override
    public Iterator iterator() {
        return new ArrayIterator();

    }

    @Override
    public Item peek() {
        return items[head];
    }

    @Override
    public String getAuthorName() {
        String names = "Pankaj Neupane & Nathan Ward";
        return names;
    }

    public void printQueue() {
        
        for (Item n : items) {
            
            if (n == null) {
                
                System.out.print("[__]");
                
            } else {
                
                System.out.print("[" + n + "]");
                
            }
            
        }
        
        System.out.println();
        
    }

    private class ArrayIterator implements Iterator {

        private int i = numItems;
        private int x = head;

        @Override
        public boolean hasNext() {
            return i > 0;
        }

        @Override
        public Item next() {
            Item returnObject = items[x];
            x++;
            if (x >= items.length) {
                x = 0;
            }
            i--;
            return returnObject;
        }

    }
}
