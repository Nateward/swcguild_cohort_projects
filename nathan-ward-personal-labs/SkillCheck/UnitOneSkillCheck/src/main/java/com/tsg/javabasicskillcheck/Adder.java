/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.javabasicskillcheck;

/**
 *
 * @author apprentice
 */
public class Adder {

    public static void main(String[] args) {

        int sum = 0;

        sum = add(1, 1);
        System.out.println(sum);

        sum = add(2, 3);
        System.out.println(sum);

        sum = add(5, 8);
        System.out.println(sum);

        sum = add(95, 42);
        System.out.println(sum);

    }

    public static int add(int a, int b) {
        
        return a + b;
    }

}
