/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.collatzsequence;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class collatzSequence {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);

        int n;
        int highest = 0;
        int counter = 0;

        System.out.println("Please enter a number for the sequence: ");
        n = sc.nextInt();
        System.out.print(n + "\t");
        while (n != 1) {

            for (int i = 0; i < 9; i++) {
                if (n != 1) {

                    if (n % 2 == 0) {
                        n = n / 2;
                    } else {
                        n = (n * 3) + 1;
                    }
                    System.out.print(n + "\t");
                    counter++;
                    if (n > highest) {
                        highest = n;
                    }
                }
            }
            System.out.println("");

        }
        System.out.println("Highest number found was: " + highest + " and number of tries it took for the sequence to end was: " + (counter + 1));
    }
}
