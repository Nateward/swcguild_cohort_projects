<%-- 
    Document   : home
    Created on : 28-Mar-2016, 1:16:10 PM
    Author     : Nathan Ward <nateward.nw@gmail.com>
--%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="s" uri="http://www.springframework.org/tags"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Company Contacts</title>
        <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet">
        <link href="${pageContext.request.contextPath}/img/dvdvideo.jpg" rel="Shortcut icon">
    </head>
    <body>
        <div class="container">
            <h1>Dvd Library</h1>
            <hr/>
            <div class="navbar">
                <ul class="nav nav-tabs">
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/home">Home</a>
                    </li>
                    <li role="presentation">
                        <a href="${pageContext.request.contextPath}/search">Search</a>
                    </li>
                    <li role="presentation" class="active">
                        <a href="${pageContext.request.contextPath}/displayDvdsList">Dvd LibraryList</a>
                    </li>
                </ul>
            </div>
        </div>
                    <div class="container">
                        <h1>Dvd Library: </h1>
                        <a href="displayNewDvdForm">Add Dvd</a>
                        <hr/>
                        
                        <c:forEach var="dvd" items="${dvdList}">
                            <s:url value="deleteDvd" var="deleteDvd_url">
                                <s:param name="dvdId" value="${dvd.id}"/>
                            </s:url>
                            <s:url value="diplayEditDvdFrom" var="editDvd_url">
                                <s:param name="dvdId" value="${dvd.id}"/>
                            </s:url>
                            Title: ${dvd.title} |
                            <a href="${deleteDvd_url}">Delete</a> |
                            <a href="${editDvd_url}">Edit</a><br/>
                            Release Date: ${dvd.releaseDate}<br/>
                            Rating: ${dvd.mpaaRating}<br/>
                            Director: ${dvd.directorsName}<br/>
                            Studio: ${dvd.studio}<br/>
                            Notes: ${dvd.userNote}<br/>
                            <hr/>
                        </c:forEach>
                    </div>
                    
                    
        <script src="${pageContext.request.contextPath}/js/jquery-1.11.3.min.js"></script>
        <script src="${pageContext.request.contextPath}/js/bootstrap.min.js"></script>
    </body>
</html>
