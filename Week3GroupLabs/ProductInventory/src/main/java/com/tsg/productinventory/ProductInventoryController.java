/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.productinventory;

import com.tsg.productinventory.dao.InventoryDAO;
import com.tsg.productinventory.dto.*;
import com.tsg.productinventory.ui.ConsoleIO;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Collection;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class ProductInventoryController {

    private InventoryDAO inventory = new InventoryDAO();
    ConsoleIO con = new ConsoleIO();

    public void run() {

        try {
            inventory.loadInventoryFile();

            int userInput = 0;
            while (userInput != 6) {
                printMenu();
                userInput = con.getInteger("Please choose a menu option.", 1, 6);
                switch (userInput) {
                    case 1:
                        addItem();
                        break;
                    case 2:
                        addQtyToItem();
                        break;
                    case 3:
                        removeQtyFromItem();
                        break;
                    case 4:
                        totalValueOfProduct();
                        break;
                    case 5:
                        totalInventoryValue();
                        break;
                    default:
                        break;
                }

            }
            inventory.writeToInventoryFile();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(ProductInventoryController.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR!!! Could not load inventory list.\n Please contact your IT department.");
        } catch (IOException ex) {
            Logger.getLogger(ProductInventoryController.class.getName()).log(Level.SEVERE, null, ex);
            System.out.println("ERROR!!! Could not write inventory list.\n Please contact your IT department.");
        }

    }

    public void printMenu() {
        con.print("\n1. Add inventory item");
        con.print("2. Add stock quantity to item");
        con.print("3. Remove stock quantity from item");
        con.print("4. Total value on hand of one product");
        con.print("5. List all items with total on hand value");
        con.print("6. Quit");
    }

    public void printAddItemMenu() {
        con.print("\n1. Beer");
        con.print("2. Cigar");
        con.print("3. Glassware");
        con.print("4. Generic product");
        con.print("5. Back to main menu");
    }

    private void addItem() {
        printAddItemMenu();
        int userInput = con.getInteger("Please choose item type to add: ", 1, 5);
        switch (userInput) {
            case 1:
                addItemBeer();
                break;
            case 2:
                addItemCigar();
                break;
            case 3:
                addItemGlassware();
                break;
            case 4:
                addItemProduct();
                break;
            default:
                break;
        }
    }

    private void addQtyToItem() {
        Product product = inventory.getProductBySku(con.getInteger("Please enter item SKU to edit: "));
        if (product != null) {
            printProductInfo(product);
            Integer quantityToAdd = con.getInteger("Please enter quantity to add: ", 0, Integer.MAX_VALUE);
            inventory.addQtyToProduct(product, quantityToAdd);
        } else {
            con.print("Item not found.");
        }
    }

    private void printProductInfo(Product product) {
        con.print("\nProduct type: " + product.getProductType());
        con.print("SKU: " + product.getSku());
        con.print("Name: " + product.getName());
        con.print("Cost: " + product.getCost());
        con.print("Price: " + product.getPrice());
        con.print("Quantity on hand: " + product.getQuantityOnHand());
        con.print("Safety stock: " + product.getSafetyStock());
        if (product instanceof Beer) {
            con.print("ABV: " + ((Beer) product).getAbv());
        } else if (product instanceof Cigar) {
            con.print("Country of origin: " + ((Cigar) product).getCountryOfOrigin());
        } else if (product instanceof Glassware) {
            con.print("Type of glass: " + ((Glassware) product).getType());
        }
    }

    private void removeQtyFromItem() {
        Product product = inventory.getProductBySku(con.getInteger("Please enter item SKU to edit: "));
        if (product != null) {
            printProductInfo(product);
            Integer quantityToAdd = con.getInteger("Please enter quantity to remove: ", 0, Integer.MAX_VALUE);
            if (product.getQuantityOnHand() - quantityToAdd < product.getSafetyStock()) {
                con.print("WARNING: You are below safety stock. Please alert purchasing agent.");
            }
            inventory.remQtyFromProduct(product, quantityToAdd);
        } else {
            con.print("Item not found.");
        }
    }

    private void totalValueOfProduct() {
        Product product = inventory.getProductBySku(con.getInteger("Please enter item SKU: "));
        if (product != null) {
            printProductInfo(product);
            con.print("The total value on hand is: $" + inventory.getTotalValueOfProduct(product.getSku()));
        } else {
            con.print("Item not found.");
        }
    }

    private void totalInventoryValue() {
        Collection<Product> allProducts = inventory.getAllProducts();
        for (Product product : allProducts) {
            printProductInfo(product);
        }
        con.print("\n------------------------------------");
        con.print("Total value of on hand inventory is: $" + inventory.getTotalValueOfInventory());
    }

    private void addItemBeer() {
        Product beer = new Beer();
        beer.setProductType("Beer");
        beer.setSku(con.getInteger("SKU: ", 0, Integer.MAX_VALUE));
        if (inventory.getProductBySku(beer.getSku()) == null) {
            beer.setName(con.getString("Name: "));
            beer.setCost(con.getDouble("Cost per unit: ", 0, Double.MAX_VALUE));
            beer.setPrice(con.getDouble("Price per unit: ", 0, Double.MAX_VALUE));
            beer.setSafetyStock(con.getInteger("Safety stock: ", 0, Integer.MAX_VALUE));
            ((Beer) beer).setAbv(con.getDouble("ABV: ", 0, 15));

            inventory.addProduct(beer);
        } else {
            con.print("Item SKU already exists.");
        }
    }

    private void addItemCigar() {
        Product cigar = new Cigar();
        cigar.setProductType("Cigar");
        cigar.setSku(con.getInteger("SKU: ", 0, Integer.MAX_VALUE));
        if (inventory.getProductBySku(cigar.getSku()) == null) {
            cigar.setName(con.getString("Name: "));
            cigar.setCost(con.getDouble("Cost per unit: ", 0, Double.MAX_VALUE));
            cigar.setPrice(con.getDouble("Price per unit: ", 0, Double.MAX_VALUE));
            cigar.setSafetyStock(con.getInteger("Safety stock: ", 0, Integer.MAX_VALUE));
            ((Cigar) cigar).setCountryOfOrigin(con.getString("Country of origin: "));

            inventory.addProduct(cigar);
        } else {
            con.print("Item SKU already exists.");
        }
    }

    private void addItemGlassware() {
        Product glassware = new Glassware();
        glassware.setProductType("Glassware");
        glassware.setSku(con.getInteger("SKU: ", 0, Integer.MAX_VALUE));
        if (inventory.getProductBySku(glassware.getSku()) == null) {
            glassware.setName(con.getString("Name: "));
            glassware.setCost(con.getDouble("Cost per unit: ", 0, Double.MAX_VALUE));
            glassware.setPrice(con.getDouble("Price per unit: ", 0, Double.MAX_VALUE));
            glassware.setSafetyStock(con.getInteger("Safety stock: ", 0, Integer.MAX_VALUE));
            ((Glassware) glassware).setType(con.getString("Type of glass: "));

            inventory.addProduct(glassware);
        } else {
            con.print("Item SKU already exists.");
        }
    }

    private void addItemProduct() {
        Product product = new Product();
        product.setProductType("Product");
        product.setSku(con.getInteger("SKU: ", 0, Integer.MAX_VALUE));
        if (inventory.getProductBySku(product.getSku()) == null) {
            product.setName(con.getString("Name: "));
            product.setCost(con.getDouble("Cost per unit: ", 0, Double.MAX_VALUE));
            product.setPrice(con.getDouble("Price per unit: ", 0, Double.MAX_VALUE));
            product.setSafetyStock(con.getInteger("Safety stock: ", 0, Integer.MAX_VALUE));

            inventory.addProduct(product);
        } else {
            con.print("Item SKU already exists.");
        }
    }
}
