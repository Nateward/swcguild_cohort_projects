/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.booleanexpressionsandflowcontrol;

/**
 *
 * @author ilyagotfryd
 */
public class ForLoopExample {
    public static void main(String[] args) {
        
        for(int i=0;
                i<10;
                i=i+1)
        {
            System.out.println("counter is " + i);
        }
        
        // i is out of scope and is not known outside of the for loop.
        //System.out.println("counter is still " + i);
    }
    
}
