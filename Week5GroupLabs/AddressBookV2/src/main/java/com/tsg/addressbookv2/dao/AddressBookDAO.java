/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.addressbookv2.dao;

import com.tsg.addressbookv2.dto.Address;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Scanner;

/**
 *
 * @author Scott Stahl <stahl.scott@gmail.com>
 */
public class AddressBookDAO {

    private HashMap<Integer, Address> addresses = new HashMap<>();
    private Integer idIndex;
    private static final String ADDRESS_FILE = "addressbook.txt";
    private final static String DELIMITER = "::";

    public void addAddress(Address address) {

        address.setIdNumber(idIndex);
        addresses.put(idIndex, address);

        this.idIndex++;
    }

    public void deleteAddressbyId(Integer addressId) {
        addresses.remove(addressId);
    }

    public ArrayList<Address> findAddressByLastName(String lastName) {
        Collection<Address> addys = addresses.values();
        ArrayList<Address> addyList = new ArrayList<>();
        for (Address addy : addys) {
            String addyLastName = addy.getLastName();
            if (addyLastName.length() >= lastName.length()) {
                String substring = addy.getLastName().substring(0, lastName.length());
                if (substring.equalsIgnoreCase(lastName)) {
                    addyList.add(addy);
                }
            }
        }

        return addyList;
    }

    public Address findAddressById(Integer addressId) {
        return addresses.get(addressId);
    }

    public ArrayList<Address> findAllAddresses() {
        Collection<Address> addys = addresses.values();
        ArrayList<Address> addyList = new ArrayList<>();
        for (Address addy : addys) {
            addyList.add(addy);
        }

        return addyList;
    }

    public Integer size() {
        return this.addresses.size();
    }

    public void loadAddressBook() throws FileNotFoundException {

        this.idIndex = 1;

        Scanner sc = new Scanner(new BufferedReader(new FileReader(ADDRESS_FILE)));
        while (sc.hasNextLine()) {
            String currentLine = sc.nextLine();
            String[] tokens = currentLine.split(DELIMITER);

            Address address = new Address();

            Integer addyIndex = Integer.parseInt(tokens[0]);

            address.setIdNumber(addyIndex);
            address.setFirstName(tokens[1]);
            address.setLastName(tokens[2]);
            address.setStreetAddress(tokens[3]);
            address.setCity(tokens[4]);
            address.setState(tokens[5]);
            address.setZip(Integer.parseInt(tokens[6]));

            if (addyIndex >= this.idIndex) {
                this.idIndex = addyIndex + 1;
            }

            this.addresses.put(address.getIdNumber(), address);
        }
    }

    public void writeAddressBook() throws IOException {
        PrintWriter out = new PrintWriter(new FileWriter(ADDRESS_FILE));

        Collection<Address> addys = addresses.values();
        for (Address addy : addys) {
            out.println(addy.getIdNumber() + DELIMITER + addy.getFirstName() + DELIMITER + addy.getLastName()
                    + DELIMITER + addy.getStreetAddress() + DELIMITER + addy.getCity() + DELIMITER + addy.getState()
                    + DELIMITER + addy.getZip());
        }

        out.flush();
        out.close();
    }
}
