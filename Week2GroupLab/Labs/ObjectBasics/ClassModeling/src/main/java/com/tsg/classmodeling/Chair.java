/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.classmodeling;

/**
 *
 * @author apprentice
 */
public class Chair {
    private String chairType; 
    private String chairMaterial; //ie wooden, steel or plastic.
    private String color;
    private int height;
    private int weight;

    public Chair(String chairType, String chairMaterial, String color, int height, int weight) {
        this.chairType = chairType;
        this.chairMaterial = chairMaterial;
        this.color = color;
        this.height = height;
        this.weight = weight;
    }

    public String getChairType() {
        return chairType;
    }

    public void setChairType(String chairType) {
        this.chairType = chairType;
    }

    public String getChairMaterial() {
        return chairMaterial;
    }

    public void setChairMaterial(String chairMaterial) {
        this.chairMaterial = chairMaterial;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    
    //Behaviors
    
    /**
     * these are the behaviors of a chair
     */
    
    public void usage()
    {
        System.out.println("A chair can be used at home, school, office, church, outdoors");
    }
}
