/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.springaddressbook.dao;

import com.tsg.springaddressbook.dto.Address;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Nathan Ward <nateward.nw@gmail.com>
 */
public class AddressBookDAODbImpl implements AddressBookDAO{
    
    private JdbcTemplate jdbcTemplate;

    public void setJdbcTemplate(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }
    
    private static final String SQL_INSERT_Address
            = "insert into Address (firstName, lastName, streetAddress, city, state, zip) values (?, ?, ?, ?, ?, ?)";

    private static final String SQL_DELETE_Address
            = "delete from Address where idNumber = ?";

    private static final String SQL_UPDATE_Address 
            = "update Address set firstName = ?, lastName = ?, streetAddress = ?, city = ?, state = ?, zip = ? WHERE idNumber = ?";
    
    private static final String SQL_SELECT_ALL_Addresses 
            = "select * from Address";
    
    private static final String SQL_SELECT_Address
            = "select * from Address where idNumber = ?";    

    @Override
    public List<Address> GetAll() {
        return jdbcTemplate.query(SQL_SELECT_ALL_Addresses, new AddressMapper());
    }

    @Override
    public Address GetById(int idNumber) {
        try{
            return jdbcTemplate.queryForObject(SQL_INSERT_Address, new AddressMapper(), idNumber);
        }catch(EmptyResultDataAccessException e){
            return null;
        }
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED, readOnly = false)
    public Address Create(Address dto) {
        jdbcTemplate.update(SQL_INSERT_Address,
                dto.getFirstName(),
                dto.getLastName(),
                dto.getStreetAddress(),
                dto.getCity(),
                dto.getState(),
                dto.getZip());
        dto.setIdNumber(jdbcTemplate.queryForObject("select LAST_INSERT_ID()", Integer.class));

        return dto;
    }

    @Override
    public void Update(int id, Address dto) {
        jdbcTemplate.update(SQL_UPDATE_Address,
                dto.getFirstName(),
                dto.getLastName(),
                dto.getStreetAddress(),
                dto.getCity(),
                dto.getState(),
                dto.getZip());
    }

    @Override
    public void Delete(int id) {
        jdbcTemplate.update(SQL_DELETE_Address, id);
    }

    @Override
    public List<Address> searchContacts(Map<SearchTerm, String> criteria) {
        if (criteria.size() == 0) {
            return GetAll();
        }else{
            StringBuilder sQuery = new StringBuilder("select * from Address where ");
            
            int numParams = criteria.size();
            int paramPosition = 0;
            
            String[] paramVals = new String[numParams];
            Set<SearchTerm> keySet = criteria.keySet();
            
            Iterator<SearchTerm> iter = keySet.iterator();
            
            while (iter.hasNext()) {
                SearchTerm currentKey = iter.next();
                
                if (paramPosition > 0) {
                    sQuery.append(" and ");
                }
                
                sQuery.append(currentKey);
                sQuery.append(" = ? ");
                
                paramVals[paramPosition] = criteria.get(currentKey);
                paramPosition++;
            }
            return jdbcTemplate.query(sQuery.toString(), new AddressMapper(), paramVals);
        }
    }
    
    private static final class AddressMapper implements RowMapper<Address> {

        @Override
        public Address mapRow(ResultSet rs, int i) throws SQLException {
            Address dto = new Address();
            dto.setIdNumber(rs.getInt("idNumber"));
            dto.setFirstName(rs.getString("firstName"));
            dto.setLastName(rs.getString("lastName"));
            dto.setStreetAddress(rs.getString("streetAddress"));
            dto.setCity(rs.getString("city"));
            dto.setState(rs.getString("state"));
            dto.setZip(rs.getString("zip"));

            return dto;
        }

    }

}
