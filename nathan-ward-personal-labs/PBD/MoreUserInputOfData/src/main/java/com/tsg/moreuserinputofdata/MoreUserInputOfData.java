/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tsg.moreuserinputofdata;

import java.util.Scanner;

/**
 *
 * @author apprentice
 */
public class MoreUserInputOfData {
    public static void main(String[] args) {
        Scanner keyboard = new Scanner(System.in);
        String fName = "";
        String lName = "";
        int grade = 0;
        int studentId = 0;
        String login = "";
        float gpa = 0.0f;
        
        
        
        System.out.println("Please enter the following info so I can sell it for a profit!\n");
        
        System.out.println("First name: ");
        fName = keyboard.next();
        
        System.out.println("Last name: ");
        lName = keyboard.next();
        
        System.out.println("Grade (9-12): ");
        grade = keyboard.nextInt();
        
        System.out.println("Student ID: ");
        studentId = keyboard.nextInt();
        
        System.out.println("Login: ");
        login = keyboard.next();
        
        System.out.println("GPA (0.0-4.0): ");
        gpa = keyboard.nextFloat();
        
        
        System.out.println("Your Information:");
        System.out.println("\t Login: " + login);
        System.out.println("\t ID: " + studentId);
        System.out.println("\t Name: " + lName + ", " + fName);
        System.out.println("\t GPA: " + gpa);
        System.out.println("\t Grade: " + grade);
    }
    
}
